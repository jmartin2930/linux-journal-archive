<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/oracle">
  <channel>
    <title>Oracle</title>
    <link>https://www.linuxjournal.com/tag/oracle</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Oracle Linux on Btrfs for the Raspberry Pi</title>
  <link>https://www.linuxjournal.com/content/oracle-linux-btrfs-raspberry-pi</link>
  <description>  &lt;div data-history-node-id="1340438" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/Raspberry_Pi_3_B%2B_%2826931245278%29.png" width="512" height="340" alt="Raspberry Pi 3 B+" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/charles-fisher" lang="" about="https://www.linuxjournal.com/users/charles-fisher" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Charles Fisher&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Enterprise comes to the micro server.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
&lt;a href="http://www.oracle.com/technetwork/server-storage/linux/downloads/oracle-linux-arm-4072846.html"&gt;Oracle
Linux 7&lt;/a&gt; has been released for the &lt;a href="https://en.wikipedia.org/wiki/Raspberry_Pi"&gt;Raspberry Pi 3&lt;/a&gt;. The release
packages &lt;a href="https://www.oracle.com/technetwork/server-storage/linux/technologies/btrfs-overview-1898045.html"&gt;Btrfs&lt;/a&gt; as the root filesystem on the UEK-branded Linux 4.14 Long
Term Support (LTS) kernel. A bootable disk image with a minimal install is
provided along with a standard ISO installer.
&lt;/p&gt;

&lt;p&gt;
CentOS &lt;a href="https://wiki.centos.org/SpecialInterestGroup/AltArch/AArch64"&gt;appears
to support&lt;/a&gt; only the "Mustang" Applied Micro X-Gene for
AArch64, and it provides the older AArch32 environment for all models of the
Raspberry Pi. Oracle Linux is a compelling option among RPM distributions
in supporting AArch64 for the Pi Model 3.
&lt;/p&gt;

&lt;p&gt;
This is not to say that Oracle AArch64 Linux is without flaw, as Oracle
warns that this is "a preview release and for development purposes only;
Oracle suggests these not be used in production." The non-functional WiFi
device is missing firmware and documentation, which Oracle admits was
overlooked. No X11 graphics are included in the image, although you can
install them. The eponymous database client (and server) are absent. Oracle
has provided a previous example of orphaned software with its &lt;a href="https://www.oracle.com/technetwork/server-storage/linux/downloads/oracle-linux-sparc-3665558.html"&gt;Linux for
SPARC&lt;/a&gt; project, which was abandoned after two minor releases. There's no
guarantee that this ARM version will not suffer the same fate, although
Oracle has responded that "our eventual target is server class platforms".
One possible hardware target is the Fujitsu &lt;a href="https://www.nextplatform.com/2018/08/24/fujitsus-a64fx-arm-chip-waves-the-hpc-banner-high"&gt;A64FX&lt;/a&gt;, a new server processor
that bundles 48 addressable AArch64 cores and 32GB of RAM on one die,
asserted to be the "fastest server processor" that exists.
&lt;/p&gt;

&lt;h3&gt;
AArch64 on the Pi
&lt;/h3&gt;

&lt;p&gt;
You'll need a Raspberry Pi Model 3 to run Oracle Linux. The 3B+ is the best
available device, and you should choose that over the predecessor Model 3B and
all other previous models. Both Model 3 boards retain the (constraining)
1GB of RAM—a SODIMM socket would be far more practical. The newer board
has a CPU that is 200MHz faster and a Gigabit-compatible Ethernet port
(that is limited to 300Mbit due to the USB2 linkage that connects it). A
Model A also exists, but it lacks many of the ports on the 3B. More
important, the Model 3 platform introduces a 64-bit CPU.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/oracle-linux-btrfs-raspberry-pi" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 22 Jul 2019 11:30:00 +0000</pubDate>
    <dc:creator>Charles Fisher</dc:creator>
    <guid isPermaLink="false">1340438 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Oracle Patches Spectre for Red Hat</title>
  <link>https://www.linuxjournal.com/content/oracle-patches-spectre-red-hat</link>
  <description>  &lt;div data-history-node-id="1339787" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--221757898_1.jpg" width="800" height="600" alt="Spectre" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/charles-fisher" lang="" about="https://www.linuxjournal.com/users/charles-fisher" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Charles Fisher&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Red Hat's Spectre remediation currently requires new microcode for a complete fix, which leaves most x86 processors vulnerable as they lack this update. Oracle has released new retpoline kernels that completely remediate Meltdown and Spectre on all compatible CPUs, which I show how to install and test on CentOS here.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
The Red Hat community has patiently awaited a retpoline kernel
implementation that remediates CVE-2017-5715 (Spectre v2) and closes all
Meltdown and Spectre vulnerabilities that have captured headlines this
year.
&lt;/p&gt;

&lt;p&gt;
Red Hat's initial fixes rely upon microcode updates for v2 remediation, a
decision that leaves the vast majority of AMD64-capable processors in an
exploitable state. Intel's new microcode has proven especially
problematic; it performs badly and the January 2018 versions were plagued with
stability issues that crashed many systems. It is a poor solution to a
pressing problem.
&lt;/p&gt;

&lt;p&gt;
Google's retpolines—an &lt;a href="https://security.googleblog.com/2018/01/more-details-about-mitigations-for-cpu_4.html"&gt;ingenious&lt;/a&gt;
approach to v2—essentially play out
the exploit within the Linux kernel in any and all vulnerable code. This
method allows the kernel to retain complete control over speculative
execution hardware in all architectures upon which it runs. It is faster
and more generic than Intel's microcode and seems in every way a
superior solution.
&lt;/p&gt;

&lt;p&gt;
Oracle appears to be the first major member of the Red Hat community that
has delivered a Linux kernel with retpoline support. The latest version
of the Unbreakable Enterprise Kernel preview release 5 (UEKr5) now offers
complete remediation for Meltdown and Spectre regardless of CPU microcode
status. The UEKr5 is based on the mainline Linux kernel's long-term
support branch v4.14 release. &lt;strong&gt;&lt;em&gt;In late-breaking news, Oracle has issued a
production release of the UEKr4 that also includes retpolines, details
below.&lt;em&gt;&lt;/em&gt;&lt;/em&gt;&lt;/strong&gt; For corporate users and others with mandated patch schedules over
a large number of servers, the UEK now seems to be the only solution for
complete Spectre coverage on all CPUs. The UEK brings a number of other
advantages over the "Red Hat-Compatible Kernel" (RHCK), but this patch
response is likely to drive Oracle deeply into the Red Hat community
should they remain the single source.
&lt;/p&gt;

&lt;h3&gt;
CentOS Installation&lt;/h3&gt;

&lt;p&gt;
&lt;a href="https://www.centos.org"&gt;CentOS&lt;/a&gt;, &lt;a href="https://www.scientificlinux.org"&gt;Scientific&lt;/a&gt; and &lt;a href="https://linux.oracle.com/pls/apex/f?p=101:101:2470398880366:::::"&gt;Oracle&lt;/a&gt; Linux are all based off the upstream
provider Red Hat. CentOS is a popular variant and is likely the best
demonstration for loading the UEK on a peer distro.
&lt;/p&gt;

&lt;p&gt;
It appears that Red Hat itself has been laying groundwork for a retpoline
kernel. A January 2018 gcc compiler update implies a successful backport:

&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/oracle-patches-spectre-red-hat" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 15 Mar 2018 14:52:25 +0000</pubDate>
    <dc:creator>Charles Fisher</dc:creator>
    <guid isPermaLink="false">1339787 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>ZFS for Linux</title>
  <link>https://www.linuxjournal.com/content/zfs-linux</link>
  <description>  &lt;div data-history-node-id="1339658" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12255f3.png" width="800" height="503" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/charles-fisher" lang="" about="https://www.linuxjournal.com/users/charles-fisher" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Charles Fisher&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;em&gt;Presenting the Solaris ZFS filesystem, as implemented in Linux FUSE, native
kernel modules and the Antergos Linux installer.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
ZFS remains one of the most technically advanced and
feature-complete filesystems since it appeared in October
2005. Code for &lt;a href="http://web.archive.org/web/20060428092023/http://www.sun.com/2004-0914/feature"&gt;Sun's
original Zettabyte File System&lt;/a&gt;
was released under the CDDL open-source license, and it has since become a
standard component of FreeBSD and slowly migrated to various BSD brethren,
while maintaining a strong hold over the descendants of OpenSolaris,
including OpenIndiana and SmartOS.
&lt;/p&gt;

&lt;p&gt;
Oracle is the owner and custodian of ZFS, and it's in a peculiar position
with respect to Linux filesystems. &lt;a href="https://docs.oracle.com/cd/E37670_01/E37355/html/ol_btrfs.html"&gt;Btrfs&lt;/a&gt;, the main challenger to ZFS,
began development at Oracle, where it is a core component of Oracle Linux,
despite &lt;a href="https://www.suse.com/communities/blog/butter-bei-die-fische"&gt;stability
issues&lt;/a&gt;
Red
Hat's recent decision to &lt;a href="https://www.theregister.co.uk/2017/08/16/red_hat_banishes_btrfs_from_rhel"&gt;deprecate
Btrfs&lt;/a&gt; likely introduces
compatibility and support challenges for Oracle's Linux road map.
Oracle obviously has deep familiarity with the Linux filesystem landscape,
having recently released &lt;a href="https://blogs.oracle.com/linuxkernel/upcoming-xfs-work-in-linux-v48-v49-and-v410%2c-by-darrick-wong"&gt;"dedup"
patches for XFS&lt;/a&gt;.
ZFS is the only filesystem option that is stable, protects your data,
is proven to survive in most hostile environments and has a lengthy
usage history with well understood strengths and weaknesses.
&lt;/p&gt;

&lt;p&gt;
ZFS has been (mostly) kept out of Linux due to
&lt;a href="https://sfconservancy.org/blog/2016/feb/25/zfs-and-linux"&gt;CDDL
incompatibility&lt;/a&gt; with Linux's GPL license.
It is the clear hope of the Linux community that
Oracle will re-license ZFS in a form that can be included in Linux,
and we should all gently cajole Oracle to do so. Obviously,
a re-license of ZFS will have a clear impact on Btrfs and the
rest of Linux, and we should work to understand Oracle's position
as the holder of these tools. However, Oracle continues to gift
large software projects for independent leadership. Incomplete
examples of Oracle's largesse include &lt;a href="http://www.zdnet.com/article/oracle-gives-openoffice-to-apache"&gt;OpenOffice&lt;/a&gt;
and recently &lt;a href="https://adtmag.com/articles/2017/09/12/java-ee-moving-to-eclipse.aspx"&gt;Java
Enterprise Edition&lt;/a&gt;,
so it is not inconceivable that Oracle's generosity may at some point
extend additionally to ZFS.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/zfs-linux" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 12 Feb 2018 15:54:56 +0000</pubDate>
    <dc:creator>Charles Fisher</dc:creator>
    <guid isPermaLink="false">1339658 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Meltdown/Spectre Status for Red Hat and Oracle</title>
  <link>https://www.linuxjournal.com/content/meltdownspectre-status-red-hat-and-oracle</link>
  <description>  &lt;div data-history-node-id="1339648" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--221757898.jpg" width="800" height="600" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/charles-fisher" lang="" about="https://www.linuxjournal.com/users/charles-fisher" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Charles Fisher&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
The Red Hat family of operating systems addressed Meltdown and Spectre in
its v3.10 kernel quickly, but relied too much upon Intel's flawed
microcode and was forced to revert from a complete solution. Oracle
implemented alternate approaches more suited to its v4.1 UEK, but both
kernels continue to lack full Spectre coverage while they wait for Intel.
Conspicuously absent from either Linux branch is Google's retpoline,
which offers far greater and more efficient coverage for all CPUs. Auditing
this status is a challenge. This article presents the latest tools for vulnerability
assessments.
&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
A frenzy of patch activity has surrounded this year's Meltdown and Spectre
CPU vulnerability disclosures. Normally quiet microcode packages for Intel
chips have seen &lt;a href="https://oss.oracle.com/ol7/SRPMS-updates"&gt;four&lt;/a&gt; updates in the month of January, one of which was
finally to roll back flawed code that triggers random reboots. For
enterprise-grade hardware, Intel's quality control has left much to be
desired.
&lt;/p&gt;

&lt;p&gt;
It is likely premature to deploy new monitoring and compliance tools, and a
final solution for this set of vulnerabilities will wait until correct
microcode is obtained. Still, it may be important for many organizations to
evaluate the patch status of servers running Linux kernels packaged by Oracle
and/or Red Hat.
&lt;/p&gt;

&lt;p&gt;
Meltdown patches exist now and should be deployed immediately on vulnerable
servers. Remediating all Spectre vulnerabilities requires not only the latest
kernels, but also a patched GCC to compile the kernel that is capable of
implementing "retpolines", or compatible microcode from your CPU vendor.
&lt;/p&gt;

&lt;h3&gt;
RHCK&lt;/h3&gt;

&lt;p&gt;
Red Hat was one of the first Linux distributions to publish &lt;a href="https://access.redhat.com/articles/3311301"&gt;guidance on
Meltdown and Spectre&lt;/a&gt;. It established three files as "kernel tunables" in
the /sys/kernel/debug/x86 directory to monitor and control these patches:
&lt;code&gt;pti_enabled&lt;/code&gt; for Meltdown, &lt;code&gt;ibpb_enabled&lt;/code&gt; for Spectre
v1 and &lt;code&gt;ibrs_enabled&lt;/code&gt; for
Spectre v2. Only the root user can access these files.
&lt;/p&gt;

&lt;p&gt;
When these files contain a numerical zero, the patches are not active. If
allowed for the CPU, a numerical one may be written to the file to enable the
relevant remediation, and a zero may be written later to disable it. This is
not always allowed—AMD processors are not vulnerable to Meltdown, and the
value in the &lt;code&gt;pti_enabled&lt;/code&gt; file is locked to zero and cannot be changed. If the
fixes are active and show 1, the performance of the CPU may be reduced.
Compatible microcode is required to enable all patches on vulnerable CPUs,
which adds new assembler/machine language op codes that erase vulnerable
kernel data from CPU pipelines and caches to close the exploit.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/meltdownspectre-status-red-hat-and-oracle" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 05 Feb 2018 20:34:57 +0000</pubDate>
    <dc:creator>Charles Fisher</dc:creator>
    <guid isPermaLink="false">1339648 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Stunnel Security for Oracle</title>
  <link>https://www.linuxjournal.com/content/stunnel-security-oracle</link>
  <description>  &lt;div data-history-node-id="1339117" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/hqdefault_0.jpg" width="480" height="360" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/charles-fisher" lang="" about="https://www.linuxjournal.com/users/charles-fisher" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Charles Fisher&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Oracle has integrated modern Transport Layer Security (TLS) network
encryption into its eponymous database product, and TLS usage no longer
requires the Advanced Security option beginning with the 10.2 database
release. Legacy configurations lacking TLS exchange encrypted passwords,
but the session payload is transmitted in clear text and is intercepted
easily by anyone with control over the intermediate network. Databases holding
sensitive content should avoid clear-text traffic configurations.
&lt;/p&gt;

&lt;p&gt;
It is possible to use the stunnel utility to wrap the Oracle
Transparent Network Substrate (TNS) Listener "invisibly" with TLS
encryption as an
isolated process, and this configuration appears to be compatible both with
Oracle's sqlplus command-line utility and with database links that are
used for distributed transactions between multiple database instances.
There are several benefits to stunnel over the TNS Listener's native TLS
implementation:
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;
&lt;p&gt;
The stunnel utility can be far less expensive. Older Oracle database
releases required the Advanced Security option to use TLS, which is
licensed at $15,000 per CPU
according to the &lt;a href="http://www.oracle.com/us/corporate/pricing/technology-price-list-070617.pdf"&gt;latest
pricing&lt;/a&gt;,
but TLS is now
included with &lt;a href="https://oracle-base.com/articles/misc/configure-tcpip-with-ssl-and-tls-for-database-connections"&gt;Standard
Edition SE2&lt;/a&gt;.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
The stunnel utility and the associated dependent libraries (that is, OpenSSL)
are patched far more often, and updates can be applied immediately with no
database "bounce" if stunnel is used in an
"inetd" configuration. Oracle
issued eight total &lt;a href="https://oss.oracle.com/ol7/SRPMS-updates"&gt;patched
versions&lt;/a&gt; of OpenSSL in 2015 for Oracle Linux 7.
Database patches are issued only four times per year at regular quarterly
intervals and require instance bounces/outages. An urgent SSL/TLS update
will have lengthy delays when implemented as a database patch (due in part
to an overabundance of caution by most DBAs), but will be far easier to
apply as a simple OS utility patch with no downtime. For this reason,
security-sensitive code that may require immediate updates should be kept
out of the database server whenever possible. The stunnel utility meets
this requirement very well.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
The stunnel utility can run as a separate user and group inside a
"chroot jail" that has limited visibility to the rest of the system.
Oracle's server TLS implementation runs with the full privilege of the TNS
Listener. A compromise of the TLS engine can be drastically less dangerous
if it is confined within a chroot() jail. Privilege separation and chroot()
are well-recognized security techniques, and many security-sensitive
installations likely will disable listener TLS for this reason alone.
&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;
Let's proceed with adding stunnel TLS services to Oracle.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/stunnel-security-oracle" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 28 Jul 2016 20:00:00 +0000</pubDate>
    <dc:creator>Charles Fisher</dc:creator>
    <guid isPermaLink="false">1339117 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Who Contributes the Most to LibreOffice?</title>
  <link>https://www.linuxjournal.com/content/who-contributes-most-libreoffice</link>
  <description>  &lt;div data-history-node-id="1018179" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/libreoffice2_3.png" width="100" height="128" alt="LibreOffice" title="LibreOffice" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/susan-linton" lang="" about="https://www.linuxjournal.com/users/susan-linton" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Susan Linton&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Cedric Bosdonnat has been tracking contributions to LibreOffice since its announced fork from OpenOffice.org.  He uses Git Data Miner to gleen results from the main branch of LibreOffice Git repositories.  Git Data Miner is more commonly known as the tool used by Jonathan Corbet in his periodic kernel code reports. &lt;/p&gt;
&lt;p&gt;Bosdonnat began tracking line contributions in the middle of September 2010 with the original 14 contributions being made by Oracle.  Oracle actually contributes code to OpenOffice.org, and then LibreOffice merges those changes, thus resulting in Oracle's contributions to the new fork.  These 112 contributions have continued throughout development, but are dwarfed by the contributions of new developers.&lt;/p&gt;
&lt;p&gt;New contributors are those that have signed on to help with LibreOffice since the fork, either with code or translations.  These contributions make up well over half of the total new changes found in LibreOffice as of mid-February.  Weekly contributions in this area have averaged between 20 and 30 with a total number of 517 line contributions.&lt;/p&gt;
&lt;p&gt;Those who worked on OpenOffice.org previously and are not employeed by any other major contributor are classified as known contributors.  While their number of contributions have been fewer, they averaged approximately five per week since the fork.  This totals 90 contributions in the 22 weeks of development.&lt;/p&gt;
&lt;p&gt;Novell has been credited with a large portion of the contributions made to LibreOffice.  When looking through changelogs the name Novell is seen over and over again.  They were significant contributors to OpenOffice.org and many of their patches are used in LibreOffice to this day.  Novell developers averaged in the neighborhood of 10 contributions per week for a total of 205 since the fork.&lt;/p&gt;
&lt;p&gt;Red Hat, who also contributed to OpenOffice.org, has chipped in as well.  With usually two contributions per week, Red Hat developers have provided 39 patches since the fork.&lt;/p&gt;
&lt;p&gt;The newest known name to join the contributors list is Canonical.  They contributed the Human theme and a later fix, but more Ubuntu integration code is likely.  Björn Michaelsen contributed 2 patches in the last few weeks so far.&lt;/p&gt;
&lt;p&gt;Bosdonnat &lt;a href="http://cedric.bosdonnat.free.fr/wordpress/?p=758"&gt;says&lt;/a&gt; there are 133 new coders and 55 localizers since the fork.  There seemed to be a slight dip at the end of last year according the graph and Bosdonnat attributes that to the festivities of the holiday season.&lt;/p&gt;
&lt;p&gt;&lt;/p&gt;&lt;center&gt;&lt;a href="http://www.tuxmachines.org/images/gitdm-lo-2011-07-people.png"&gt;&lt;img src="http://www.tuxmachines.org/images/gitdm-lo-2011-07-people_small.png" /&gt;&lt;/a&gt;&lt;/center&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/who-contributes-most-libreoffice" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 01 Mar 2011 14:00:00 +0000</pubDate>
    <dc:creator>Susan Linton</dc:creator>
    <guid isPermaLink="false">1018179 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
