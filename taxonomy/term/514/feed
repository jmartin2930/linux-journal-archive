<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/otrs">
  <channel>
    <title>OTRS</title>
    <link>https://www.linuxjournal.com/tag/otrs</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Make Customers Smile in 7 Easy Steps with OTRS - Part 4</title>
  <link>https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-4</link>
  <description>  &lt;div data-history-node-id="1017942" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/image25-small.jpg" width="640" height="456" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/vikram-vaswani" lang="" about="https://www.linuxjournal.com/users/vikram-vaswani" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Vikram Vaswani&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;In &lt;a href="http://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-3"&gt;Part 3&lt;/a&gt;, you learned how to accept, open and close tickets, and use the OTRS interface to communicate with customers and internal team members through the ticket resolution workflow. This is, in many ways, the core of OTRS, and the wide variety of features available ensure that are able to support a large variety of different scenarios, and also customize the system to your own special requirements.&lt;/p&gt; 
 
&lt;p&gt;In this concluding segment, I'll look at the OTRS configuration panel, which allows you to tweak system behaviour to your needs, and the reporting interface, which provides managers and administrators with a birds-eye view of operational issues at any time.&lt;/p&gt; 
 
&lt;h2&gt;Step 6: Configure Global Settings&lt;/h2&gt; 
 
&lt;p&gt;OTRS comes with an extensive set of configuration screens, which allow administrators to configure many different aspects of system behaviour. These screens are accessible through the Dashboard -&gt; Admin -&gt; SysConfig menu to users belonging to the "admin" group, and are grouped by function. Figure 24 illustrates what the main configuration interface looks like:&lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image24-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 24: OTRS system configuration&lt;/p&gt; 
 
&lt;p&gt;In particular, you might want to pay special attention to the Core group (Figure 25), which lets you enter essential information for the system: the organization name; domain information; default theme, character set and languages; administrative information; and email notification parameters. For example, you can control whether OTRS should check the destination mail server's MX record (CheckMXRecord) and the recipient email address for validity (CheckEmailValidAddress) before dispatching email messages, and you can also run the system in simulation (DemoSystem) or high-security mode (SecureMode). &lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image25-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 25: OTRS core settings&lt;/p&gt; 
 
&lt;p&gt;You should also briefly look through the settings in the Frontend::Agent group, which allow you to control the various notifications sent to agents, and the Frontend::Customer group (Figure 26), which allow you to adjust the customer front-end to your needs. For example, you might want to disable self-serve account creation (CustomerPanelCreateAccount), or adjust the login and logout URLs used by customers (CustomerPanelLoginURL and CustomerPanelLogoutURL).&lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image26-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 26: OTRS customer settings&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-4" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 28 Feb 2011 14:00:00 +0000</pubDate>
    <dc:creator>Vikram Vaswani</dc:creator>
    <guid isPermaLink="false">1017942 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Make Customers Smile in 7 Easy Steps with OTRS - Part 3</title>
  <link>https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-3</link>
  <description>  &lt;div data-history-node-id="1017939" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/image16-small.jpg" width="640" height="384" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/vikram-vaswani" lang="" about="https://www.linuxjournal.com/users/vikram-vaswani" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Vikram Vaswani&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;In &lt;a href="http://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-2"&gt;Part 2&lt;/a&gt;, you learned about agents, customers and queues, and you also got a brief look under OTRS' hood, by learning how to customize the customer self-ervice portal with your own theme and logo. The customer portal is more than just a pretty face, however. It is the easiest way for customers to register new support tickets, check the status of existing tickets, and enter into online conversations with support staff. The easiest way to demonstrate how this works is with an example, and that's what will be the subject of Part 3. &lt;/p&gt; 
 
&lt;h2&gt;Step 5: Begin Accepting and Resolving Tickets&lt;/h2&gt; 
 
&lt;p&gt;Put yourself in the shoes of Ms. Alice Apple, a long-time Dizzy Domains customer. This morning, when comes in to work, she realizes that she isn't able to download her email from the Dizzy Domains mail server. So, she pops open her Web browser, surfs on over to the Dizzy Domains Web site, and clicks the big red "Support" link. This link redirects her to the OTRS customer support portal, where she registers for a new account by entering her name and email address. After completing this process, Alice receives a verification email, which contains her password. She uses this to log in to OTRS, where she is presented with a screen like the one shown in Figure 16.&lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image16-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 16: OTRS customer dashboard&lt;/p&gt; 
 
&lt;p&gt;To create a new support ticket, Alice must select the New Ticket option, select the appropriate queue for her problem, and enter details about her problem. Figure 17 illustrates the support ticket editing interface.&lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image17-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 17: New ticket creation&lt;/p&gt; 
 
&lt;p&gt;Once Alice submits the form, the ticket is registered, assigned a unique numeric identifier and reappears in the customer dashboard. Figure 18 illustrates:&lt;/p&gt; 
 
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image18-small.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 18: OTRS customer dashboard displaying new ticket&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-3" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 10 Feb 2011 14:00:00 +0000</pubDate>
    <dc:creator>Vikram Vaswani</dc:creator>
    <guid isPermaLink="false">1017939 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Make Customers Smile in 7 Easy Steps with OTRS - Part 2</title>
  <link>https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-2</link>
  <description>  &lt;div data-history-node-id="1015704" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/image15.png" width="640" height="456" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/vikram-vaswani" lang="" about="https://www.linuxjournal.com/users/vikram-vaswani" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Vikram Vaswani&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs"&gt;In Part 1&lt;/a&gt;, I introduced you to OTRS and guided you through the process of installing and configuring oTRS on your system. At the end of Part 1, you were able to log in to the OTRS Dashboard, which serves as the central point for all OTRS operations.&lt;/p&gt;

&lt;p&gt;In Part 2, you'll learn a little more about how OTRS works and you'll also understand the concepts of agents, queues and customers. These concepts are essential to working with OTRS in a production environment. You'll also learn how to create your own OTRS theme, to customize OTRS to your corporate environment.&lt;/p&gt;

&lt;h2&gt;Step 2: Add Agents&lt;/h2&gt;

&lt;p&gt;Now that you've got all the pieces installed and configured, it's time to take OTRS out for a spin and see how it works. To begin, assume for a moment that you run a Web hosting service named Dizzy Domains, selling customers the bandwidth and disk space they need to host their Web sites and email. As a small, service-oriented business, you've always tried to support your customers by responding to problems quickly and efficiently. Word has spread about the good service you offer, and you're signing up more and more customers every day. The natural consequence of more customers is, of course, more support requests and so, you've decided to install OTRS to help your three-man support team cope with the increasing workload.&lt;/p&gt;

&lt;p&gt;OTRS defines two main types of users: agents and customers. Customers generate support tickets; agents receive and resolve them. Therefore, as a necessary first step, you must create agent accounts for each of your support personnel, to enable them to log in to OTRS and begin dealing with customer requests. To do this, use the Dashboard -&gt; Admin -&gt; Users -&gt; User Management -&gt; Add User command and create accounts for each of your agents. Figure 8 has an example of what the account creation form looks like:&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/image8.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;Figure 8: Agent account creation&lt;/p&gt;

&lt;p&gt;You will need to enter each agent's first and last name, login username, password, preferred language, together with notification preferences for different OTRS events. Once these details are submitted, you'll be asked to specify the agent's privileges. By default, OTRS comes with three groups - "users", "admin" and "stats" - and allows you to customize the privileges each agent has with respect to tickets in each group. It's generally a good idea to ensure that your agents have full privileges for the "users" group, as this allows them to move, merge, create, add notes to and set priorities for tickets in most queues. Figure 9 has an example of how you can configure this:&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs-part-2" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 02 Nov 2010 14:00:00 +0000</pubDate>
    <dc:creator>Vikram Vaswani</dc:creator>
    <guid isPermaLink="false">1015704 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Make Customers Smile in 7 Easy Steps with OTRS</title>
  <link>https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs</link>
  <description>  &lt;div data-history-node-id="1014816" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/otrs-article.jpg" width="504" height="480" alt="OTRS screenshot" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/vikram-vaswani" lang="" about="https://www.linuxjournal.com/users/vikram-vaswani" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Vikram Vaswani&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Back in the old days, if a customer had a problem with a product or service, he'd pick up the phone and dial the service provider or vendor responsible. In most cases, he'd be attended to by a technical expert, who'd ask for details and then attempt to diagnose and resolve the problem. Complex problems that could not be immediately resolved might require the customer to make one or more follow-up calls, often needing to repeat the details of the problem each time. The more professional agencies might maintain a record of the call and its eventual resolution; the less professional ones wouldn't bother.&lt;/p&gt;
&lt;p&gt;In today's fast-paced environment, this approach wouldn't work at all. Today's customers don't just use the phone to make complaints; they also send emails and faxes, visit support Web sites, send Facebook messages and write messages in forums. They're also an impatient lot who don't like repeating themselves, they expect service representative to have their entire complaint history at their fingertips, and they want problems to be resolved "yesterday".&lt;/p&gt;
&lt;p&gt;Enter the trouble ticket system, a useful little invention that lets service and support staff efficiently record, track and resolve customer complaints, regardless of where they come from or how many of them there are. As the name suggests, this system works like an automated assistant, helping service center staff to log, centralize and correlate communications on particular issues, search and lookup similar issues for more efficient resolution and, ultimately, handle customer communication in a professional and efficient manner.&lt;/p&gt;
&lt;p&gt;In this article, I'll introduce you to one such system: OTRS, or the Open Ticket Request System. OTRS is a popular, open-source ticket system that offers all of the features listed above (and a few more besides). Over the next few pages, I'll show you how to install and configure it, and then walk you through the process of adding, processing and resolving tickets for an example business scenario. So come on in, and let's get started!&lt;/p&gt;
&lt;h2&gt;Introducing OTRS&lt;/h2&gt;
&lt;p&gt;First up, a quick introduction to OTRS.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.otrs.com/"&gt;OTRS&lt;/a&gt; is an open-source, highly scalable trouble ticket system written in Perl. Built and maintained in Germany by the OTRS Group, OTRS is very popular with service providers of all sizes on account of its powerful tools for ticket management and problem resolution.&lt;/p&gt;
&lt;p&gt;Here's a quick list of the key features:&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/make-customers-smile-7-easy-steps-otrs" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 05 Oct 2010 15:45:15 +0000</pubDate>
    <dc:creator>Vikram Vaswani</dc:creator>
    <guid isPermaLink="false">1014816 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
