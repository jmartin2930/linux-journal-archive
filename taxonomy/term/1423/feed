<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/golang">
  <channel>
    <title>Golang</title>
    <link>https://www.linuxjournal.com/tag/golang</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Introductory Go Programming Tutorial</title>
  <link>https://www.linuxjournal.com/content/introductory-go-programming-tutorial</link>
  <description>  &lt;div data-history-node-id="1340128" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/gopher.jpg" width="800" height="400" alt="Go Programming Language Logo" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/jay-ts" lang="" about="https://www.linuxjournal.com/users/jay-ts" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Jay Ts&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;How to get started with this useful new programming language.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
You've probably heard of Go. Like any new programming language, it took a
while to mature and stabilize to the point where it became useful
for production applications. Nowadays, Go is a well established language
that is used in web development, writing DevOps tools, network programming
and databases. It was used to write Docker, Kubernetes, Terraform and
Ethereum. Go is accelerating in popularity, with adoption increasing by
76% in 2017, and there now are Go user groups and Go conferences. Whether
you want to add to your professional skills or are just interested in
learning a new programming language, you should check it out.
&lt;/p&gt;

&lt;h3&gt;
Go History&lt;/h3&gt;

&lt;p&gt;
A team of three programmers at Google created Go: Robert Griesemer,
Rob Pike and Ken Thompson. The team decided to create Go because they
were frustrated with C++ and Java, which through the years have become
cumbersome and clumsy to work with. They wanted to bring enjoyment and
productivity back to programming.
&lt;/p&gt;

&lt;p&gt;
The three have impressive accomplishments. Griesemer worked on Google's
ultra-fast V8 JavaScript engine used in the Chrome web browser, Node.js
JavaScript runtime environment and elsewhere. Pike and Thompson were
part of the original Bell Labs team that created UNIX, the C language
and UNIX utilities, which led to the development of the GNU utilities
and Linux. Thompson wrote the very first version of UNIX and created
the B programming language, upon which C was based. Later, Thompson and
Pike worked on the Plan 9 operating system team, and they also worked
together to define the UTF-8 character encoding.
&lt;/p&gt;

&lt;h3&gt;Why Go?&lt;/h3&gt;

&lt;p&gt;
Go has the safety of static typing and garbage collection along with
the speed of a compiled language. With other languages, "compiled"
and "garbage collection" are associated with waiting around for the
compiler to finish and then getting programs that run slowly. But Go has
a lightning-fast compiler that makes compile times barely noticeable
and a modern, ultra-efficient garbage collector. You get fast compile
times along with fast programs. Go has concise syntax and grammar with
few keywords, giving Go the simplicity and fun of dynamically typed
interpreted languages like Python, Ruby and JavaScript.
&lt;/p&gt;

&lt;p&gt;
The idea of Go's design is to have the best parts of many languages. At
first, Go looks a lot like a hybrid of C and Pascal (both of which are
successors to Algol 60), but looking closer, you will find ideas taken
from many other languages as well.
&lt;/p&gt;

&lt;p&gt;
Go is designed to be a simple compiled language that is easy to use,
while allowing concisely written programs that run efficiently. Go lacks
extraneous features, so it's easy to program fluently, without needing
to refer to language documentation while programming. Programming in Go
is fast, fun and productive.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/introductory-go-programming-tutorial" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 24 Jan 2019 13:00:00 +0000</pubDate>
    <dc:creator>Jay Ts</dc:creator>
    <guid isPermaLink="false">1340128 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>FOSS Project Spotlight: Pydio Cells, an Enterprise-Focused File-Sharing Solution</title>
  <link>https://www.linuxjournal.com/content/foss-project-spotlight-pydio-cells-enterprise-focused-file-sharing-solution</link>
  <description>  &lt;div data-history-node-id="1339956" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Icon-Of-Exchanging-Files-Conc-231776218.jpg" width="600" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/italo-vignoli" lang="" about="https://www.linuxjournal.com/users/italo-vignoli" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Italo Vignoli&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Pydio Cells is a brand-new product focused on the needs of enterprises and
large organizations, brought to you from the people who launched the concept
of the open-source
file sharing and synchronization solution in 2008. The concept behind
Pydio Cells is challenging: to be to file sharing what Slack has been to
chats—that is, a revolution in terms of the number of features, power and ease of
use.
&lt;/p&gt;

&lt;p&gt;
In order to reach this objective, Pydio's development team has switched
from the old-school development stack (Apache and PHP) to Google's Go
language to overcome the bottleneck represented by legacy technologies.
Today, Pydio Cells offers a faster, more scalable microservice architecture
that is in tune with dynamic modern enterprise environments.
&lt;/p&gt;

&lt;p&gt;
In fact, Pydio's new "Cells" concept delivers file sharing as a
modern collaborative app. Users are free to create flexible group spaces for
sharing based on their own ways of working with dedicated in-app messaging
for improved collaboration.
&lt;/p&gt;

&lt;p&gt;
In addition, the enterprise data management functionality gives both
companies and administrators reassurance, with controls and reporting that
directly answer corporate requirements around the General Data Protection
Regulation (GDPR) and other tightening data
protection regulations.
&lt;/p&gt;

&lt;h3&gt;
Pydio Loves DevOps&lt;/h3&gt;

&lt;p&gt;
In tune with modern enterprise DevOps environments, Pydio Cells now runs as
its own application server (offering a dependency-free binary, with no need for
external libraries or runtime environments). The application is available as
a Docker image, and it offers out-of-the-box connectors for
containerized application orchestrators, such as Kubernetes.
&lt;/p&gt;

&lt;p&gt;
Also, the application has been broken up into a series of logical
microservices. Within this new architecture, each service is allocated its
own storage and persistence, and can be scaled independently. This enables
you to manage and scale Pydio
more efficiently, allocating resources to each
specific service.
&lt;/p&gt;

&lt;p&gt;
The move to Golang has delivered a ten-fold improvement in performance. At
the same time, by breaking the application into logical microservices, larger
users can scale the application by targeting greater resources only to the
services that require it, rather than inefficiently scaling the entire
solution.
&lt;/p&gt;

&lt;h3&gt;
Built on Standards&lt;/h3&gt;

&lt;p&gt;
The new Pydio Cells architecture has been built with a renewed focus on the
most popular modern open standards:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/foss-project-spotlight-pydio-cells-enterprise-focused-file-sharing-solution" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 13 Jul 2018 14:20:00 +0000</pubDate>
    <dc:creator>Italo Vignoli</dc:creator>
    <guid isPermaLink="false">1339956 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Back Up GitHub and GitLab Repositories Using Golang</title>
  <link>https://www.linuxjournal.com/content/back-github-and-gitlab-repositories-using-golang</link>
  <description>  &lt;div data-history-node-id="1339936" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/github-collab-retina-preview.gif" width="400" height="400" alt="github logo" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/amit-saha" lang="" about="https://www.linuxjournal.com/users/amit-saha" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Amit Saha&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;em&gt;Want to learn Golang and build something useful? Learn how to write a
tool to back up your GitHub and GitLab repositories.&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
GitHub and GitLab are two
popular Git repository hosting services that are used to host and manage
open-source projects. They also have become an easy way for content
creators to be able to invite others to share and collaborate without
needing to have their own infrastructure setup.
&lt;/p&gt;

&lt;p&gt;
Using hosted services that you don't manage yourself, however, comes with a
downside. Systems fail, services go down and disks crash. Content
hosted on remote services can simply vanish. Wouldn't it be nice if you
could have an easy way to back up your git repositories periodically into
a place you control?
&lt;/p&gt;

&lt;p&gt;
If you follow along with this article, you will write a Golang program to back up git
repositories from &lt;a href="https://github.com"&gt;GitHub&lt;/a&gt; and &lt;a href="https://about.gitlab.com"&gt;GitLab&lt;/a&gt; (including custom GitLab
installations). Being familiar with Golang basics will be helpful, but
not required. Let's get started!
&lt;/p&gt;

&lt;h3&gt;
Hello Golang&lt;/h3&gt;

&lt;p&gt;
The latest stable release of Golang at the time of this writing is 1.8. The package name is
usually golang, but if your Linux distro doesn't have this release,
you can &lt;a href="https://golang.org/dl"&gt;download the Golang compiler&lt;/a&gt; and
other tools for Linux. Once downloaded, extract it to
/usr/local:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
$ sudo tar -C /usr/local -xzf &lt;filename-from-above&gt;
$ export PATH=$PATH:/usr/local/go/bin
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
Opening a new terminal and typing &lt;code&gt;$ go version&lt;/code&gt; should show the
following:


&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
$ go version
go version go1.8 linux/amd64
&lt;/code&gt;
&lt;/pre&gt;



&lt;p&gt;
Let's write your first program. Listing 1 shows a program that expects
a &lt;code&gt;-name&lt;/code&gt; flag (or argument) when run and prints a greeting using the
specified name. Compile and run the program as follows:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
$ go build listing1.go
$ ./listing1 -name "Amit"
Hello Amit

$ ./listing1
./listing1
2017/02/18 22:48:25 Please specify your name using -name
$ echo $?
1
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
If you don't specify the &lt;code&gt;-name&lt;/code&gt; argument, it exits printing a message
with a non-zero exit code. You can combine both compiling and running
the program using &lt;code&gt;go run&lt;/code&gt;:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
$ go run listing1.go -name Amit
2017/03/04 23:08:11 Hello Amit
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;&lt;em&gt;
Listing 1. Example Program listing1.go&lt;/em&gt;

&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/back-github-and-gitlab-repositories-using-golang" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 04 Jun 2018 21:46:37 +0000</pubDate>
    <dc:creator>Amit Saha</dc:creator>
    <guid isPermaLink="false">1339936 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
