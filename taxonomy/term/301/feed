<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/csound">
  <channel>
    <title>csound</title>
    <link>https://www.linuxjournal.com/tag/csound</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Linux Audio Conference 2011: A Report From Maynooth</title>
  <link>https://www.linuxjournal.com/content/linux-audio-conference-2011-report-maynooth</link>
  <description>  &lt;div data-history-node-id="1021352" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/nui_maynooth_6.jpg" width="300" height="200" alt="A view of Maynooth." title="LAC2011 at Maynooth, Ireland." typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/dave-phillips" lang="" about="https://www.linuxjournal.com/users/dave-phillips" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Dave Phillips&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;On May 7 and 8 I attended the &lt;a href="http://lac.linuxaudio.org/2011/"&gt;Linux Audio Conference for 2011&lt;/a&gt; held in Maynooth, Ireland. Due to a temporary mental malfeasance - for some reason I assumed the Earth rotated in the opposite direction - I booked my flight for the wrong departure date and was unable to change its itinerary without paying out a hefty sum to the airline. So, on Saturday morning I arrived at &lt;a href="http://www.nuim.ie/"&gt;NUI&lt;/a&gt; in Maynooth, completely out of sync with the local time zone and ready to pack four days worth of activity into two.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;LAC 2011&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;Due to scheduling constraints it's impossible for anyone to attend all the presentations and demonstrations at the conference, so I didn't bother trying. I attended some of the scheduled presentations, but the bulk of my time was spent in inspiring conversations and casual meetings with friends old and new. Of course the talk focused on the conference events, related Linux audio news and stories, and where to find the best pubs in Maynooth.&lt;/p&gt;
&lt;p&gt;Before all else I want to send a huge shout-out of thanks to &lt;a href="http://music.nuim.ie/staff/drvictorlazzarini"&gt;Dr. Victor Lazzarini&lt;/a&gt; and his able crew at NUIM. As far as I could tell everything flowed smoothly from time slot to time slot with no logistical disasters. The presentations I attended were well-prepared and very enjoyable, thanks especially to the great efforts of the organizers.  &lt;/p&gt;
&lt;p&gt;Time worked against me, but I was able to catch Jeremy Jongepier's hands-on demonstration of his creative use of Linux audio software such as &lt;a href="http://qtractor.sourceforge.net/qtractor-index.html"&gt;QTractor&lt;/a&gt;, &lt;a href="http://yoshimi.sourceforge.net/"&gt;Yoshimi&lt;/a&gt;, &lt;a href="http://qsynth.sourceforge.net/qsynth-index.html"&gt;QSynth&lt;/a&gt;, and others; Joern Nettingsmeier's lecture on loudness metering and why it matters; an update on recent improvements to the &lt;a href="http://sourceforge.net/apps/trac/fluidsynth/"&gt;Fluidsynth&lt;/a&gt; soundfont synthesizer; and Conor Dempsey's unveiling of the &lt;em&gt;WADE&lt;/em&gt; system, a portal for on-line audio synthesis. Of course I missed a dozen other presentations, due primarily to those lively conversations that went on constantly in the halls at NUIM and at the restaurants and pubs in Maynooth.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/linux-audio-conference-2011-report-maynooth" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 13 Jun 2011 13:00:00 +0000</pubDate>
    <dc:creator>Dave Phillips</dc:creator>
    <guid isPermaLink="false">1021352 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>A Linux Audio Christmas</title>
  <link>https://www.linuxjournal.com/content/linux-audio-christmas</link>
  <description>  &lt;div data-history-node-id="1017632" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/apb-small.jpg" width="100" height="100" alt="Cover photo of The Audio Programming Book." title="The Audio Programming Book, ed. Boulanger and Lazzarini" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/dave-phillips" lang="" about="https://www.linuxjournal.com/users/dave-phillips" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Dave Phillips&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Some interesting items showed up under Dave's Christmas tree, including a new book by some old friends, a laptop keyboard (music, not QWERTY), and an excellent Linux audio plugin for high-quality reverb. Dave must have been a &lt;em&gt;very&lt;/em&gt; good boy last year.&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;The Audio Programming Book&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;Dr. Richard Boulanger has dedicated his life to the promotion of computer-based music, with a particular focus on the &lt;a href="http://www.csounds.com"&gt;Csound&lt;/a&gt; audio/MIDI programming language. His resume includes work with the Who's Who of pioneering electroacoustic musicians and researchers, and his role as educator continues to bring new talents to greater awareness and achievement.&lt;/p&gt;
&lt;p&gt;In the year 2000 Professor Boulanger - a.k.a. Dr. B, a.k.a. Rick - fulfilled one part of his mission when the MIT Press published &lt;a href="http://mitpress.mit.edu/catalog/item/default.asp?ttype=2&amp;tid=3349"&gt;The Csound Book&lt;/a&gt;. That book was a collaborative work that drew contributions from the leading lights of the Csound development and user communities. Rick edited those contributions into an organized and coherent presentation of Csound's many facets, from the details of its C source code to the design of rich synthesis and composition tools. Thanks to the good doctor's guidance and his own contributions &lt;em&gt;The Csound Book&lt;/em&gt; is considered a fundamental text for serious students, and in this Csounder's opinion it's likely to remain so for a long time. &lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/medium-350px-centered/u800764/audioprogrammingbook.jpg" /&gt;&lt;/p&gt;
&lt;p&gt;Figure 1. The Audio Programming Book&lt;/p&gt;
&lt;p&gt;Jump to 2010 and Rick's latest magnum opus &lt;a href="http://mitpress.mit.edu/catalog/item/default.asp?ttype=2&amp;tid=12283"&gt;The Audio Programming Book&lt;/a&gt; (Figure 1). With this volume Dr. B and co-editor Victor Lazzarini have published another fundamental work sure to provide a thorough course of study relevant now and for future generations of students, composers, and researchers. &lt;em&gt;The Audio Programming Book&lt;/em&gt; gathers material from diverse domains - including mathematics, computer science, music, audio engineering, and digital signal analysis - and presents them in the unified context of audio programming in the C programming language (with a liberal dose of C++). The book's broad topics are arranged in this order :&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/linux-audio-christmas" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 02 Feb 2011 16:15:26 +0000</pubDate>
    <dc:creator>Dave Phillips</dc:creator>
    <guid isPermaLink="false">1017632 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Introducing CsoundAC: Algorithmic Composition With Csound And Python</title>
  <link>https://www.linuxjournal.com/content/introducing-csoundac-algorithmic-composition-csound-and-python</link>
  <description>  &lt;div data-history-node-id="1012197" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/little-csoundac-scite.png" width="290" height="400" alt="An image of the SciTE editor with CsoundAC code." title="SciTE runs Python and CsoundAC." typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/dave-phillips" lang="" about="https://www.linuxjournal.com/users/dave-phillips" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Dave Phillips&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
What happens when the world's most powerful waveform compiler meets one of the world's most popular programming languages ? Find out how one programmer makes it all work out in this introduction to CsoundAC.
&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/introducing-csoundac-algorithmic-composition-csound-and-python" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 03 Aug 2010 11:00:00 +0000</pubDate>
    <dc:creator>Dave Phillips</dc:creator>
    <guid isPermaLink="false">1012197 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
