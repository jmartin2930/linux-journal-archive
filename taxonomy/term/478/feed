<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/ldap">
  <channel>
    <title>LDAP</title>
    <link>https://www.linuxjournal.com/tag/ldap</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>FOSS Project Spotlight: Pydio Cells, an Enterprise-Focused File-Sharing Solution</title>
  <link>https://www.linuxjournal.com/content/foss-project-spotlight-pydio-cells-enterprise-focused-file-sharing-solution</link>
  <description>  &lt;div data-history-node-id="1339956" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Icon-Of-Exchanging-Files-Conc-231776218.jpg" width="600" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/italo-vignoli" lang="" about="https://www.linuxjournal.com/users/italo-vignoli" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Italo Vignoli&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Pydio Cells is a brand-new product focused on the needs of enterprises and
large organizations, brought to you from the people who launched the concept
of the open-source
file sharing and synchronization solution in 2008. The concept behind
Pydio Cells is challenging: to be to file sharing what Slack has been to
chats—that is, a revolution in terms of the number of features, power and ease of
use.
&lt;/p&gt;

&lt;p&gt;
In order to reach this objective, Pydio's development team has switched
from the old-school development stack (Apache and PHP) to Google's Go
language to overcome the bottleneck represented by legacy technologies.
Today, Pydio Cells offers a faster, more scalable microservice architecture
that is in tune with dynamic modern enterprise environments.
&lt;/p&gt;

&lt;p&gt;
In fact, Pydio's new "Cells" concept delivers file sharing as a
modern collaborative app. Users are free to create flexible group spaces for
sharing based on their own ways of working with dedicated in-app messaging
for improved collaboration.
&lt;/p&gt;

&lt;p&gt;
In addition, the enterprise data management functionality gives both
companies and administrators reassurance, with controls and reporting that
directly answer corporate requirements around the General Data Protection
Regulation (GDPR) and other tightening data
protection regulations.
&lt;/p&gt;

&lt;h3&gt;
Pydio Loves DevOps&lt;/h3&gt;

&lt;p&gt;
In tune with modern enterprise DevOps environments, Pydio Cells now runs as
its own application server (offering a dependency-free binary, with no need for
external libraries or runtime environments). The application is available as
a Docker image, and it offers out-of-the-box connectors for
containerized application orchestrators, such as Kubernetes.
&lt;/p&gt;

&lt;p&gt;
Also, the application has been broken up into a series of logical
microservices. Within this new architecture, each service is allocated its
own storage and persistence, and can be scaled independently. This enables
you to manage and scale Pydio
more efficiently, allocating resources to each
specific service.
&lt;/p&gt;

&lt;p&gt;
The move to Golang has delivered a ten-fold improvement in performance. At
the same time, by breaking the application into logical microservices, larger
users can scale the application by targeting greater resources only to the
services that require it, rather than inefficiently scaling the entire
solution.
&lt;/p&gt;

&lt;h3&gt;
Built on Standards&lt;/h3&gt;

&lt;p&gt;
The new Pydio Cells architecture has been built with a renewed focus on the
most popular modern open standards:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/foss-project-spotlight-pydio-cells-enterprise-focused-file-sharing-solution" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 13 Jul 2018 14:20:00 +0000</pubDate>
    <dc:creator>Italo Vignoli</dc:creator>
    <guid isPermaLink="false">1339956 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
