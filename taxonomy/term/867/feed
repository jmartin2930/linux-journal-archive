<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/chromebook">
  <channel>
    <title>Chromebook</title>
    <link>https://www.linuxjournal.com/tag/chromebook</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Exploring the Samsung ARM Chromebook 3G</title>
  <link>https://www.linuxjournal.com/content/exploring-samsung-arm-chromebook-3g</link>
  <description>  &lt;div data-history-node-id="1150980" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11495f1.jpg" width="550" height="413" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/bill-childers" lang="" about="https://www.linuxjournal.com/users/bill-childers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Bill Childers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Back in late 2010, Google announced a "Chromebook"—a low-cost,
entry-level netbook that would run Google's own operating system,
ChromeOS. Google's vision of ChromeOS, although based on Linux, 
basically would be a giant Web browser, with all the apps on the machine running
in the browser. ChromeOS would be a nearly stateless computer, with all
the user's apps based in Google's cloud, running the Google Apps suite.
&lt;/p&gt;

&lt;p&gt;
Google's first stab at this was the CR-48: an Intel Atom-powered netbook
with 2GB of RAM and 16GB of Flash. The CR-48 wasn't a powerhouse by any
means, but it had a couple cool things going for it. First, it came
with 100MB of free 3G service a month. Second, it had a "developer
mode"
that allowed users to break free of the strict Chrome-based browser
jail and expose the chewy Linux center. A CR-48 in developer mode became
a usable machine for a lot of people, because the machine pretty much
became a small Linux laptop.
&lt;/p&gt;

&lt;h3&gt;
Today—the Samsung ARM Chromebook&lt;/h3&gt;

&lt;p&gt;
Fast-forward a couple years, and the first real Chromebook products are
hitting the market. Quite a few Chromebooks exist today,
but all of them are Intel-based (either Atom or Celeron). In late 2012,
however, Samsung released an ARM-based Chromebook. This little guy is
different in lots of ways—primarily, it beats its bigger brothers in
size and battery life, without compromising much on performance. Speaking
of performance, let's go over the specifications of the XE303—the
first non-Intel powered Chromebook:
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;
&lt;p&gt;
Dual-Core, Samsung Exynos 5 ARM CPU (Cortex A15, 1.7GHz).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
2GB of RAM (not upgradable, soldered to the mainboard).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
16GB SSD/Flash-based disk (also not upgradable, soldered to the mainboard).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
ARM Mali T-604 Quad-Core GPU.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Wi-Fi (802.11 a/b/g/n).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Bluetooth 3.0 (sadly, no Bluetooth 4.0 here).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
11" LCD screen at 1366x768 resolution.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
One USB 2.0 port, one USB 3.0 port, one HDMI-out port and one SD card slot.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Optional 3G modem (CDMA, on the Verizon network in the US, equipped
 on this model).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
6.5 hours of (rated) battery life.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Dimensions: 2.4 lbs (1.10 kg), 11.4" x 8.2" x 0.68" (289.6 x 208.5 x 17.5mm).
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Bonus: 100GB of Google Drive included for two years.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;
&lt;p&gt;
Bonus: 100MB/month of Verizon 3G service included for two years (3G model only).
&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11495f1.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Figure 1. The Samsung ARM Chromebook, atop an iPad for Scale
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/exploring-samsung-arm-chromebook-3g" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 19 Sep 2013 18:08:55 +0000</pubDate>
    <dc:creator>Bill Childers</dc:creator>
    <guid isPermaLink="false">1150980 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>There's Browser in My SSH</title>
  <link>https://www.linuxjournal.com/content/theres-browser-my-ssh</link>
  <description>  &lt;div data-history-node-id="1090470" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11513shellf1.png" width="565" height="480" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
No, there's SSH in my browser! Although it may not be as logical of a
combination as chocolate and peanut butter, for Chromebook users, an
HTML5 SSH client is pretty amazing. Granted, Google's "crosh" shell
has SSH abilities, but it's a very limited implementation. With the
Chrome extension "Secure Shell", it's easy to SSH in to remote servers
and interact like a traditional terminal window—mostly.
&lt;/p&gt;

&lt;p&gt;
Secure Shell is getting better all the time, and at the time of this writing,
it supports port forwarding, logging in with keys, socks proxying
and even many keyboard shortcuts for programs like Irssi. The keyboard
shortcut support isn't perfect, but for me at least, it's manageable.
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11513shellf1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Figure 1. It's simple. It's texty. It's awesome.
&lt;/p&gt;

&lt;p&gt;
If you're a Chromebook user and want a real SSH client, give the
"Secure Shell" extension a try. Heck, regardless of the OS you're
using (I'm looking at you, Windows), it's a fast way to get a secure
connection. It's being developed by Google, and it's free via the &lt;a href="http://goo.gl/irXYG"&gt;Play
Store&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/theres-browser-my-ssh" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 17 Jul 2013 21:44:27 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1090470 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
