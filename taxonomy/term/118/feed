<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/nokia">
  <channel>
    <title>Nokia</title>
    <link>https://www.linuxjournal.com/tag/nokia</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>N900 with a Slice of Raspberry Pi</title>
  <link>https://www.linuxjournal.com/content/n900-slice-raspberry-pi</link>
  <description>  &lt;div data-history-node-id="1084297" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11317f2.png" width="640" height="384" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
It may not come as a surprise to anyone who regularly reads my column
that I tried to be first in line to order the Raspberry Pi. I mean, what's
not to like in a $35, 700MHz, 256MB of RAM computer with HDMI out that runs
Linux? In the end, I didn't make the first batch of 10,000, but I wasn't
too far behind either. So, now that I've had a Raspberry Pi for a week,
I've already found a number of interesting uses for it. You can expect
more Raspberry Pi columns from me in the future (possibly including an
update to my beer fridge article), but to start, in this article, I
talk about a combination of two of my favorite pocket-size Linux
computers: the Raspberry Pi and my Nokia N900.
&lt;/p&gt;

&lt;p&gt;
At first you may wonder why combine the two computers. After all, both
are around the same size and have similar initial hardware specs. Each
computer has its own strengths, such as cellular networking and a touchscreen on the N900 and an Ethernet port and HDMI video output on the
Raspberry Pi. In this article, I explain how to connect
the N900 to the Raspberry Pi in a private USB network, share the N900's
cellular connection, and even use the N900 as a pocket-size display. In
all of the examples, I use the default Debian Squeeze Raspberry
Pi image linked off the main &lt;a href="http://www.raspberrypi.org"&gt;http://www.raspberrypi.org&lt;/a&gt;
page.
&lt;/p&gt;
&lt;h3&gt;
Set Up USB Tethering&lt;/h3&gt;

&lt;p&gt;
The first step to using the N900 with the Raspberry Pi is to set up
a private USB network between the two devices. There are a number of
ways to do this, but I've found that the most effective way is via the Mobile
Hotspot application on the N900. This program started as a way to allow
you to tether your computer with your N900 by turning the N900 into a
wireless access point; however, because it uses WEP for security, I always
favored using Mobile Hotspot's lesser-known USB networking option. That
way, I not only get to tether my laptop, but because tethering uses up quite
a bit of battery power, by being plugged in over USB, my laptop can keep
my N900 charged as well.
&lt;/p&gt;

&lt;p&gt;
By default, the Raspberry Pi is not set up to enable USB networking,
but luckily, this is easy to set up. Just log in to your Raspberry Pi and
edit the /etc/network/interfaces file as root. Below where it says:

&lt;/p&gt;&lt;pre&gt;&lt;code&gt;
iface eth0 inet dhcp
&lt;/code&gt;&lt;/pre&gt;


&lt;p&gt;
add:

&lt;/p&gt;&lt;pre&gt;&lt;code&gt;
iface usb0 inet dhcp
&lt;/code&gt;&lt;/pre&gt;


&lt;p&gt;
Now, launch the Mobile Hotspot program on your N900 and make sure it
is configured so that the Interface is set to USB, as shown in Figure
1. Then connect the Raspberry Pi to your N900, which should prompt you to
select between Mass Storage mode or PC Suite mode. Choose PC Suite mode,
and then click the Start button on the Mobile Hotspot GUI. This 
automatically should set up the USB network for you, and you should see logs
like the following in your Raspberry Pi's /var/log/syslog:

&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/n900-slice-raspberry-pi" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 12 Feb 2013 18:19:52 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1084297 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Oh Nokia, We Loved You So...</title>
  <link>https://www.linuxjournal.com/content/oh-nokia-we-loved-you-so</link>
  <description>  &lt;div data-history-node-id="1018028" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/tear.png" width="308" height="261" alt="Microsoft?  Really?" title="Microsoft?  Really?" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Nokia, we invited you into our house. We let you put your feet on the coffee table.  And yet this is how you treat us? Heck, we even tolerated the resistive screen on the N900, because it was a full blown Linux computer.  We even put MeeGo on the &lt;a href="http://www.linuxjournal.com/on-newsstands"&gt;cover of our March issue&lt;/a&gt;, and now you decide to defect to the dark side?  Oh, Nokia...&lt;/p&gt;
&lt;p&gt;As many of you know, today &lt;a href="http://conversations.nokia.com/nokia-strategy-2011/"&gt;Nokia announced&lt;/a&gt; that they've abandoned Linux, and partnered with Microsoft for their future phones.  While current Linux based phones (Like our own Kyle Rankin's N900) will continue to identify with freedom, any future offerings from Nokia will be all Windowsy.  For those of us interested in Linux based handsets, our choices have been seriously decreased.  Android is great, but competition within the Linux community is great too.  Competition often sparks innovation.  Sadly there's little we can do but weep.  Well, that and look for another hardware vendor to love.&lt;/p&gt;
&lt;p&gt;Oh, and Nokia?  Breaking up with the Linux community 3 days before Valentine's Day is just tacky.  You still owe us dinner and a movie.&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/oh-nokia-we-loved-you-so" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 11 Feb 2011 17:21:29 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1018028 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
