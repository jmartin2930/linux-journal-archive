<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/blurry-fonts">
  <channel>
    <title>blurry fonts</title>
    <link>https://www.linuxjournal.com/tag/blurry-fonts</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Xubuntu 11.10 and my Netbook</title>
  <link>https://www.linuxjournal.com/content/xubuntu-1110-and-my-netbook</link>
  <description>  &lt;div data-history-node-id="1026292" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/netbook_crop_200.jpeg" width="200" height="222" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;October saw the release of the latest version of the Ubuntu family and that includes &lt;a href="http://www.xubuntu.org/"&gt;Xubuntu&lt;/a&gt;, the Xfce edition. I’ve just installed Xubuntu 11.10 on my netbook and the experience was rather good.&lt;/p&gt;
&lt;p&gt;The netbook in question is an eMachines (Acer) model eM350. The specs are: 1.6GHz Atom processor, 1GB of memory and a 160GB hard drive. I'd been using it for a couple of months with the default installation of Windows XP.&lt;/p&gt;
&lt;p&gt;In this case, I installed Xubuntu by downloading the ISO as usual and using &lt;a href="http://unetbootin.sourceforge.net/"&gt;Unetbootlin&lt;/a&gt; to copy it to a flashdrive.&lt;/p&gt;
&lt;p&gt;With each release, Xfce becomes more and more comprehensive. Xubuntu 11.10 uses Xfce 4.8 (&lt;a href="http://www.linuxjournal.com/content/xfce-48-lightweight-desktop-environment"&gt;covered here&lt;/a&gt;), and overall, the dark default Xubuntu theme looks attractive. As I've speculated in the past, I think that Xfce is going to become home to more and more users who want something that is under active development, yet looks and works like a Gnome 2, KDE 3 or even Windows XP desktop. In its default configuration, the task switcher, application launcher menu and status area are located on a bar at the top of screen. Moving the pointer to the bottom of the screen causes an icon-based application bar to appear, an efficient use of space on a netbook display as it retracts when not in use.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/xubuntu1110_600.png" /&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;A layout that strikes a good balance between a traditional desktop and a space-constrained one.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;As good as things looked initially, I began to notice a problem on the visual front: the font rendering looked rather blurry. Fortunately, a Google search turned up &lt;a href="http://johan.kiviniemi.name/blag/ubuntu-fonts/"&gt;this neat little hack&lt;/a&gt;. Creating the two configuration files, as directed, altered the way that anti-aliasing is applied to fonts, giving them a crisp, smooth, if slightly heavy appearance. The default setup of the font rendering was so poor, that I honestly don't think I could have used it in that condition on a long term basis. Without the fix, text also had a misty quality when I installed Xubuntu within a VM on a desktop machine. I couldn't detect the same problem when I installed Ubuntu 11.10.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/xubuntu11_10_misty_fonts_crop.png" /&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Xubuntu 11.10 installed in VirtualBox. Notice the misty quality of the Xubuntu text compared with the Kubuntu native rendering.&lt;/em&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/xubuntu-1110-and-my-netbook" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 31 Oct 2011 15:32:47 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1026292 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
