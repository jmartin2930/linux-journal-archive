<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/raspberry-pi-zero">
  <channel>
    <title>Raspberry Pi Zero</title>
    <link>https://www.linuxjournal.com/tag/raspberry-pi-zero</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Indie Makers Using Single-Board Computers</title>
  <link>https://www.linuxjournal.com/content/indie-makers-using-single-board-computers</link>
  <description>  &lt;div data-history-node-id="1340457" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/IMG_20180917_152343.jpg" width="800" height="600" alt="TinyPi" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/bryan-lunduke" lang="" about="https://www.linuxjournal.com/users/bryan-lunduke" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Bryan Lunduke&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Possibly the most amazing thing, to me, about single board computers
(SBCs) is that they allow small teams of people (and even lone
individuals) to create new gadgets using not much more than SBCs and 3D
printers. That opportunity for makers and small companies is absolutely
astounding.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Two such projects have really caught my attention lately: the &lt;a href="http://www.noodlepi.com"&gt;Noodle Pi&lt;/a&gt; and
the &lt;a href="https://www.tindie.com/products/petay/tinypi-a-tiny-pi-based-gaming-device"&gt;TinyPi&lt;/a&gt;.
&lt;/p&gt;

&lt;p&gt;
The Noodle Pi is a simple, handheld computer (about the size of a deck of
playing cards). And, when I say simple, I mean &lt;em&gt;simple&lt;/em&gt;. It's got a
micro-USB charging port, another for plugging in USB devices, a touch
screen and a battery. Think of it like an old-school PDA without any
buttons (other than a small power toggle) and the ability to run a full
Linux-based desktop.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/noodle-pi.jpg" width="650" height="334" alt="Noodle Pi" class="image-max_650x650" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. The Credit-Card-Sized, Pi Zero-Powered, Noodle Pi&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
The TinyPi is a gaming handheld. And, believe it or not, it's even smaller
than the Noodle Pi, with a tiny screen and tiny buttons. This is the sort of
handheld game console you could put on a keychain.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/IMG_20180917_152343.jpg" width="650" height="488" alt="TinyPi" class="image-max_650x650" /&gt;&lt;p&gt;&lt;em&gt;Figure 2. The Impossibly Small TinyPi (Banana for
Scale)&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Both of these are built on top of the (super-tiny and super-cheap)
Raspberry Pi Zero. And, both are built by lone individuals with a heavy
reliance on 3D printers.
&lt;/p&gt;

&lt;p&gt;
I wanted to know how they did it and how their experience was. What can we
learn from these independent gadget makers? So, I reached out to both of
them and asked them each the same questions (more or less).
&lt;/p&gt;

&lt;p&gt;
Let's start with a chat with Pete Barker (aka "&lt;a href="https://pi0cket.com"&gt;pi0cket&lt;/a&gt;"), maker of the TinyPi.
&lt;/p&gt;

&lt;h3&gt;Interview with Pete Barker (pi0cket), TinyPi Maker&lt;/h3&gt;

&lt;p&gt;
&lt;strong&gt;Bryan Lunduke:&lt;/strong&gt; Could you give a quick overview of the TinyPi?
&lt;/p&gt;

&lt;p&gt;
&lt;strong&gt;Pete Barker:&lt;/strong&gt; TinyPi is (unofficially) the world's smallest pi-based gaming device.
It started life as a bit of a joke—"how small can i make
this?"—but
it actually turned into something pretty good. The Pro version added more
features and improvements, and a kickstarter was funded on December 30,
2018. Manufacturing is already underway, and the early-bird backers should
start getting the kits in February 2019.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/IMG_20181207_211718_HHT.jpg" width="650" height="488" alt="TinyPi parts" class="image-max_650x650" /&gt;&lt;p&gt;&lt;em&gt;Figure 3. The parts of the TinyPi—the
Smallest Handheld Game Console I Can Possibly Imagine&lt;/em&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/indie-makers-using-single-board-computers" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 28 Feb 2019 14:31:41 +0000</pubDate>
    <dc:creator>Bryan Lunduke</dc:creator>
    <guid isPermaLink="false">1340457 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
