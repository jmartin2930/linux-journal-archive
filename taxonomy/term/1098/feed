<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/hcs">
  <channel>
    <title>HCS</title>
    <link>https://www.linuxjournal.com/tag/hcs</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Hybrid Cloud Storage Delivers Performance and Value </title>
  <link>https://www.linuxjournal.com/content/hybrid-cloud-storage-delivers-performance-and-value</link>
  <description>  &lt;div data-history-node-id="1339342" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/cloud_2.jpg" width="800" height="533" alt="Hybrid Cloud Storage" title="Hybrid Cloud Storage" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/ted-schmidt" lang="" about="https://www.linuxjournal.com/users/ted-schmidt" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Ted Schmidt&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;a href="http://ibm.biz/HCS-title-LJ8"&gt;Hybrid Cloud Storage Delivers Performance and Value&lt;/a&gt;
&lt;br /&gt;Just in the past two years, technology has created a veritable ocean of data. And like an ocean, that data, created by social technology, mobile technology and IoT, is vast, bountiful and dynamic. And also like an ocean, the climate, or temperature of data, constantly shifts and changes. Chances are that your enterprise has perhaps not an ocean, but certainly a sea or lake of data readily available. The challenge is how to measure the temperature, navigate the vastness of that data and reap its waiting bounty.
&lt;p&gt;
&lt;/p&gt;
Data generally can be grouped into three classifications: hot, warm and cold. Hot data is data (such as customer behavior) that is accessed frequently and so needs to be available immediately. Cold data (for example, prior year financials), on the other hand, is used infrequently, and it does not need to be accessed as often. Warm data is somewhere in between, and as you can imagine, each class of data has different storage requirements. And, like the shifting nature of an ocean, your data may move between those classifications, making the challenge of storing and accessing your data in the most cost-effective way a huge challenge, and opportunity.
&lt;p&gt;
&lt;/p&gt;
Most enterprises have relied on one of two general methods for storing data: on-site or cloud. Each of these approaches has its merits, but using one or the other also creates certain constraints for your enterprise. On-site storage provides all the benefits of localized control, but as your data grows or shifts between hot and cold, it can present cost issues. To respond to the cost and scalability issues, your enterprise can move its data to the cloud, but potentially at the risk of availability or regulatory compliance, like PII (personally identifiable information).
&lt;p&gt;
&lt;/p&gt;
But if your enterprise could blend the two--on-site and cloud storage--in a way that helped manage down costs while providing for your enterprise availability, scalability, security and compliance needs, it would provide a viable solution. &lt;a href="http://ibm.biz/HCS-inside-art-LJ8"&gt;Hybrid Cloud Storage (HCS)&lt;/a&gt;  creates a perfect method for your enterprise to place data exactly where it makes sense, depending on its class, and it helps manage costs effectively. 
&lt;p&gt;
&lt;/p&gt;
&lt;a href="https://bs.serving-sys.com/serving/adServer.bs?cn=trd&amp;mc=click&amp;pli=20904384&amp;PluID=0&amp;ord=[timestamp]"&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u800391/HCS_banner_300x250_LP_V1.jpg" alt="HPC IBM" title="" class="imagecache-large-550px-centered" /&gt;&lt;/a&gt;&lt;img src="https://bs.serving-sys.com/serving/adServer.bs?cn=display&amp;c=19&amp;mc=imp&amp;pli=20904384&amp;PluID=0&amp;ord=[timestamp]&amp;rtu=-1" /&gt;&lt;p&gt;
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/hybrid-cloud-storage-delivers-performance-and-value" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 29 Mar 2017 11:23:29 +0000</pubDate>
    <dc:creator>Ted Schmidt</dc:creator>
    <guid isPermaLink="false">1339342 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
