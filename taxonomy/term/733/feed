<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/gnome-3">
  <channel>
    <title>gnome 3</title>
    <link>https://www.linuxjournal.com/tag/gnome-3</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Gnome 3.2 More Evolution than Revolution</title>
  <link>https://www.linuxjournal.com/content/gnome-32-more-evolution-revolution</link>
  <description>  &lt;div data-history-node-id="1025350" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/gnome_3_2_boot_200.png" width="200" height="150" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Gnome 3.2 has been &lt;a href="http://www.gnome.org/news/2011/09/gnome-3-improved-and-refined-with-the-release-of-gnome-3-2/"&gt;released&lt;/a&gt;. This time around, the developers have focused on a large number of small improvements rather than big, headline features. That said, there are a couple of interesting new additions in the areas of web integration and personal data management.&lt;/p&gt;
&lt;p&gt;Adhering to a six month release schedule, this is the first major Gnome release since version 3.0 debuted in April this year. Rival desktop environments KDE4 (released Jan 2008) and Gnome 3 have something in common in that they were both quite heavily criticized upon release. However, the situation isn’t exactly the same because KDE 4.0 was, arguably, unusable upon release in addition to being a huge departure from the previous series. By contrast, most of the criticism of Gnome 3 has centered on the new user interface and seems to be subject to personal taste. Therefore, it’s not surprising that most of the work seems to have gone into improving refining the new shell.&lt;/p&gt;
&lt;p&gt;Sure enough, in the opening section of the official Gnome 3.2 announcement, entitled “Evolution” we are told:&lt;/p&gt;
&lt;p&gt;&lt;em&gt;“Based on user feedback, lots of small changes have been made to give a smoother experience in GNOME 3.2.”&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;See the &lt;a href="http://library.gnome.org/misc/release-notes/3.2/#evolved"&gt;appropriate section&lt;/a&gt; of the the release notes for the full list of improvements. Notable additions include the new Contacts application, the instant image previews in the file manager and the new color management feature. Generally, everything has been spruced up, refined or subjected to greater integration with the whole.&lt;/p&gt;
&lt;p&gt;The default theme is a dark one, and overall, Gnome 3.2 now shares quite a bit with KDE4 in the looks department.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/gnome_3_2_boot_600.jpg" /&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Dark blue hues are prominent in the default scheme of the Suse Gnome 3.2 Live CD.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;However, despite the focus on refinement rather than new features per se there are two fairly major new features that both relate to web integration.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/gnome_3_2_desktop_crop_600.png" /&gt;&lt;br /&gt;&lt;strong&gt;&lt;br /&gt;&lt;/strong&gt;&lt;em&gt;Sticking with plan A: The old task bar and application launcher of  Gnome 2 have been replaced by icons that run down the side of the  screen. Some love it; some hate it.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Online Accounts&lt;/strong&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/gnome-32-more-evolution-revolution" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 03 Oct 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1025350 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
