<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/librem-13">
  <channel>
    <title>Librem 13</title>
    <link>https://www.linuxjournal.com/tag/librem-13</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Hello Again, Linux</title>
  <link>https://www.linuxjournal.com/content/hello-again-linux</link>
  <description>  &lt;div data-history-node-id="1340638" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-isolated-laptop-114393053.jpg" width="900" height="669" alt="Linux on a Laptop" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/richard-mavis" lang="" about="https://www.linuxjournal.com/users/richard-mavis" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Richard Mavis&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;My first MacBook was the first computer I really loved, but I wasn't happy
about the idea of buying a new one.
I decided it's important to live your values and to
support groups that value the things you do.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
After ten years of faithful service, last year the time finally came to
retire my MacBook. Not many laptops last ten years—not many companies
produce a machine as durable and beautiful as Apple does—but, if one
was available, I was willing to invest in a machine that might last me
through the next ten years.
A lot has changed in ten years—for Apple, for Linux and for
myself—so
I started looking around.
&lt;/p&gt;

&lt;h3&gt;The Situation&lt;/h3&gt;

&lt;p&gt;
Prior to 2006, I had used only Windows. Around that time, there was a lot
of anxiety about its upcoming successor to Windows XP, which at the time
was code-named Project Longhorn. My colleagues and I all were dreading
it. So, rather than go through all that trouble, I switched to Linux.
&lt;/p&gt;

&lt;p&gt;
However, my first experience with Linux was not great. Although 2006 was &lt;em&gt;The Year of
the Linux Desktop&lt;/em&gt; (I saw headlines on Digg proclaiming it almost every
day), I quickly learned, right after wiping my brand-new laptop's
hard drive to make way for Fedora, that maybe it wasn't quite &lt;em&gt;The Year
of the Linux Laptop&lt;/em&gt;. After a desperate and miserable weekend, I finally
got my wireless card working, but that initial trauma left me leery. So,
about a year later, when I decided to quit my job and try the digital
nomad freelance thing, I bought a MacBook. A day spent hunting down
driver files or recompiling my kernel was a day not making money. I
needed the assurance and convenience Apple was selling. And it proved
a great investment.
&lt;/p&gt;

&lt;p&gt;
During the next decade, I dabbled with Linux. Every year seemed to
be The Year of the Linux Desktop—the real one, at last—so
on my desktop at work (freelancing wasn't fun for long), I
installed Ubuntu, then Debian, then FreeBSD. An article in this
journal introduced me to tiling window managers in general and
&lt;a href="https://www.linuxjournal.com/content/going-fast-dwm"&gt;DWM&lt;/a&gt;
in
particular. The first time I felt something like disappointment with my
MacBook was after using DWM on Debian for the first time.
&lt;/p&gt;

&lt;p&gt;
Through the years, as my MacBook's hardware failures became increasingly
inconvenient, and as my personal preference in software shifted from big
beautiful graphical applications to small command-line programs, Linux
started to look much more appealing. And, Linux's hardware compatibility
had expanded—companies had even started selling laptops with Linux
already installed—so I felt reasonably sure I wouldn't need to waste
another weekend struggling with a broken wireless connection or risk
frying my monitor with a misconfigured Xorg.conf.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/hello-again-linux" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 31 May 2019 13:26:34 +0000</pubDate>
    <dc:creator>Richard Mavis</dc:creator>
    <guid isPermaLink="false">1340638 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Review: the Librem 13v2</title>
  <link>https://www.linuxjournal.com/content/review-librem-13v2</link>
  <description>  &lt;div data-history-node-id="1339838" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12366f1.jpg" width="800" height="579" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
The Librem 13—"the first 13-inch ultraportable designed to
protect your digital life"—ticks all the boxes, but is it as good in real life
as it is on paper?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
I don't think we're supposed to call portable computers "laptops"
anymore. There's something about them getting too hot to use safely on your lap,
so now they're officially called "notebooks" instead. I must be a thrill-seeker
though,
because I'm writing this review with the Librem 13v2 directly on my
lap. I'm wearing pants, but apart from that, I'm risking it all for the
collective. The first thing I noticed about the Librem 13? The company
refers to it as a laptop. Way to be brave, Purism!
&lt;/p&gt;

&lt;h3&gt;
Why the Librem?&lt;/h3&gt;

&lt;p&gt;
I have always been a fan of companies who sell laptops (er, notebooks)
pre-installed with Linux, and I've been considering buying a Purism laptop for years.
When our very own Kyle Rankin started working for the company, I figured
a company smart enough to hire Kyle deserved my business, so I ordered
the Librem 13 (Figure 1). And when I ordered it, I discovered I could pay with
Bitcoin, which made me even happier!
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/12366f1.jpg" width="1300" height="941" alt="Photo of Librem 13" Laptop" class="image-max_1300x1300" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. The 13" Librem 13v2 is the perfect size for taking on the road (photo from
&lt;a href="https://puri.sm"&gt;Purism&lt;/a&gt;)&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
There are other reasons to choose Purism computers too. The company is extremely
focused on privacy, and it goes so far as to have hardware switches that turn
off the webcam and WiFi/Bluetooth radios. And because they're designed for
open-source operating systems, there's no "Windows" key; instead there's a
meta key with a big white rectangle on it, which is called the Purism Key
(Figure 2). On top of all those things, the computer itself is rumored
to be extremely well built, with all the bells and whistles usually
available only on high-end top-tier brands.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/12366f2.png" width="700" height="257" alt="No Windows key here! This beats a sticker-covered Windows logo any day." class="image-max_1300x1300" /&gt;&lt;p&gt;
&lt;em&gt;Figure 2. No Windows key here! This beats a sticker-covered Windows logo any day (photo
from &lt;a href="https://puri.sm"&gt;Purism&lt;/a&gt;).&lt;/em&gt;
&lt;/p&gt;

&lt;h3&gt;
My Test Unit&lt;/h3&gt;

&lt;p&gt;
Normally when I review a product, I get whatever standard model the
company sends around to reviewers. Since this was going to be my actual
daily driver, I ordered what I wanted on it. That meant the following:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/review-librem-13v2" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 03 May 2018 12:00:00 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1339838 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
