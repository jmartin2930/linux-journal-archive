<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/debian-gnu/linux">
  <channel>
    <title>Debian GNU/Linux</title>
    <link>https://www.linuxjournal.com/tag/debian-gnu/linux</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Is It Linux or GNU/Linux?</title>
  <link>https://www.linuxjournal.com/content/it-linux-or-gnulinux</link>
  <description>  &lt;div data-history-node-id="1339859" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Penguin-936090.jpg" width="399" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/christine-hall" lang="" about="https://www.linuxjournal.com/users/christine-hall" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Christine Hall&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;After putting this question to the experts, the conclusion is that no
matter what you call it, it's still Linux at its core.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Should the Linux operating system be called "Linux" or "GNU/Linux"? These
days, asking that question might get as many blank stares returned as
asking, "Is it live or is it Memorex?"
&lt;/p&gt;

&lt;p&gt;
Some may remember that the Linux naming convention was a controversy
that raged from the late 1990s until about the end of the first decade
of the 21st century. Back then, if you called it "Linux", the GNU/Linux crowd
was sure to start a flame war with accusations that the GNU Project wasn't
being given due credit for its contribution to the OS. And if you called it
"GNU/Linux",
accusations were made about political correctness, although operating
systems are pretty much apolitical by nature as far as I can tell.
&lt;/p&gt;
&lt;p&gt;
The brouhaha got started in the mid-1990s when Richard Stallman, among
other things the founder of the Free Software Movement who penned the
General Public License, began insisting on using the term "GNU/Linux"
in recognition of the importance of the GNU Project to the OS. GNU was
started by Stallman as an effort to build a free-in-every-way operating
system based on the still-not-ready-for-prime-time Hurd microkernel.
&lt;/p&gt;

&lt;p&gt;
According to this take, Linux was merely the kernel, and GNU software
was the sauce that made Linux work.
&lt;/p&gt;

&lt;p&gt;
Noting that the issue seems to have died down in recent years, and mindful
of Shakespeare's observation on roses, names and smells, I wondered if
anyone really cares anymore what Linux is called. So, I put the issue
to a number of movers and shakers in Linux and open-source circles by
asking the simple question, "Is it GNU/Linux or just plain Linux?"
&lt;/p&gt;

&lt;p&gt;
"This has been one of the more ridiculous debates in the FOSS realm,
far outdistancing the Emacs-vi rift", said Larry Cafiero, a longtime
Linux advocate and FOSS writer who pulls publicity duties at the
Southern California Linux Expo. "It's akin to the Chevrolet-Chevy
moniker. Technically the car produced by GM is a Chevrolet, but
rarely does anyone trot out all three syllables. It's a Chevy. Same
with the shorthand for GNU/Linux being Linux. The shorthand version—the
Chevy version—is Linux. If you insist in calling it a Chevrolet,
it's GNU/Linux."
&lt;/p&gt;

&lt;p&gt;
Next up was Steven J. Vaughan Nichols, who's "been covering Unix since
before Linux was a grad student". He didn't mince any words.
&lt;/p&gt;

&lt;p&gt;
"Enough already", he said. "RMS tried, and failed, to create an operating
system: Hurd. He and the Free Software Foundation's endless attempts to
plaster his GNU name to the work of Linus Torvalds and the other Linux
kernel developers is disingenuous and an insult to their work. RMS gets
credit for EMACS, GPL, and GCC. Linux? No."
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/it-linux-or-gnulinux" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 11 May 2018 13:08:08 +0000</pubDate>
    <dc:creator>Christine Hall</dc:creator>
    <guid isPermaLink="false">1339859 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Spotlight on Linux:  Debian GNU/Linux 6.0 "Squeeze"</title>
  <link>https://www.linuxjournal.com/content/spotlight-linux-debian-gnulinux-60-squeeze</link>
  <description>  &lt;div data-history-node-id="1018370" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/debian_6_kde-small.jpg" width="550" height="309" alt="Debian GNU/Linux 6.0 " title="Debian GNU/Linux 6.0 " typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/susan-linton" lang="" about="https://www.linuxjournal.com/users/susan-linton" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Susan Linton&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://www.debian.org"&gt;Debian&lt;/a&gt; is a bit unique in the Linux world.  It's one of the most respected projects, it's one of the oldest distributions, and it is one of the most versatile systems.  Debian comes in more architectures and more installation methods than most any other.  It offers one of the widest selections of software available.  In fact, it's often referred to as the Universal Operating System.  It took two years, but &lt;a href="http://www.debian.org/News/2011/20110205a"&gt;version 6.0&lt;/a&gt; finally emerged to what many say would say was worth the wait.&lt;/p&gt;
&lt;p&gt;This release brings lots of updates to its software stack.  These include goodies such as KDE 4.4.5, Xfce 4.6, LXDE 0.5.0, X.Org 7.5, OpenOffice.org 3.2.1, GIMP 2.6.11, IceWeasel 3.5.16, GCC 4.4.5, and Linux 2.6.32.  Other changes include a total free (as in freedom) kernel; all proprietary code has been removed (as much as possible), although much of it is still available to install separately.  The installer has been streamlined and is believed to be friendlier and now includes support for ext4 and Btrfs.   Although for those with both SATA and older ATA drives might find booting the new install challenging when using non-Debian bootloaders.  If using the new DVD, a plethora of software is installed at boot.  &lt;/p&gt;
&lt;p&gt;Debian comes in a variety of install methods.  One can install from one or more traditional CDs, a network install image, USB, or the new cornucopia DVD.  Debian can also be installed on wide range of machines from the common AMD and Intel 32-bit and 64-bit desktops and laptops, to ARM and MIPS embedded devices, SPARC, Itanium, PowerPC, and more.  This release even brought 32-bit and 64-bit kFreeBSD versions.  There are several "&lt;a href="http://blends.alioth.debian.org/"&gt;Blends&lt;/a&gt;," or customized versions, as well.  Debian Accessibility, Debian Science, and Debian Multimedia are a few.&lt;/p&gt;
&lt;p&gt;To many, Debian is the perfect server OS.  Its rock steady stability, timely updates, and easy package management are just a few of the positive characteristics.  Debian may do some things a little differently than other distributions, but once learned, Debian is almost out-of-the-box ready and practically self-sustaining.  &lt;/p&gt;
&lt;p&gt;Desktop operation isn't much different.  Rock solid stability, timely updates, and easy package management are top reasons Debian is used on a large number of desktops and laptops.  This release brought a new theme that has received mixed reviews.  Most seem to like the space motif, while a few others say it's too child-like.  No matter, Debian ships with a number of alternative theme elements.  &lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/spotlight-linux-debian-gnulinux-60-squeeze" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 03 Mar 2011 14:00:00 +0000</pubDate>
    <dc:creator>Susan Linton</dc:creator>
    <guid isPermaLink="false">1018370 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
