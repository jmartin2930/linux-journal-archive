<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/popcon">
  <channel>
    <title>popcon</title>
    <link>https://www.linuxjournal.com/tag/popcon</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Popcon - Are You In Or Out?</title>
  <link>https://www.linuxjournal.com/content/popcon-are-you-or-out</link>
  <description>  &lt;div data-history-node-id="1017753" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/popcon_cropped.png" width="556" height="283" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Those of you who regularly install Debian may have noticed a prompt that asks you if you would like to install Popcon, the Debian Popularity Contest. Popcon gathers statistics about package usage and periodically submits it to Debian. The anonymous statistics gathered by the script are freely available on the Debian website, and the script can be invoked manually to give a clearer idea of package usage on your own system.&lt;/p&gt;
&lt;p&gt;I must admit that I had always declined to take part in the survey. Some people will object on privacy grounds, but personally, I trust that Debian aren't going to do anything devious with the info. I had opted out because it sounded like another possible point of failure and didn't actually know what the project did.&lt;/p&gt;
&lt;p&gt;If you didn't select it when installing Debian, you can install Popcon at any time via the package manager, and this doesn't hamper the quality of the data. If you're installing it manually, bear in mind that it installation script prompts for user input, so make sure that you can view the text output of your package management system. The information that it is actually gathering is the installation date and most recent access date of every package on your system. By default, Popcon gathers the information and submits it once a week using a cron job.&lt;/p&gt;
&lt;p&gt;Once installed, you can invoke it automatically by typing (as root) &lt;/p&gt;
&lt;p&gt;&lt;code&gt;popularity-contest&lt;/code&gt;&lt;/p&gt;
&lt;p&gt;You'll receive a long list of all of the packages on your system arranged in order of most recently accessed. Here is a sample of the output when I ran it on my Debian Sid box.&lt;/p&gt;
&lt;blockquote&gt;&lt;p&gt;
1290877204 1290877209 iptables /usr/sbin/ip6tables-apply OLD&lt;br /&gt;
1290877204 1290877339 ed /usr/bin/red OLD&lt;br /&gt;
1290877204 1290877401 laptop-detect /usr/sbin/laptop-detect OLD&lt;br /&gt;
1290877204 1290877230 libnfsidmap2 /usr/lib/libnfsidmap/static.so OLD&lt;br /&gt;
1290877204 1290877414 libruby1.8 /usr/lib/ruby/1.8/net/ftp.rb OLD&lt;br /&gt;
1290877204 1290877455 google-gadgets-gst /usr/lib/google-gadgets/modules/gst-audio-framework.so OLD&lt;br /&gt;
1290877204 1290877246 tcpd /usr/sbin/tcpd OLD
&lt;/p&gt;&lt;/blockquote&gt;
&lt;p&gt;The first two numbers are the access and the creation time of the most recently accessed file within the library. The time is presented in Unix time format, that is, number of seconds elapsed since midnight January 1970. This is followed by the name of the library and the most recently accessed file in that library. The last piece of information is a tag which indicates if that library is considered old (not accessed for more than a month). There are tags to indicate if the library is recently installed or contains no runnable programs. &lt;/p&gt;
&lt;p&gt;Obviously, the output for a typical system is going to be vast. For this reason, if you're invoking it from the command line, either piping to a file or grep is the best approach. For example, piping it to a file with&lt;/p&gt;
&lt;p&gt;&lt;code&gt;popularity-contest &gt;popcon.txt&lt;/code&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/popcon-are-you-or-out" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 31 Jan 2011 14:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1017753 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
