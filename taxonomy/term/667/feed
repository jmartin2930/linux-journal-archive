<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/javascript">
  <channel>
    <title>JavaScript</title>
    <link>https://www.linuxjournal.com/tag/javascript</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>JavaScript All the Way Down</title>
  <link>https://www.linuxjournal.com/content/javascript-all-way-down</link>
  <description>  &lt;div data-history-node-id="1338679" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11808f1.png" width="640" height="442" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/user/1001710" lang="" about="https://www.linuxjournal.com/user/1001710" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Federico Kereki&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
There is a well known story about a scientist who gave a talk about the
Earth and its place in the solar system. At the end of the talk, a woman
refuted him with "That's rubbish; the Earth is really like a flat dish,
supported on the back of a turtle." The scientist smiled and asked back
"But what's the turtle standing on?", to which the woman, realizing
the logical trap, answered, "It's very simple: it's turtles all the
way down!" No matter the verity of the anecdote, the identity of the
scientist (Bertrand Russell or William James are sometimes mentioned),
or even if they were turtles or tortoises, today we may apply a similar
solution to Web development, with "JavaScript all the way down".
&lt;/p&gt;
&lt;p&gt;
If you are going to develop a Web site, for client-side development, you could
opt for Java applets, ActiveX controls, Adobe Flash animations and,
of course, plain JavaScript. On the other hand, for server-side coding,
you could go with C# (.Net), Java, Perl, PHP and more, running on
servers, such as Apache, Internet Information Server, Nginx, Tomcat and
the like. Currently, JavaScript allows you to do away with most of this
and use a single programming language, both on the client and the server
sides, and with even a JavaScript-based server. This way of working 
even has produced a totally JavaScript-oriented acronym along the lines of
the old LAMP (Linux+Apache+MySQL+PHP) one: MEAN, which stands for MongoDB
(a NoSQL database you can access with JavaScript), Express (a Node.js
module to structure your server-side code), Angular.JS (Google's Web
development framework for client-side code) and Node.js.
&lt;/p&gt;

&lt;p&gt;
In this article, I cover several JavaScript tools for writing,
testing and deploying Web applications, so you can consider whether
you want to give a twirl to a "JavaScript all the way down" Web stack.
&lt;/p&gt;


&lt;h3&gt;
What's in a Name?&lt;/h3&gt;

&lt;p&gt;
JavaScript originally was developed at Netscape in 1995, first under
the name Mocha, and then as LiveScript. Soon (after Netscape and Sun got
together; nowadays, it's the Mozilla Foundation that manages the language)
it was renamed JavaScript to ride the popularity wave, despite having
nothing to do with Java. In 1997, it became an industry standard under a
fourth name, ECMAScript. The most common current version of JavaScript is
5.1, dated June 2011, and version 6 is on its way. (However, if you want
to use the more modern features, but your browser won't support them, take
a look at the Traceur compiler, which will back-compile version 6 code
to version 5 level.) 
&lt;/p&gt;

&lt;p&gt;
Some companies produced supersets of the language,
such as Microsoft, which developed JScript (renamed to avoid legal problems)
and Adobe, which created ActionScript for use with Flash. 
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/javascript-all-way-down" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 09 Apr 2015 17:41:57 +0000</pubDate>
    <dc:creator>Federico Kereki</dc:creator>
    <guid isPermaLink="false">1338679 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Javascript PC emulator runs Linux</title>
  <link>https://www.linuxjournal.com/content/javascript-pc-emulator-runs-linux</link>
  <description>  &lt;div data-history-node-id="1021252" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/js_pcem_resize.png" width="640" height="467" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Fabrice Bellard, creator of the multiple architecture emulator QEMU and FFmpeg, amongst other open source projects, has &lt;a href="http://bellard.org/jslinux/tech.html"&gt;unleashed&lt;/a&gt; his &lt;a href="http://en.wikipedia.org/wiki/Javascript"&gt;Javascript&lt;/a&gt; powered PC emulator. In its current state, it boots a stripped down, text mode Linux implementation and runs within a modern browser. Interesting curio or a potentially useful tool?&lt;/p&gt;
&lt;p&gt;To say it's written in Javascript, an &lt;a href="http://en.wikipedia.org/wiki/Interpreted_language]"&gt;interpreted language&lt;/a&gt; that is usually used for client-side scripting on web pages, the emulator runs remarkably quickly. It boots to a command prompt in about a minute on this machine (Sempron 3000, FF 4.1). [&lt;strong&gt;Update: Considerably faster than that on my CoreDuo&lt;/strong&gt;] By the way, the emulator requires a modern browser such as Firefox 4 or Chrome in order to operate.&lt;/p&gt;
&lt;p&gt;Things are still at the proof of concept stage but there is already enough to play around with. Sitting on a command line prompt, the first thing I tried was typing &lt;em&gt;ls&lt;/em&gt;. Doing this revealed the presence of a single file, a C source code file called &lt;em&gt;hello.c&lt;/em&gt;. Attempting to compile with GCC wont work because GCC isn't installed. Examining the content of the file got the the bottom of things:&lt;/p&gt;
&lt;p&gt;&lt;em&gt;~ # cat hello.c &lt;br /&gt;/* This C source can be compiled with: &lt;br /&gt;tcc -o hello hello.c &lt;br /&gt;*/ &lt;br /&gt;#include &lt;tcclib.h&gt; &lt;br /&gt;int main(int argc, char **argv) &lt;br /&gt;{ &lt;br /&gt;printf("Hello World\n"); &lt;br /&gt;return 0; &lt;br /&gt;} &lt;br /&gt;~ # &lt;/em&gt;&lt;/p&gt;
&lt;p&gt;That the system includes the &lt;a href="http://en.wikipedia.org/wiki/Tiny_C_Compiler"&gt;Tiny C Compiler&lt;/a&gt; (not to be confused with &lt;a href="http://en.wikipedia.org/wiki/Small_C"&gt;Small-C&lt;/a&gt;) makes sense as that is another project that was started by Bellard. You can get an idea of what other utilities are supported by the system by typing&lt;/p&gt;
&lt;p&gt;&lt;em&gt;ls /bin&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;So, what could this thing actually be used for, I hear you ask? As it stands, not all that much. For one thing, networking is not yet emulated. To perform any type of file transfer between the host and guest environments, one has to use the system cut and paste buffer and a virtual &lt;em&gt;/dev/clipboard&lt;/em&gt; device. Other than that, there's no way to get data into or out of the system. This could be a point in the system's favor because, as it really is running locally, rather than on a server, it's fairly good in privacy terms. You can wipe the entire system by simply hitting refresh.&lt;/p&gt;
&lt;p&gt;It could perhaps be used to provide some sort of training environment to teach people how to use the Linux command line. As it stands, the system could be used to compile simple snippets of C code if you were to find yourself stuck on a machine without a compiler installed.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/javascript-pc-emulator-runs-linux" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 09 Jun 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1021252 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
