<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/linux-journal-20">
  <channel>
    <title>Linux Journal 2.0</title>
    <link>https://www.linuxjournal.com/tag/linux-journal-20</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Join the Linux Journal Crusade</title>
  <link>https://www.linuxjournal.com/content/join-linux-journal-crusade</link>
  <description>  &lt;div data-history-node-id="1340110" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/image2.jpeg" width="600" height="600" alt="Kyle and Mark" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Linux Journal&lt;/em&gt; has been reporting on Linux every month since version 1.0 in April 1994.&lt;/p&gt;

&lt;p&gt;Through the nearly 25 years that have passed since then, Linux has come to support approximately everything an operating system can, while &lt;em&gt;Linux Journal&lt;/em&gt; has maintained its status as the leading magazine covering Linux and all Linux does (or at least as much as we can fit into more pages than ever).&lt;/p&gt;

&lt;p&gt;Here is where Linux currently stands among the world's operating systems (stats via the Linux Foundation):&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;#1 Internet client (Android).&lt;/li&gt;
	&lt;li&gt;82% of the smartphone market share (Android again).&lt;/li&gt;
	&lt;li&gt;100% share of the supercomputer market.&lt;/li&gt;
	&lt;li&gt;90% share of mainframe customers.&lt;/li&gt;
	&lt;li&gt;90% of the public cloud workload.&lt;/li&gt;
	&lt;li&gt;62% of the embedded systems market.&lt;/li&gt;
	&lt;li&gt;#2 to Windows in enterprise.&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Linux is also at the base of countless open-source software stacks, which in turn support vast sums of productivity and economic benefit to countless verticals. Telecom, retail, automotive, energy, transportation, medicine, networking, entertainment and pharma are just a few of the big familiar ones.&lt;/p&gt;

&lt;p&gt;&lt;em&gt;Linux Journal&lt;/em&gt;'s coverage has ranged just as widely, but we've also kept faith with the serious developers who made Linux a success in the first place and are still our core readership.&lt;/p&gt;

&lt;p&gt;Our monthly issues are big. Where in print we were limited to less than 100 pages per issue, now we tend to run around 160–170 pages. Each issue also features a Deep Dive section, devoted to one topic in-depth, which is like a new ebook within each issue. And although we used to charge for our extensive archive (again, going back to 1994), we now provide access to all paying subscribers. The same subscribers also will get a free topical ebook with each renewal. (Currently it's &lt;em&gt;SysAdmin 101&lt;/em&gt; by our own Kyle Rankin, but we change it regularly.)&lt;/p&gt;

&lt;p&gt;After we were acquired (by the parent company of &lt;a href="https://www.privateinternetaccess.com"&gt;Private Internet Access&lt;/a&gt;) early this year, we completely overhauled the &lt;a href="https://www.linuxjournal.com"&gt;LinuxJournal.com&lt;/a&gt; website, taking it from Drupal 6 to 8 and redesigning it to maximize simplicity and responsiveness. Our constant aim with the website is to make all our editorial matter (which we won't demean by calling it mere "content") easy and enjoyable to read on all devices. This may be one reason our subscriber base has grown nearly 20% so far this year.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/join-linux-journal-crusade" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 04 Sep 2018 07:52:00 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1340110 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Now What?</title>
  <link>https://www.linuxjournal.com/content/now-what</link>
  <description>  &lt;div data-history-node-id="1339562" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/2000px-Question_Mark.svg_.png" width="800" height="600" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;em&gt;Linux Journal&lt;/em&gt; was a print magazine for 17+ years, then a digital one for
the next 7+. What shall we be now? That's the Big Question, and there are
many answers, some of which are already settled.
&lt;/p&gt;

&lt;p&gt;
For example, we need to remain digital, true to Linux and maintain a journal (or
journals) of some kind (or kinds), but those are among the few givens. We can be
many other things too. Here's a sampling of suggestions vetted by the few of us
internally and the many of us externally (for example, on IRC and in the comments
under Carlie's &lt;a href="http://www.linuxjournal.com/content/happy-new-year-linux-journal-alive"&gt;Happy
New Year post&lt;/a&gt;):
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;
Video
&lt;/li&gt;

&lt;li&gt;
Blogs
&lt;/li&gt;

&lt;li&gt;
Podcasts
&lt;/li&gt;

&lt;li&gt;
Reviews
&lt;/li&gt;

&lt;li&gt;
Tips
&lt;/li&gt;

&lt;li&gt;
Email newsletter(s)
&lt;/li&gt;
&lt;li&gt;
Techmeme-like news aggregator(s) or river(s)
&lt;/li&gt;

&lt;li&gt;
Events (of our own, plus participating in others)
&lt;/li&gt;

&lt;li&gt;
Doing more (or less, or other, than what we're already doing) in "social" media
&lt;/li&gt;

&lt;li&gt;
Stuff for wizards, full of code
&lt;/li&gt;

&lt;li&gt;
Stuff for muggles, full of stuff other than code
&lt;/li&gt;

&lt;li&gt;
Expanding coverage (in various media) of topics overlapping with Linux (such as
privacy/anti-surveillance, personal agency, tech policy, cryptocurrencies and
distributed ledgers, mobile, languages, business/models, gaming, protocols,
standards, system administration, IT, IoT, AI, ML, VR and other two-, three- and
four-letter acronyms)
&lt;/li&gt;

&lt;li&gt;
Becoming more international
&lt;/li&gt;

&lt;li&gt;
Becoming multi-lingual (also -cultural in several senses of that word and others
like it)
&lt;/li&gt;

&lt;li&gt;
Returning to print again (with the magazine, books or whatever, at intervals tbd)
&lt;/li&gt;

&lt;li&gt;
All the above
&lt;/li&gt;

&lt;li&gt;
Some of the above
&lt;/li&gt;

&lt;li&gt;
None of the above
&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;
That list probably ought to be a matrix, perhaps in four dimensions (meaning in no
particular order, top to bottom, right to left, front to back, forward through
time).
&lt;/p&gt;

&lt;p&gt;
We want and need thoughts and ideas of all kinds, especially from subscribers who
have kept us alive long enough to be revived. (Already we've been called "zombie
overlords" in comments. Good humor is a good sign, and so is the airing of old and good
complaints.)
&lt;/p&gt;

&lt;p&gt;
In the coming days, Carlie, myself and others on our still-small staff will be
raising questions here and also answering comments below (and also in our IRC channel
at &lt;a href="http://freenode.net"&gt;freenode&lt;/a&gt;, now a sister of ours).
&lt;/p&gt;

&lt;p&gt;
So please weigh in. We'll be taking copious notes and responding as best we can.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/now-what" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 03 Jan 2018 19:11:16 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1339562 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
