<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/applications">
  <channel>
    <title>applications</title>
    <link>https://www.linuxjournal.com/tag/applications</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Dia - The Diagram Creation Tool</title>
  <link>https://www.linuxjournal.com/content/dia-diagram-creation-tool-0</link>
  <description>  &lt;div data-history-node-id="1031186" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/dia097_network_200.jpg" width="200" height="200" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://live.gnome.org/Dia"&gt;Dia&lt;/a&gt; is an application designed for quick creation of structured diagrams such as simple, line-based illustrations, flowcharts, UML charts and network diagrams. Being a vector based tool, there is some overlap with other applications such as Inkscape, but Dia's focus is on diagrams that are more functional than aesthetic.&lt;/p&gt;
&lt;p&gt;The hub of Dia is its floating toolbox. Personally, I prefer to work fullscreen with this type of task, so I set the toolbox to always float on top of other windows in the preferences. The actual diagrams are edited within one or more document windows. The document is theoretically infinite in size, and you create more page space by scrolling off the side of the page.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/dia098_tea_640.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;The toolbox features a set of tools that will be familiar to anyone who has used a vector drawing package before, such as lines and boxes, alongside selection and text tools. These facilities seem sparse until you reach the meat of the program, the shapes palette. Initially, this defaults to about 25 flowchart objects that can be placed onto the diagram. However, these objects are more than just simple icons. For example, if you select the first shape, a rectangular box that represents “operation” in a standard flow chart, you can then place it within the diagram. The operation box has a flashing cursor in the middle of it for text input, and it automatically resizes itself to contain text, even when you add multiple lines.&lt;/p&gt;
&lt;p&gt;Objects typically feature connection points around the perimeter, which makes it very easy to connect shapes using lines. You can connect a line to a specific connection point or allow the program to automatically select the best connection point, which updates as you move the object around.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/dia097_network_640.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;You can choose other sets of shapes by using a drop-down menu. For example, there is a set for creating diagrams of electrical circuits, and another for chemical engineering diagrams. In total there are about 20 sets, most of which are applicable to scientific, computing and business fields. Dia is particularly good for creating computer network diagrams, and has a set of objects for just this purpose. &lt;/p&gt;
&lt;p&gt;Dia can export to the PNG bitmap format in addition to a wide range of common vector formats. The program itself has some support in third party applications such as LaTeX publishing system LyX. It's also available as a Windows application.&lt;br /&gt;&lt;strong&gt;&lt;br /&gt;Verdict&lt;/strong&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/dia-diagram-creation-tool-0" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 13 Feb 2012 15:54:11 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1031186 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Short Notices: News In Linux Audio</title>
  <link>https://www.linuxjournal.com/content/short-notices-news-linux-audio</link>
  <description>  &lt;div data-history-node-id="1028518" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/spec3d-icon.png" width="640" height="243" alt="A Spectrum3D screenshot." title="A Spectrum3D screenshot." typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/dave-phillips" lang="" about="https://www.linuxjournal.com/users/dave-phillips" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Dave Phillips&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;I hope all my readers enjoyed the best of the holiday season. I've been busy with the predictable confusions and minor crises that attend this time of year, but I managed to find time to jot down some recommendations for my readers. Go on, you've been good, give yourself a few extra belated gifts and don't worry if your budget's busted - it's all free software, you can't beat these deals.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://harmonyseq.wordpress.com/"&gt;&lt;strong&gt;harmonySEQ&lt;/strong&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;"&lt;em&gt;live loop-based MIDI software sequencer intended to aid music composers and performers&lt;/em&gt;"&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u800764/1-harmonyseq-small.jpg" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;Figure 1. harmonySEQ (&lt;a href="http://linux-sound.org/images/1-harmonyseq.png"&gt;Full size&lt;/a&gt;)&lt;/p&gt;
&lt;p&gt;I built and installed the latest development version 0.16 of &lt;a href="http://harmonyseq.wordpress.com/"&gt;harmonySEQ&lt;/a&gt;  (Figure 1) on a 64-bit &lt;a href="http://www.archlinux.org/"&gt;Arch&lt;/a&gt; system. This "little" sequencer is a  straightforward MIDI loop composition environment with some unique  features, e.g. a Control Sequence editor for writing MIDI Controller  curves and OSC support for remote transport control. HarmonySEQ has also  borrowed a page from Dr. Emile Tobenfeld's great &lt;a href="http://tamw.atari-users.net/omega.htm"&gt;Keyboard Controlled Sequencer&lt;/a&gt;,  i.e. the user can assign sequences to keypresses, a very flexible  method for patching together fixed-form compositions or for formal  improvisation in realtime performance. However you choose to use it,  harmonySEQ is likely to find a permanent place in your Linux MIDI  system. For more information see the harmonySEQ home and &lt;a href="http://www.louigiverona.ru/?page=projects&amp;s=writings&amp;t=linux&amp;a=linux_harmonyseq"&gt;Louigi Verona's article on his use of harmonySEQ&lt;/a&gt;. While you're out there, you should also check out his &lt;a href="http://harmonyseq.wordpress.com/showcase/"&gt;music improvised with harmonySEQ&lt;/a&gt;, it's an impressive display of the program's potential (and the composer's talent).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.warmplace.ru/soft/sunvox/"&gt;&lt;strong&gt;SunVox&lt;/strong&gt;&lt;/a&gt;&lt;br /&gt;  &lt;em&gt;"modular synthesizer with pattern based sequencer (tracker)"&lt;br /&gt;&lt;/em&gt;&lt;br /&gt;Developer Alex Zolotov calls his &lt;a href="http://www.warmplace.ru/soft/sunvox/"&gt;SunVox&lt;/a&gt; (Figure 2) a "modular music creation studio". It's a light-weight but complete music composition environment that includes a handy set of built-in synthesizers and effects processors. The "modular" in Alex's description refers to the program's module tracker, a music sequencing interface designed originally for music composition programs in the late 1980s. The tracker UI is easy to use and lends itself to complex pattern-based composition, but I should emphasize that any kind of music can be composed with a tracker.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/short-notices-news-linux-audio" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 11 Jan 2012 16:24:05 +0000</pubDate>
    <dc:creator>Dave Phillips</dc:creator>
    <guid isPermaLink="false">1028518 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
