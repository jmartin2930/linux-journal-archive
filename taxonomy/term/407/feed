<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/apt">
  <channel>
    <title>APT</title>
    <link>https://www.linuxjournal.com/tag/apt</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>APTonCD</title>
  <link>https://www.linuxjournal.com/content/aptoncd</link>
  <description>  &lt;div data-history-node-id="1015649" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/aptoncd1_scaled.png" width="200" height="180" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://aptoncd.sourceforge.net/"&gt;APTonCD&lt;/a&gt; is a GUI tool that can extract the .deb packages that you have manually installed on your machine and selectively copy them to an ISO image that can be burned to a CD or DVD. The tool can then be used, on another machine, to restore those packages. The main use is to create disc-based package collections that can be used to add packages to systems that don’t have an Internet connection, perhaps as part of a mass installation strategy.&lt;/p&gt;
&lt;p&gt;Clicking on the create button causes the program to start collating a list of packages that you have added to your system. When the process is complete, it displays a list of packages that can be included or deselected. Optionally, all dependencies for the selected packages are included. You can also add extra packages at this point by manually directing the program to actual .deb files. Once this is complete, click on the burn icon and the program will very quickly create an ISO containing the .deb files. The program can even span a collection files over more than one disc.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" height="480" width="480" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/u1013687/aptoncd2.png" /&gt;&lt;/p&gt;
&lt;p&gt;Restoring is just as simple. However, it’s important to note that APT on CD doesn’t actually install any software to your system. Instead, it copies the .deb files to the APT cache of your target machine. This way, when you install the packages using your chosen APT tool such as the Synaptic package manager, the system wont attempt to retrieve the packages from the Internet. APTtoCD can load the .deb files from an actual CD that you have burned or from an ISO file located on another medium.&lt;/p&gt;
&lt;p&gt;It’s worth noting that actually installing APTtoCD to a target machine is fairly simple as it is included in the collection of .deb files on the CD that you burn. Simply install it as you would any other .deb file.&lt;/p&gt;
&lt;p&gt;The &lt;a href="http://aptoncd.sourceforge.net/"&gt;APTonCD&lt;/a&gt; website&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/aptoncd" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 07 Dec 2010 16:14:25 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1015649 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>apt-file: Locate Missing Package Files</title>
  <link>https://www.linuxjournal.com/content/apt-file-locate-missing-package-files</link>
  <description>  &lt;div data-history-node-id="1013331" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/apt-get_icon.png" width="507" height="383" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Ever had a source package fail to build due to a missing file? If so, &lt;a href="http://en.wikipedia.org/wiki/Apt-file" rel="nofollow"&gt;apt-file&lt;/a&gt;, a tool that searches online repositories for a specific file, may be the answer.&lt;/p&gt;
&lt;p&gt;Occasionally, when building a package from source, disaster strikes and the whole process grinds to a halt due to a missing file. Fortunately, this is increasingly uncommon due to the maturity of Linux package management and the ubiquity of Autoconf configuration scripts. However, some software projects don't use Autoconf, either because the maintainers prefer another solution such as CMake or because the program is too small to make setting up a configure script worthwhile.&lt;/p&gt;
&lt;p&gt;Fortunately, on the rare occasions when this problem does crop up, there may be a solution in the form of apt-file: a member of the APT family of package management tools. This utility assembles an index of all of the files stored in the online repositories that your distribution is configured to use and allows you to search for individual files.&lt;/p&gt;
&lt;p&gt;Case example: recently, I decided to check out KBackup, a KDE backup management utility that isn't currently in the Ubuntu repository. Following the instructions, I ran the CMake build tool, only to be disappointed when the build process stopped with a notice that it couldn't find a file called “FindKDE4Internal.cmake”.&lt;/p&gt;
&lt;p&gt;To solve the problem, I installed apt-file via the command line:&lt;/p&gt;
&lt;pre&gt;
sudo apt-get install apt-file
&lt;/pre&gt;&lt;p&gt;
After installation, apt-file's internal index needs to be populated with the following command:&lt;/p&gt;
&lt;pre&gt;
apt-file update
&lt;/pre&gt;&lt;p&gt;
This process takes a minute or so to complete, depending on the speed of your Internet connection. Once the index was up to date, I searched for the missing file:&lt;/p&gt;
&lt;pre&gt;
apt-file search FindKDE4Internal.cmake
&lt;/pre&gt;&lt;p&gt;
and apt-file gave me a single line of output that told me everything I needed to know:&lt;/p&gt;
&lt;pre&gt;
kdelibs5-dev: /usr/share/kde4/apps/cmake/modules/FindKDE4Internal.cmake
&lt;/pre&gt;&lt;p&gt;
In other words, the file I needed was in the package “kdelibs5-dev”, which I installed from the command line:&lt;/p&gt;
&lt;pre&gt;
sudo apt-get install kdelibs5-dev
&lt;/pre&gt;&lt;p&gt;
After this, I was able to continue and complete the normal install procedure.&lt;/p&gt;
&lt;p&gt;&lt;em&gt;&lt;br /&gt;
Note: if you do find an oversight in a source package that necessitates the use of apt-file in this way, even after a successful run of an Autoconf configure script, please consider telling the developers about it.&lt;br /&gt;&lt;/em&gt;&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/apt-file-locate-missing-package-files" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 19 Aug 2010 12:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1013331 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
