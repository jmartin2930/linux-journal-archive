<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/office-suite">
  <channel>
    <title>office suite</title>
    <link>https://www.linuxjournal.com/tag/office-suite</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>FOSS Project Spotlight: ONLYOFFICE, an Online Office Suite</title>
  <link>https://www.linuxjournal.com/content/foss-project-spotlight-onlyoffice-online-office-suite</link>
  <description>  &lt;div data-history-node-id="1339962" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/onlyoffice.jpg" width="800" height="482" alt="OnlyOffice" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/tatiana-kochedykova" lang="" about="https://www.linuxjournal.com/users/tatiana-kochedykova" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Tatiana Kochedykova&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;a href="https://www.onlyoffice.com"&gt;ONLYOFFICE&lt;/a&gt; is a free and open-source office suite that provides an
alternative for three major MS Office apps—Word, Excel and
PowerPoint—working online.
&lt;/p&gt;

&lt;p&gt;
ONLYOFFICE's main features include:
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;
Text, spreadsheet and presentation online viewers and editors.
&lt;/li&gt;

&lt;li&gt;
Support for all popular file formats: DOC, DOCX, ODT, RTF, TXT, PDF, HTML,
EPUB, XPS, DjVu, XLS, XLSX, ODS, CSV, PPT, PPTX and ODP.
&lt;/li&gt;

&lt;li&gt;
A set of formatting and styling tools common for desktop office apps.
&lt;/li&gt;

&lt;li&gt;
A set of collaboration tools: two co-editing modes (fast and strict),
commenting and built-in chat, tracking changes and version history.
&lt;/li&gt;

&lt;li&gt;
Ready-to-use plugins: Translator, YouTube, OCR, Photo Editor and more.
&lt;/li&gt;

&lt;li&gt;
Macros to standardize work with documents.
&lt;/li&gt;

&lt;li&gt;
Support for hieroglyphs.
&lt;/li&gt;&lt;/ul&gt;&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/12436f1.png" width="1240" height="708" alt="""" class="image-max_1300x1300" /&gt;&lt;p&gt;&lt;em&gt;
Figure 1. ONLYOFFICE Formatting and Styling Tools&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Ways to use ONLYOFFICE:
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;
Integrated with a collaboration platform: for teams, ONLYOFFICE can be installed together with a set of
productivity tools designed by ONLYOFFICE that includes CRM, projects,
document management system, mail, calendar, blogs, forums and chat.
&lt;/li&gt;

&lt;li&gt;
Integrated with popular web services:
for users of popular services like Nextcloud, ownCloud, Alfresco,
Confluence or SharePoint, ONLYOFFICE offers official connectors to integrate
online editors and edit documents within them. Some web services, like the eXo
Platform, provide users with their own connectors or offer instructions like
Seafile for integration with ONLYOFFICE.
&lt;/li&gt;

&lt;li&gt;
Integrated with your own web apps:
for developers who are building their own productivity app, no matter what
kind of application they provide to users or which language they use to
write it, ONLYOFFICE offers an API to help them integrate online editors with
their apps.
&lt;/li&gt;
&lt;/ul&gt;&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/12436f2.png" width="1240" height="708" alt="""" class="image-max_1300x1300" /&gt;&lt;p&gt;&lt;em&gt;
Figure 2. Co-Editing with ONLYOFFICE&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
To use this online office suite, you need to have the ONLYOFFICE Document Server
installed, and you can choose from multiple &lt;a href="https://www.onlyoffice.com/download.aspx"&gt;installation options&lt;/a&gt;: compile the
source code available on &lt;a href="https://github.com/ONLYOFFICE/DocumentServer"&gt;GitHub&lt;/a&gt;, use .deb or
.rpm packages or the Docker
image.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/foss-project-spotlight-onlyoffice-online-office-suite" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 06 Jul 2018 13:30:00 +0000</pubDate>
    <dc:creator>Tatiana Kochedykova</dc:creator>
    <guid isPermaLink="false">1339962 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>LibreOffice 5.0 Looking Good and Shipping This Week</title>
  <link>https://www.linuxjournal.com/content/libreoffice-50-looking-good-and-shipping-week</link>
  <description>  &lt;div data-history-node-id="1338792" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/lo.png" width="300" height="169" alt="LibreOffice 5.0 Looking Good and Shipping This Week" title="LibreOffice 5.0" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/james-darvell" lang="" about="https://www.linuxjournal.com/users/james-darvell" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;James Darvell&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u800391/lo.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;LibreOffice 5.0 ships this week, and it brings a host of improvements that users will be excited about. There are changes in all of the applications which make up the suite, and there are significant changes to the shared code, too. Writer and Calc are the apps that I use most often, so I'm really glad to see the number of useful improvements in these areas.
&lt;/p&gt;
&lt;p&gt;&lt;cite&gt;&lt;strong&gt;UPDATE 12:16pm GMT August 5, 2015:&lt;/strong&gt; LibreOffice 5.0 has been released and is immediately available from the following link: &lt;a href="http://www.libreoffice.org/download/"&gt;http://www.libreoffice.org/download/&lt;/a&gt;.&lt;/cite&gt;
&lt;/p&gt;&lt;p&gt;
Using a Linux-based operating system is all about making a choice in favor of free, standards-based software. But it's still important to be able to exchange files with users of proprietary software. For most users, this really becomes an issue where office software is concerned. Although LibreOffice (and OpenOffice.org before it) has always been compatible with Microsoft's Office suite, it's never been 100% complete. Small incompatibilities have often led to glitches that required workarounds. 
&lt;/p&gt;&lt;p&gt;
This has especially been true where files contained embedded objects such as images or audio files. Text highlighting was another feature which was sometimes messed up when importing Microsoft files. LibreOffice 5.0 has addressed a number of these issues. This will lead to a more consistent presentation of documents across platforms (i.e. complex documents and presentations won't look broken).
&lt;/p&gt;&lt;p&gt;
If you love using emojis, then you'll be glad to know that Writer has added shortcode support. It translates easy-to-remember shortcodes such as :family: and :flags: into their Unicode 6 counterparts.
&lt;/p&gt;&lt;p&gt;
It's possible to crop images in Writer using a simple interface. Although Writer is never going to replace Gimp as a full featured image editor, it helps to be able to make these small changes without opening another application.
&lt;/p&gt;&lt;p&gt;
Calc has improved support for pivot tables and allows item labels to be repeated multiple times now. Conditional formatting (which has been around for several releases now) has an improved UI. The conditional formatting is also preserved when exporting to the Microsoft XLSX format.
&lt;/p&gt;&lt;p&gt;
Spreadsheet functions have been upgraded, too. Some new functions have been added, for Excel compatibility. And other functions have optional parameters to make them fully compatible with OpenFormula.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/libreoffice-50-looking-good-and-shipping-week" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 05 Aug 2015 03:14:24 +0000</pubDate>
    <dc:creator>James Darvell</dc:creator>
    <guid isPermaLink="false">1338792 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
