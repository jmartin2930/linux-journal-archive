<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/subutai">
  <channel>
    <title>Subutai</title>
    <link>https://www.linuxjournal.com/tag/subutai</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>As Cloud Computing Providers Post Record Profits, One Company Wants to Make Them Obsolete</title>
  <link>https://www.linuxjournal.com/content/cloud-computing-providers-post-record-profits-one-company-wants-make-them-obsolete</link>
  <description>  &lt;div data-history-node-id="1339987" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/LinuxJournal-SubutaiBanner_2x.jpg" width="800" height="471" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/graham-templeton" lang="" about="https://www.linuxjournal.com/users/graham-templeton" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Graham Templeton&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Could the world's idle computers make the $200 billion cloud
computing industry obsolete?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In the final years of the Obama administration, it seemed that the world
was witnessing the emergence of an odd alliance between the largest
establishment tech companies and the traditionally anti-establishment
community of independent techies.  The highest profile case involved Apple
and other major corporations siding with civil rights organizations to
advocate against weakening device encryption on behalf of law enforcement,
and it it led to a hope that these companies were finally seeing the
profit potential in having secure, satisfied customers. That hope has
now evaporated in the face of continuous betrayals of user trust, and
rather than take it lying down, tech users are looking into alternative
solutions that put them in control.
&lt;/p&gt;

&lt;p&gt;
The recently passed federal &lt;em&gt;Clarifying Lawful Overseas Use of Data
Act&lt;/em&gt;,
or the CLOUD Act, makes it perfectly clear why this shift away from
self-interested stakeholders is so necessary. The new law &lt;a href="https://blogs.microsoft.com/on-the-issues/2018/04/03/the-cloud-act-is-an-important-step-forward-but-now-more-steps-need-to-follow"&gt;was
motivated by&lt;/a&gt;
a legal demand for Microsoft to hand user data to US law enforcement,
even though that data was stored outside US borders and, thus, in a
different legal jurisdiction. At first, it seemed that Microsoft was
planning to stand up for privacy and national sovereignty by opposing
the demand in court—but, predictably, the corporation rolled over
just as soon as it deemed that its own interests were protected.
&lt;/p&gt;

&lt;p&gt;
The goal of the CLOUD Act is for the US to be able to compel any company
that does business in the country to provide information to US law
enforcement, even if that information is not actually stored within the
US. If it's successful, a company's willingness to fight will become
essentially irrelevant. It makes all cloud computing and cloud storage
companies suspect, simply by virtue of being cloud computing companies.
&lt;/p&gt;

&lt;p&gt;
Groups like the &lt;a href="https://www.eff.org/deeplinks/2018/02/cloud-act-dangerous-expansion-police-snooping-cross-border-data"&gt;Electronic
Frontier Foundation&lt;/a&gt; and the &lt;a href="https://www.aclu.org/blog/privacy-technology/internet-privacy/cloud-act-dangerous-piece-legislation"&gt;American Civil
Liberties Union&lt;/a&gt; believe it also could allow US law enforcement to
seize information in foreign jurisdictions because those regions have
more lenient privacy laws than the US itself.  The reverse, in which
foreign law enforcement agencies operating under &lt;a href="https://global.handelsblatt.com/politics/with-new-us-law-how-safe-is-online-data-in-europe-914956"&gt;more
stringent rules
than the US&lt;/a&gt;, could gain access to protected information stored in the
country, is also a major concern.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/cloud-computing-providers-post-record-profits-one-company-wants-make-them-obsolete" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 28 Jun 2018 12:00:00 +0000</pubDate>
    <dc:creator>Graham Templeton</dc:creator>
    <guid isPermaLink="false">1339987 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
