<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/lab">
  <channel>
    <title>lab</title>
    <link>https://www.linuxjournal.com/tag/lab</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Creating Software-backed iSCSI Targets in Red Hat Enterprise Linux 6</title>
  <link>https://www.linuxjournal.com/content/creating-software-backed-iscsi-targets-red-hat-enterprise-linux-6</link>
  <description>  &lt;div data-history-node-id="1022412" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/200px-Question_book-2.svg_.png" width="200" height="158" alt="iSCSI Lab" title="iSCSI Lab" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/pete-vargas-mas" lang="" about="https://www.linuxjournal.com/users/pete-vargas-mas" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Pete Vargas Mas&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Studying for certification exams can be an adventure. Even more so when the certification exam is a hands-on, performance-based exam. The quandry most people I know fall into, is that to effectively study for such an exam, you need access to a lab environment with elements that may be beyond the scope of the average Linux enthusiast. One such element is iSCSI. &lt;/p&gt;
&lt;p&gt;A good example of this comes from the Red Hat website, which mentions on their Red Hat Certified Engineer (RHCE) exam objectives page:&lt;/p&gt;
&lt;blockquote&gt;&lt;p&gt;&lt;a href="http://www.redhat.com/certification/rhce/objectives/"&gt;Configure a system as an iSCSI initiator that persistently mounts an iSCSI target&lt;/a&gt;&lt;/p&gt;
&lt;/blockquote&gt;
&lt;p&gt;Unless I'm reading too much into this statement (and I don't think I am...), I interpret this objective as stating that I need to know how to configure the iSCSI &lt;em&gt;Initiator&lt;/em&gt; side and not the iSCSI &lt;em&gt;Target&lt;/em&gt; side. Keep in mind that the Initiator is the host that is accessing the storage device (like a client), as opposed to the Target, which is the host that is sharing out a iSCSI device. The Target could be a complicated network-attached storage device, or something as simple as a Linux host.&lt;/p&gt;
&lt;p&gt;Therefore, for our lab setup to be useful, we will need access to a host with a configured iSCSI Target. Let's take a look at one way we can make this happen.&lt;/p&gt;
&lt;h3&gt;The Environment&lt;/h3&gt;
&lt;p&gt;In my lab setup, I have a host named server1.example.com, with an IP address of 192.168.1.10, which runs Red Hat Enterprise Linux Server v6.0 (x86_64). This will be my iSCSI Target. A second host, named client1.example.com (192.168.1.11), which is also running RHEL 6, will be the iSCSI initator.  &lt;/p&gt;
&lt;p&gt;I will use the Open-iSCSI project software (a high-performance, transport independent,&lt;br /&gt;multi-platform implementation of RFC3720 iSCSI) included with RHEL 6.&lt;/p&gt;
&lt;h3&gt;Creating the iSCSI Target&lt;/h3&gt;
&lt;p&gt;First, you will need to create a software-backed iSCSI target. Install the scsi-target-utils package and its dependencies:&lt;/p&gt;
&lt;p&gt;From the RHEL 6 DVD, I installed the following packages on server1 using the &lt;em&gt;yum localinstall&lt;/em&gt; command:&lt;/p&gt;
&lt;p&gt;   Packages/scsi-target-utils-1.0.4-3.el6.x86_64.rpm&lt;br /&gt;   Packages/perl-Config-General-2.44-1.el6.noarch.rpm&lt;/p&gt;
&lt;p&gt;Also from the RHEL 6 DVD, I installed the following packages on client1using the &lt;em&gt;yum localinstall&lt;/em&gt; command:&lt;/p&gt;
&lt;p&gt;   Packages/iscsi-initiator-utils-6.2.0.872-10.el6.x86_64.rpm&lt;/p&gt;
&lt;h3&gt;Start the tgtd service&lt;/h3&gt;
&lt;p&gt;The tgtd service hosts SCSI targets and uses the iSCSI protocol to enable communications between targets and initiators. Start the tgtd service and make the service persistant after reboots with the chkconfig command:&lt;/p&gt;
&lt;p&gt;# service tgtd start&lt;br /&gt;# chkconfig tgtd on&lt;/p&gt;
&lt;p&gt;Now we will need some type of storage device to use as a target.  Any accessible storage device will do, but to keep things simple, here are two options we can use:&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/creating-software-backed-iscsi-targets-red-hat-enterprise-linux-6" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 20 Jul 2011 13:00:00 +0000</pubDate>
    <dc:creator>Pete Vargas Mas</dc:creator>
    <guid isPermaLink="false">1022412 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
