<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/robotics">
  <channel>
    <title>robotics</title>
    <link>https://www.linuxjournal.com/tag/robotics</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>peekabot—3-D Robotic Visualization</title>
  <link>https://www.linuxjournal.com/content/peekabot-3-d-robotic-visualization</link>
  <description>  &lt;div data-history-node-id="1010964" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/10623f6.inline.jpg" width="480" height="243" alt="" title="A number of Blender-created models allow for some snazzy active objects, available freely on the Web. " typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/john-knight" lang="" about="https://www.linuxjournal.com/users/john-knight" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;John Knight&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;According to the peekabot project's Web site:&lt;/p&gt;
&lt;p&gt;    "peekabot is a distributed real-time 3-D visualization tool for robotics researchers and developers, written in C++. Its purpose is to simplify the visualization needs faced by a roboticist daily—using visualization as a debugging aid or making fancy slides for a presentation, for example.&lt;/p&gt;
&lt;p&gt;    Our goal is to provide a flexible tool that will cater to the vast majority of a roboticist's visualization needs, yet is easy to use. Typical scenarios include visualization of simulations, data display from real robots and monitoring of remotely deployed robots.&lt;/p&gt;
&lt;p&gt;    ...to enable remote data visualization, peekabot uses a distributed client-server architecture. All the gory details of networking is handled by the client library, used by your programs."&lt;/p&gt;
&lt;p&gt;&lt;img src="https://www.linuxjournal.com/ufiles/10623f5.inline.jpg" /&gt;&lt;br /&gt;
peekabot's low-level control of actions allows for some very advanced scripting, such as the object pathing shown here. &lt;/p&gt;
&lt;p&gt;&lt;img src="https://www.linuxjournal.com/ufiles/10623f6.inline.jpg" /&gt;&lt;br /&gt;
A number of Blender-created models allow for some snazzy active objects, available freely on the Web. &lt;/p&gt;
&lt;p&gt;&lt;/p&gt;&lt;h2&gt;Installation&lt;/h2&gt;
&lt;p&gt;Head to the &lt;a href="http://www.peekabot.org"&gt;Web site&lt;/a&gt;, and grab the latest tarball. In terms of library requirements, the documentation helpfully states the following (note, in the list below: *not required when building only the client API; **needed only if building the unit tests, which are disabled by default):&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;
A decently recent version of GCC.
&lt;/li&gt;
&lt;li&gt;Boost 1.34.0+ (Boost.Thread, Boost.DateTime, Boost.Filesystem*, Boost.ProgramOptions* and Boost.Test**).
&lt;/li&gt;
&lt;li&gt;
Xerces-C++ 2.7.0+*.
&lt;/li&gt;
&lt;li&gt;
FLTK 1.1.6+*.
&lt;/li&gt;
&lt;li&gt;
OpenGL*.
&lt;/li&gt;
&lt;li&gt;
GLU*.
&lt;/li&gt;
&lt;li&gt;
libpng*.
&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;I also had to grab these development files: libxerces-c2-dev and libfltk1.1-dev. Once you download the tarball, open a terminal wherever it's saved and enter the following: &lt;/p&gt;
&lt;pre&gt;
$ tar xvzpf peekabot-x.y.z.tar.gz 
$ cd peekabot-x.y.z
$ ./configure --prefix=/usr
$ make
$ sudo make install
&lt;/pre&gt;&lt;p&gt;
Assuming all went well, when the compilation has finished, you can run the program with the command: &lt;/p&gt;
&lt;pre&gt;
$ peekabot
&lt;/pre&gt;&lt;p&gt;
Before we jump in, I have to warn you that we've covered only half the equation. peekabot is made of two key parts: the server and its clients. After the initial building process, you will have the server by itself. The server is the main GUI screen where you'll be testing and interacting with your client programs. The clients generally will be standalone programs that communicate with the peekabot server while following their own coding structure. Although this may be daunting for new users (me included), it does make the system very open, powerful and flexible.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/peekabot-3-d-robotic-visualization" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 30 Mar 2010 15:08:48 +0000</pubDate>
    <dc:creator>John Knight</dc:creator>
    <guid isPermaLink="false">1010964 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
