<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/wi-fi">
  <channel>
    <title>Wi-Fi</title>
    <link>https://www.linuxjournal.com/tag/wi-fi</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>#geeklife: weBoost 4G-X OTR Review</title>
  <link>https://www.linuxjournal.com/content/geeklife-weboost-4g-x-otr-review</link>
  <description>  &lt;div data-history-node-id="1340026" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12503f2.jpg" width="800" height="600" alt="Kyle's awesome van" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Will a cellular booster help me stay connected on my epic working
road trip?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
I'm a Linux geek, and I think I safely can assume everyone reading an article
in &lt;em&gt;Linux Journal&lt;/em&gt; identifies themselves as Linux geeks as well.
Through
the years I've written about many of my geeky projects here in &lt;em&gt;Linux
Journal&lt;/em&gt;, such as my Linux-powered beer fermentation fridge or my 3D
printer that's remotely controlled using a Raspberry Pi and Octoprint
software. The thing is, my interests don't stop strictly at Linux,
and I doubt yours do either. While my homebrewing, 3D printing and
(more recently) RV interests sometimes involve Linux, often they don't,
yet my background means I've taken a geek's perspective and approach
to all of those interests. I imagine you take a similar approach to
your hobbies and side projects, and readers would find some of those
stories interesting, useful and inspirational.
&lt;/p&gt;

&lt;p&gt;
We discussed this at &lt;em&gt;Linux Journal&lt;/em&gt; and realized there should be a
space for Linux geeks to tell their geeky stories even if they don't
directly involve Linux. This new series, #geeklife, aims to provide a
place where Linux geeks can talk about interests and projects even
if they might not be strictly Linux-related. We invite you to send proposals
for #geeklife articles to ljeditor@linuxjournal.com.
&lt;/p&gt;

&lt;p&gt;
For this first #geeklife article, I'm telling the story of a geeky,
connected working road trip I just took in my RV, and within that context,
I also review a particular piece of hardware I hoped would make the trip possible,
the &lt;a href="https://www.weboost.com/products/drive4g-x-otr"&gt;weBoost Drive
4G-X OTR&lt;/a&gt;. In the interest of full disclosure, Wilson
Electronics provided me with this review unit, and I did not purchase
it independently.
&lt;/p&gt;

&lt;h3&gt;
Working Remotely&lt;/h3&gt;

&lt;p&gt;
My job is 100% remote. It took me many years of braving multi-hour
California Bay Area commutes and turning down opportunities to
find a job where I finally could work completely from home. Smart
organizations are finally beginning to realize the many
advantages to having a remote workforce,
but I've found it works best if you have the right team,
the right tools and the bulk of the workforce is remote. When everyone is
distributed, everyone relies on the incredible modern collaboration tools
currently available, and you have focus and incredible productivity
when you need it while still being able to communicate with your peers.
&lt;/p&gt;

&lt;p&gt;
My wife is a freelance writer and has worked from her home office long
before I also worked from home. Once I also landed a job where I was
completely remote, we posed the following question to ourselves: in
theory, we could work from anywhere with a decent internet connection,
but in practice, is that really something we could do? What would that
kind of working trip look like?
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/geeklife-weboost-4g-x-otr-review" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 07 Aug 2018 12:00:00 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1340026 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Roll Your Own Enterprise Wi-Fi</title>
  <link>https://www.linuxjournal.com/content/roll-your-own-enterprise-wi-fi</link>
  <description>  &lt;div data-history-node-id="1338858" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11917unifif1.jpg" width="550" height="260" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
As you can tell by my Wi-Fi focus in The Open-Source Classroom &lt;a href="http://lj.mybigcommerce.com/linux-journal-october-2015-258/"&gt;this month&lt;/a&gt;,
I really love wireless networking. I've implemented wireless solutions
for schools on a shoestring budget, and I've helped plan campus-wide
rollouts of redundantly controlled enterprise solutions. The one thing
I really like about the enterprise solutions is the single point of
management. The problem is those controllers and enterprise-capable
access points are so darn expensive! That's why I love Ubiquiti.
&lt;/p&gt;
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11917unifif1.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
The UniFi line of products from Ubiquiti is affordable and reliable, but
the really awesome feature is its (free!) Web-based controller app. The
only UniFi products I have are wireless access points, even though the
company also has
added switches, gateways and even VoIP products to the mix. Even with
my limited selection of products, however, the Web controller makes
designing and maintaining a wireless network not just easy, but fun!
&lt;/p&gt;

&lt;p&gt;
The Web controller runs happily on a little Atom Web server, and if it
happens to go off-line, the access points keep working independently. If
you'd like robust hardware with a powerful Web controller application,
but you're not ready to invest the thousands of dollars for a traditional
enterprise solution, check out the &lt;a href="https://www.ubnt.com"&gt;UniFi&lt;/a&gt; product line. If you're half
as nerdy as I am, you won't regret the decision!
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/roll-your-own-enterprise-wi-fi" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 15 Oct 2015 19:31:18 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1338858 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Wi-Fi Mini Honeypot</title>
  <link>https://www.linuxjournal.com/content/wi-fi-mini-honeypot</link>
  <description>  &lt;div data-history-node-id="1084427" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11175f1.png" width="461" height="480" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/marcin-teodorczyk" lang="" about="https://www.linuxjournal.com/users/marcin-teodorczyk" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Marcin Teodorczyk&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Do you have an old, unused wireless router collecting
dust? Have some fun and make a Wi-Fi honeypot with it!
&lt;/p&gt;
&lt;p&gt;
Recently, I've been playing with some new wireless gear. It's nothing special: 200mW
Atheros-based transceiver and 18dBi yagi antenna. I'm living in an
apartment in a city of about 640,000 people. I've pointed the antenna to
a window and passively received about 30 wireless ESSIDs, three of which
were unsecured (open) and six secured with WEP (easily crackable). I
haven't connected to any of them, of course, but that gave me some ideas.
&lt;/p&gt;

&lt;p&gt;
What if I deployed a wireless access point deliberately open? Some people
eventually will connect and try to use it for Internet access—some might be
malicious, and 
some might think that it's a hotspot. And, what if I deployed a similar
access point, but secured with easily crackable WEP this time? Well, in my
humble opinion, it's not possible to unconsciously crack WEP. If somebody
that I don't know connects to this AP, I've just been attacked. All I need
to do is to monitor.
&lt;/p&gt;

&lt;p&gt;
That's exactly a wireless honeypot: fake access point, deliberately
unsecured or poorly secured and monitored, so you can get as much
information about attackers as you want. Such honeypots are especially
useful in large networks as early threat indicators, but you
also can play with them on your home network, just for fun and research.
&lt;/p&gt;

&lt;p&gt;
You can build a wireless honeypot with old hardware, some spare time and,
of course, a Linux-based solution. &lt;a href="https://openwrt.org"&gt;OpenWrt&lt;/a&gt; and 
&lt;a href="http://www.dd-wrt.com/site/index"&gt;DD-WRT&lt;/a&gt;
are the two most popular Linux-based firmware projects for routers. I
use them and some old spare routers in this article to show you how to build three kinds of honeypots:
a very basic one that logs only information about packets sent by users into
its memory, a little more sophisticated one with USB storage that logs a
few more details about malicious clients to the storage, and finally,
a solution that redirects HTTP traffic through a proxy that not
only can log, but also interfere with communication.
&lt;/p&gt;

&lt;h3&gt;
Basic Honeypot with DD-WRT&lt;/h3&gt;

&lt;p&gt;
Building a very basic wireless honeypot shouldn't take you more than an
hour or two. Just grab your old router and pick up the firmware. Be sure
to look at supported routers for both DD-WRT and OpenWrt. In my case, it
came up that the router is supported only by DD-WRT, as it has 32MB of RAM
and 4MB of Flash memory. OpenWrt's hardware requirements are a little bigger.
&lt;/p&gt;

&lt;p&gt;
Next, flash your router (that's the risky part). Basically, you need to
download the firmware for your machine and upload it to the memory. On
some routers, it's as easy as clicking a button on the Web interface. On
others, you have to connect through a serial cable, for example. Remember,
this step can be dangerous. Make a backup first and be sure to 
read the instructions carefully on the DD-WRT/OpenWrt sites.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/wi-fi-mini-honeypot" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 29 Mar 2013 17:12:27 +0000</pubDate>
    <dc:creator>Marcin Teodorczyk</dc:creator>
    <guid isPermaLink="false">1084427 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Wi-Fi on the Command Line</title>
  <link>https://www.linuxjournal.com/content/wi-fi-command-line</link>
  <description>  &lt;div data-history-node-id="1016748" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/computer-grass_1.jpg" width="200" height="300" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/joey-bernard" lang="" about="https://www.linuxjournal.com/users/joey-bernard" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Joey Bernard&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
More people than ever are using wireless networks as their primary networking
medium. Great programs are available under X11 that give users a graphical
interface to their wireless cards. Both GNOME and KDE include network management
utilities, and a desktop-environment-agnostic utility called wicd 
also offers great functionality. But, what if you aren't running X11
and want to manage your wireless card? I don't cover how to 
install and activate your card here (for that, take a look at projects like madwifi
or ndiswrapper). I assume your card is installed and
configured properly, and that it is called wlan0. Most of the utilities mentioned
below need to talk directly to your wireless card (or at least the card driver),
so they need to be run with root privileges (just remember to use sudo).
&lt;/p&gt;

&lt;p&gt;
The first step is to see what wireless networks are available in your area.
A utility called iwlist provides all sorts of information about your wireless
environment.
To scan your environment for available networks, do the following:

&lt;/p&gt;&lt;pre class="programlisting"&gt;sudo iwlist wlan0 scan
&lt;/pre&gt;
&lt;p&gt;
You'll see output resembling:


&lt;/p&gt;&lt;pre class="programlisting"&gt;Cell 01 - Address: 00:11:22:33:44:55
          ESSID:"network-essid"
          Mode:Master
          Channel:11
          Frequency:2.462 GHz (Channel 11)
          Quality=100/100  Signal level:-47dBm  Noise level=-100dBm
          Encryption key:off
          .
          .
          .
&lt;/pre&gt;
&lt;p&gt;
The details (address and essid) have been changed to protect the guilty. Also,
the ... represents extra output that may or may not be available, depending on your
hardware. You will get a separate cell entry for each access point within your
wireless card's range. For each access point, you can find the hardware
address, the essid and the channel on which it's operating. Also, you can learn
in what mode the access point is operating (whether master or ad hoc). Usually,
you will be most interested in the essid and what encryption is being
used.
&lt;/p&gt;&lt;p&gt;

Once you know what's available in your immediate environment,
configure your wireless card to use one of these access points
using the iwconfig utility to set the parameters for your wireless card. First,
set the essid, which identifies the network
access point you want:

&lt;/p&gt;&lt;pre class="programlisting"&gt;sudo iwconfig wlan0 essid network-essid
&lt;/pre&gt;
&lt;p&gt;
Depending on your card and its driver, you may have the option to set the essid
to the special value “any”. In this case, your card will pick the first
available access point. This is called promiscuous mode.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/wi-fi-command-line" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 17 Dec 2010 15:53:50 +0000</pubDate>
    <dc:creator>Joey Bernard</dc:creator>
    <guid isPermaLink="false">1016748 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
