<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/version-control">
  <channel>
    <title>version control</title>
    <link>https://www.linuxjournal.com/tag/version-control</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>A Git Origin Story</title>
  <link>https://www.linuxjournal.com/content/git-origin-story</link>
  <description>  &lt;div data-history-node-id="1339955" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/git-logo_2.jpg" width="800" height="471" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/zack-brown" lang="" about="https://www.linuxjournal.com/users/zack-brown" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Zack Brown&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;A look at Linux kernel developers' various
revision control solutions through the years, Linus Torvalds' decision to use
BitKeeper and the controversy that followed, and how Git came to be created.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Originally, Linus Torvalds used no revision control at all. Kernel
contributors would post their patches to the Usenet group, and later to the
mailing list, and Linus would apply them to his own source tree. Eventually,
Linus would put out a new release of the whole tree, with no division between any of
the patches. The only way to examine the history of his process was as a
giant &lt;code&gt;diff&lt;/code&gt; between two full releases.
&lt;/p&gt;

&lt;p&gt;
This was not because there were no open-source revision control systems
available. CVS had been around since the 1980s, and it was still the most
popular system around. At its core, it would allow contributors to submit
patches to a central repository and examine the history of patches going
into that repository.
&lt;/p&gt;

&lt;p&gt;
There were many complaints about CVS though. One was that it tracked changes on a
per-file basis and didn't recognize a larger patch as a single revision,
which made it hard to interpret the past contributions of other developers.
There also were some hard-to-fix bugs, like race conditions when two
conflicting patches were submitted at the same time.
&lt;/p&gt;

&lt;p&gt;
Linus didn't like CVS, partly for the same reasons voiced by others and
partly for his own reasons that would become clear only later. He
also didn't like Subversion, an open-source project that emerged around the start of the
2000s and had the specific goal of addressing the bugs and misfeatures in
CVS.
&lt;/p&gt;

&lt;p&gt;
Many Linux kernel developers were unhappy with the lack of proper revision
control, so there always was a certain amount of community pressure for Linus
to choose something from one of the available options. Then, in 2002, he did.
To everyone's shock and dismay, Linus chose BitKeeper, a closed-source
commercial system developed by the BitMover company, run by Larry McVoy.
&lt;/p&gt;

&lt;p&gt;
The Linux kernel was the most important open-source project in history, and
Linus himself was the person who first discovered the techniques of open-source development that had eluded the GNU project, and that would be
imitated by open-source projects for decades to come, right up to the present
day. What was Linus thinking? How could he betray his community and the Open
Source world like this? Those were the thoughts of many when Linus first
started using BitKeeper for kernel development.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/git-origin-story" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 27 Jul 2018 11:45:00 +0000</pubDate>
    <dc:creator>Zack Brown</dc:creator>
    <guid isPermaLink="false">1339955 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Mercurial - Revision Control Approximated </title>
  <link>https://www.linuxjournal.com/content/mercurial-revision-control-approximated</link>
  <description>  &lt;div data-history-node-id="1037533" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11144f1.png" width="640" height="337" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/joey-bernard" lang="" about="https://www.linuxjournal.com/users/joey-bernard" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Joey Bernard&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
A short while ago, an article appeared in &lt;em&gt;Linux
Journal&lt;/em&gt; implying 
Git was the be-all and end-all of source code revision control systems
("&lt;a href="http://www.linuxjournal.com/content/git-revision-control-perfected"&gt;Git—Revision Control Perfected" by Henry Van Styn, August 2011&lt;/a&gt;). I would like to
challenge that assumption and declare to the world that the
&lt;em&gt;real&lt;/em&gt; perfect
version control system is here, and its name is Mercurial.
&lt;/p&gt;

&lt;p&gt;
In case you didn't notice it, my tongue was firmly in my cheek in that last
paragraph. I think version control systems are like editors. They are
all different and fit people and their work habits differently. There
is no one perfect system to rule them all. Git may be the perfect fit for
some people, and RCS may fit
someone else better. This article describes another option to add to the
mix. Mercurial provides some of the features of systems like Git, and some
of the features of systems like CVS or Subversion. Hopefully, after reading
this article,
you'll have enough information to make a rational choice as
to what is best for you. 
&lt;/p&gt;

&lt;p&gt;
The main Mercurial site contains lots of documentation for
end users and developers alike. Several tutorials are available,
and they even include a series of work flows that cover how end users
can use Mercurial for their development projects. Using those, you can
see how you could use Mercurial as a solo developer or as one of a group
of developers, or how to work with a central repository like CVS. These
work flows are great starting points for you to create your own.
&lt;/p&gt;

&lt;p&gt;
First, let's look at what makes up Mercurial. A Mercurial repository
consists of a working directory, which is paired with a store. The
store contains the history of the repository. Every working
directory is paired with its own copy of the store. This means that
Mercurial has a distributed system, much like Git. When you commit a
series of file changes, a single changeset is created, encapsulating
these changes. Each changeset gets a sequential number, called the
revision number. But, remember that each working directory gets its own
copy of the store, so these revision numbers may not actually match
up. For this reason, each revision also gets a 40-digit hexadecimal
globally unique ID. 
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11144f1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Figure 1. Here you see that Mercurial repositories are tagged for easy finding.
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11144f2.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Figure 2. Right-clicking a file and pulling up the properties
gives you lots of Mercurial information.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/mercurial-revision-control-approximated" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 24 Apr 2012 17:17:51 +0000</pubDate>
    <dc:creator>Joey Bernard</dc:creator>
    <guid isPermaLink="false">1037533 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Git - Revision Control Perfected</title>
  <link>https://www.linuxjournal.com/content/git-revision-control-perfected</link>
  <description>  &lt;div data-history-node-id="1023997" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/10971f1.png" width="640" height="413" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/henry-van-styn" lang="" about="https://www.linuxjournal.com/users/henry-van-styn" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Henry Van Styn&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
In 2005, after just two weeks, Linus Torvalds completed the first version
of Git, an open-source version control system. Unlike typical centralized
systems, Git is based on a distributed model. It is extremely flexible and
guarantees data integrity while being powerful, fast and efficient. With
widespread and growing rates of adoption, and the increasing popularity
of services like GitHub, many consider Git to be the best version control
tool ever created.
&lt;/p&gt;

&lt;p&gt;
Surprisingly, Linus had little interest in writing a version control tool
before this endeavor. He created Git out of necessity and frustration. The
Linux Kernel Project needed an open-source tool to manage
its massively distributed development effectively, and no existing tools were up to
the task.
&lt;/p&gt;

&lt;p&gt;
Many aspects of Git's design are radical departures from the approach
of tools like CVS and Subversion, and they even differ significantly from
more modern tools like Mercurial. This is one of the reasons Git
is intimidating to many prospective users. But, if you throw away your
assumptions of how version control should work, you'll find that Git is
actually simpler than most systems, but capable of more.
&lt;/p&gt;

&lt;p&gt;
In this article, I cover some of the fundamentals of how Git works and
stores data before moving on to discuss basic usage and work flow. I
found that knowing what is going on behind the scenes makes it much
easier to understand Git's many features and capabilities. Certain parts
of Git that I previously had found complicated suddenly were easy and
straightforward after spending a little time learning how it worked.
&lt;/p&gt;

&lt;p&gt;
I find Git's design to be fascinating in and of itself. I peered behind
the curtain, expecting to find a massively complex machine, and instead saw
only a little hamster running in a wheel. Then I realized 
a complicated design not only wasn't needed, but also wouldn't add
any value.
&lt;/p&gt;

&lt;h3&gt;
Git Object Repository&lt;/h3&gt;

&lt;p&gt;
Git, at its core, is a simple indexed name/value database. It stores
pieces of data (values) in "objects" with unique names. But, it does this
somewhat differently from most systems. Git operates on the principle of
"content-addressed storage", which means the names are derived from the
values. An object's name is chosen automatically by its content's SHA1
checksum—a 40-character string like this:

&lt;/p&gt;&lt;pre&gt;&lt;code&gt;
1da177e4c3f41524e886b7f1b8a0c1fc7321cac2
&lt;/code&gt;&lt;/pre&gt;


&lt;p&gt;
SHA1 is cryptographically strong, which guarantees a different checksum for
different data (the actual risk of two different pieces of data sharing
the same SHA1 checksum is infinitesimally small). The same chunk of data
always will have the same SHA1 checksum, which always will identify only
that chunk of data. Because object names are SHA1 checksums, they identify
the object's content while being truly globally unique—not just to one
repository, but to all repositories everywhere, forever.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/git-revision-control-perfected" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 24 Aug 2011 14:00:00 +0000</pubDate>
    <dc:creator>Henry Van Styn</dc:creator>
    <guid isPermaLink="false">1023997 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
