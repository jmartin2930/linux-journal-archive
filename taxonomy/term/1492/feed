<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/silicon-valley">
  <channel>
    <title>Silicon Valley</title>
    <link>https://www.linuxjournal.com/tag/silicon-valley</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Book Review: Valley of Genius: The Uncensored History of Silicon Valley (As Told by the Hackers, Founders, and Freaks Who Made It Boom) by Adam Fisher</title>
  <link>https://www.linuxjournal.com/content/book-review-valley-genius-uncensored-history-silicon-valley-told-hackers-founders-and</link>
  <description>  &lt;div data-history-node-id="1340105" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/valleyofgeniuscover_0.jpg" width="800" height="400" alt="Valley of Genius book cover" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/petros-koutoupis" lang="" about="https://www.linuxjournal.com/users/petros-koutoupis" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Petros Koutoupis&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
I don't know where to begin—and I mean that in a very positive
way. I can best describe &lt;em&gt;Valley of Genius: The Uncensored History of 
Silicon Valley (As Told by the Hackers, Founders, and Freaks Who Made It Boom)&lt;/em&gt;
as a "literary documentary". The book provides a sort of oral
history of the Valley from the legends who built it.
&lt;/p&gt;

&lt;p&gt;
The author, Adam Fisher, grew up in Silicon Valley. He continues
to live in the Bay Area, so he's been exposed to many of
the early technologies created in the region. He eventually
became a computer programmer and writer, writing for &lt;em&gt;Wired&lt;/em&gt; magazine
and other publications. &lt;em&gt;Valley of Genius&lt;/em&gt; is his first book,
but he wrote very little of it—and he didn't need to
do much more than
piece together the many interviews he conducted to form a wonderful
and continuous narrative that begins as early as the 1950s.
&lt;/p&gt;

&lt;p&gt;
The story starts off with the very first computer that was more than
just a super calculator created by Doug Engelbart.
With a small team, he built a prototype: the oN-Line System,
or NLS. It even was equipped with a "mouse"! The story continues
on to the first video games manufactured by Nolan Bushnell and company
in their pre-Atari days.
&lt;/p&gt;

&lt;p&gt;
The book also details how, in parallel, Engelbart's prototype
inspired the computers of the future developed at Xerox PARC, while
the &lt;em&gt;Spacewar&lt;/em&gt; video game would motivate a young Steve Wozniak not
only to help Steve Jobs create video games for the later Atari, but also
eventually to build the original Apple computer.
&lt;/p&gt;

&lt;p&gt;
The narrative progresses with the birth of Apple, the
company, was born and took the world of personal computing by
storm—at least initially. What followed was an emotional roller
coaster. The Apple II was a success, and up until Jobs looked to
Alan Kay's visions preserved in the Xerox Alto, Apple continued to fail,
but then later turned it all around with the Macintosh, as the story goes.
&lt;/p&gt;

&lt;p&gt;
The book covers the evolving hardware (and software), and how the culture it
nurtured evolved along with it. It explores how the early versions of the internet
connected the youngest and brightest, and how ideas were shared—all of
them centered around the concept of openness.
&lt;/p&gt;

&lt;p&gt;
It looks at how passionate people
started flame wars, and how publications, such as &lt;em&gt;Wired&lt;/em&gt;, captured those
times and emotions best.
The book explores how &lt;em&gt;Wired&lt;/em&gt; also rode the internet wave by shifting
some of that focus toward its HotWired website.
It considers the early
days of the internet, at a time when it was all research and
bulletin-board systems (or BBSes), and the problem of how to navigate this
new World
Wide Web. It describes how early web browsers, such as Mosaic and Netscape Navigator
(the Mosaic killer or Mozilla), solved this need—and with it,
helping to open the internet to more users.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/book-review-valley-genius-uncensored-history-silicon-valley-told-hackers-founders-and" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 07 Sep 2018 13:08:08 +0000</pubDate>
    <dc:creator>Petros Koutoupis</dc:creator>
    <guid isPermaLink="false">1340105 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
