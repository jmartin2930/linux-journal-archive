<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/yocto-project">
  <channel>
    <title>Yocto Project</title>
    <link>https://www.linuxjournal.com/tag/yocto-project</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Linux IoT Development: Adjusting from a Binary OS to the Yocto Project Workflow</title>
  <link>https://www.linuxjournal.com/content/linux-iot-development-adjusting-binary-os-yocto-project-workflow</link>
  <description>  &lt;div data-history-node-id="1340660" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--214042171.jpg" width="900" height="590" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/mirza-krak" lang="" about="https://www.linuxjournal.com/users/mirza-krak" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Mirza Krak&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Introducing the Yocto Project and the benefits of using it in embedded
Linux development.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In embedded Linux development, there are two approaches when it comes to what
operating system to run on your device. You either build your own distribution
(with tools such as Yocto/OpenEmbedded-Core, Buildroot and so on), or you use a
binary distribution where Debian and derivatives are common.
&lt;/p&gt;

&lt;p&gt;
It's common to start out with a binary distribution. This is a natural
approach, because it's a familiar environment for most people who have used
Linux on a PC. All the commodities are in place, and someone else has created
the distribution image for you to download. There normally are custom vendor
images for specific hardware that contain optimizations to make it easy to
get started to utilize your hardware fully.
&lt;/p&gt;

&lt;p&gt;
Any package imaginable is an &lt;code&gt;apt install&lt;/code&gt; command away. This, of
course, makes it suitable for prototyping and evaluation, giving you a head
start in developing your application and your product. In some cases, you
even might ship pre-series devices using this setup to evaluate your
idea and product further. This is referred to as the "golden image"
approach and involves the following steps:
&lt;/p&gt;

&lt;ol&gt;&lt;li&gt;
Flash the downloaded Debian image to an SD card.
&lt;/li&gt;

&lt;li&gt;
Boot the SD card, log in and make any modifications needed (for example, installing
custom applications).
Once all the modifications are complete, this becomes your golden image.
&lt;/li&gt;

&lt;li&gt;
Duplicate the SD card into an image on your workstation (for example, using
&lt;code&gt;dd&lt;/code&gt;).
&lt;/li&gt;

&lt;li&gt;
Flash the "golden image" to a fleet of devices.
&lt;/li&gt;&lt;/ol&gt;&lt;p&gt;
And every time you need to make a change, you just repeat steps 2–4, with one
change—that is, you boot the already saved "golden image" in step
2 instead of the "vanilla" image.
&lt;/p&gt;

&lt;p&gt;
At a certain point, the approach of downloading a pre-built distribution image
and applying changes to it manually will become a problem, as it does not
scale well and is error-prone due to the amount of manual labor that can
lead to inconsistent output. The optimization would be to find ways to
automate this, generating distribution images that contain your applications
and your configuration in a reproducible way.
&lt;/p&gt;

&lt;p&gt;
This is a crossroad where you decide either to stick with a binary
distribution or move your idea and the result of the evaluation and
prototyping phase to a tool that's able to generate custom distributions and
images in a reproducible and automated way.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/linux-iot-development-adjusting-binary-os-yocto-project-workflow" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 11 Jul 2019 11:30:00 +0000</pubDate>
    <dc:creator>Mirza Krak</dc:creator>
    <guid isPermaLink="false">1340660 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
