<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/blogging">
  <channel>
    <title>Blogging</title>
    <link>https://www.linuxjournal.com/tag/blogging</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Will Anything Make Linux Obsolete?</title>
  <link>https://www.linuxjournal.com/content/will-anything-make-linux-obsolete</link>
  <description>  &lt;div data-history-node-id="1339379" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12180f1.png" width="375" height="372" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Remember blogging? Hell, remember magazine publishing? Shouldn't be hard.
You're reading some now.
&lt;/p&gt;

&lt;p&gt;
Both are still around, but they're obsolete—at least relatively. Two cases
in point: &lt;a href="http://blogs.harvard.edu/doc"&gt;my blog&lt;/a&gt;
and &lt;em&gt;Linux
Journal&lt;/em&gt;.
&lt;/p&gt;

&lt;p&gt;
Back when blogging was a thing, in the early 2000s, about 20,000
people subscribed to RSS feeds of my original blog (1999–2007, still
mothballed &lt;a href="http://doc.weblogs.com"&gt;here&lt;/a&gt;). At its peak, I posted many times per day and had a strong
sense of connection with my readership.
&lt;/p&gt;

&lt;p&gt;
Same went, by the way, for my postings in &lt;em&gt;Linux
Journal&lt;/em&gt;, on our website
and on one of our own blogs, called IT Garage—lots of readers, lots of
engagement.
&lt;/p&gt;

&lt;p&gt;
Most early bloggers were journalists by profession or avocation—good
writers, basically. Some blogs turned into online pubs. BoingBoing,
TechCrunch and TPM all started as blogs.
&lt;/p&gt;

&lt;p&gt;
But blogging began to wane after Twitter and Facebook showed up in 2006.
After that journalism also waned, as "content generation" became the way to
fill online publications. Participating in "social media" also became a
requisite function for journalists still hoping to stay active online (if
not also employed).
&lt;/p&gt;

&lt;p&gt;
These days, I blog only a few times per month, for readers that range in number
from dozens to hundreds. Usually I duplicate those posts in
&lt;em&gt;Medium&lt;/em&gt;, where
they get about the same numbers. Meanwhile, I have 23.7k followers on
Twitter (as @dsearls). Although that's a goodly number, you could say the same
for the average parking space. (Which, if it could speak, might say "Hey, I
had 25k impressions on passing drivers today and engaged 15.") From
what I can tell from counting clicks of shortlinks I produce with Bit.ly,
most of my tweets are clicked on by a few dozen people, tops. I'd gladly
trade all my followers (and my Klout score of 81) for the actual readers I
had in my old blog. But alas, this is now.
&lt;/p&gt;

&lt;p&gt;
Thanks to loyal subscribers, &lt;em&gt;Linux Journal&lt;/em&gt; is still trucking along, proving it is possible to operate a journal that isn't just another sluice for
"content".
&lt;/p&gt;

&lt;p&gt;
But we have to face the facts here: content production has clearly
obsolesced journalism—just like TV obsolesced radio, cars obsolesced
horses and printing obsolesced scribes. All of the obsolesced things do
persist, but in a diminished state relative to what obsolesced
them.
&lt;/p&gt;

&lt;p&gt;
To understand why and how, it helps to raid the works of Marshall McLuhan,
the media scholar best known for saying "the media is the
message" (or "the
massage"—he said both) and coining the term "global
village" decades
before the internet materialized one.
&lt;/p&gt;

&lt;p&gt;
The analytic system McLuhan and his colleagues created for understanding
how one medium obsolesces another is the &lt;em&gt;tetrad&lt;/em&gt; (Greek
for &lt;em&gt;group of four&lt;/em&gt;).
Every medium, he said, does four things. These are discovered in answers to
four questions:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/will-anything-make-linux-obsolete" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 09 May 2017 11:27:19 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1339379 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
