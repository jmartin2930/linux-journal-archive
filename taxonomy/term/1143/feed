<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/single-board-computers">
  <channel>
    <title>Single-Board Computers</title>
    <link>https://www.linuxjournal.com/tag/single-board-computers</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Piventory: LJ Tech Editor's Personal Stash of Raspberry Pis and Other Single-Board Computers</title>
  <link>https://www.linuxjournal.com/content/piventory-lj-tech-editors-personal-stash-raspberry-pis-and-other-single-board-computers</link>
  <description>  &lt;div data-history-node-id="1339868" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/RPI-image_2.jpg" width="400" height="263" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;It's like an extra-geeky episode of &lt;em&gt;Cribs&lt;/em&gt; featuring single-board
computers.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
I'm a big fan of DIY projects and think that there is a lot of value
in doing something yourself instead of relying on some third party. I
mow my own lawn, change my own oil and do most of my own home repairs,
and because of my background in system administration, you'll find
all sorts of DIY servers at my house too. In the old days, geeks like
me would have stacks of loud power-hungry desktop computers around and
use them to learn about Linux and networking, but these days, VMs and
cloud services have taken their place for most people. I still like
running my own servers though, and thanks to the advent of these tiny,
cheap computers like the Raspberry Pi series, I've been able to replace
all of my home services with a lot of different small, cheap, low-power
computers.
&lt;/p&gt;

&lt;p&gt;
Occasionally, I'll hear people talk about how they have a Raspberry Pi
or some other small computer lying around, but they haven't figured out
quite what to do with it yet. And it always shocks me, because I have a house full of
those small computers doing all sorts of things, so in this article, I
describe my personal
"Piventory"—an inventory of all of the little low-power computers that
stay running around my house. So if you're struggling to figure out
what to do with your own Raspberry Pi, maybe this article will give you
some inspiration.
&lt;/p&gt;

&lt;h3&gt;
Primary NAS and Central Server&lt;/h3&gt;

&lt;p&gt;
In &lt;a href="https://www.linuxjournal.com/content/papas-got-brand-new-nas"&gt;"Papa's
Got a Brand New NAS"&lt;/a&gt; I wrote about my search for a replacement
for my rackmount server that acted as a Network-Attached Storage (NAS)
for my house, along with a bunch of other services. Ultimately, I found
that I could replace the whole thing with an ODroid XU4. Because of its
octo-core ARM CPU, gigabit networking and high-speed USB3 port, I was
able to move my hard drives over to a Mediasonic Probox USB3 disk array
and set up a new low-power NAS that paid for itself in electricity costs.
&lt;/p&gt;

&lt;p&gt;
In addition to a NAS, this server provides a number of backup services
for my main server that sits in a data center. It acts as a backup mail
server, authoritative DNS, and it also provides a VPN so I can connect to
my home network from anywhere in the world—not bad for a little $75
ARM board.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/figure1.jpg" width="975" height="1300" alt="""" class="image-max_1300x1300" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. Papa's New NAS&lt;/em&gt;
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/piventory-lj-tech-editors-personal-stash-raspberry-pis-and-other-single-board-computers" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 14 Jun 2018 12:00:00 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1339868 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Raspberry Pi Alternatives</title>
  <link>https://www.linuxjournal.com/content/raspberry-pi-alternatives</link>
  <description>  &lt;div data-history-node-id="1339613" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12261f1.jpg" width="800" height="600" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;em&gt;A look at some of the many interesting Raspberry Pi
competitors.&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
The phenomenon behind the Raspberry Pi computer series has been
pretty amazing. It's obvious why it has become so popular for Linux
projects—it's a low-cost computer that's actually quite capable for the
price, and the GPIO pins allow you to use it in a number of electronics
projects such that it starts to cross over into Arduino territory in
some cases. Its overall popularity has spawned many different add-ons
and accessories, not to mention step-by-step guides on how to use the
platform. I've personally written about Raspberry Pis often in
this space, and in my own home, I use one to control a beer fermentation
fridge, one as my media PC, one to control my 3D printer and one as a
handheld gaming device.
&lt;/p&gt;

&lt;p&gt;
The popularity of the Raspberry Pi also has spawned competition, and there
are all kinds of other small, low-cost, Linux-powered Raspberry Pi-like
computers for sale—many of which even go so far as to add
"Pi" to their
names. These computers aren't just clones, however. Although some share
a similar form factor to the Raspberry Pi, and many also copy the GPIO
pinouts, in many cases, these other computers offer features unavailable
in a traditional Raspberry Pi. Some boards offer SATA, Wi-Fi or Gigabit
networking; others offer USB3, and still others offer higher-performance CPUs or
more RAM. When you are choosing a low-power computer for a project or as
a home server, it pays to be aware of these Raspberry Pi alternatives, as
in many cases, they will perform much better. So in this article, I discuss
some alternatives to Raspberry Pis that I've used personally,
their pros and cons, and then provide some examples of where they work best.
&lt;/p&gt;

&lt;h3&gt;
Banana Pi&lt;/h3&gt;

&lt;p&gt;
I've mentioned the Banana Pi before in past articles (see "Papa's Got a Brand
New NAS" in the September 2016 issue and "Banana
Backups" in the September 2017 issue), and it's a great choice when you want
a board with a similar form factor, similar CPU and RAM specs, and a
similar price (~$30) to a Raspberry Pi but need faster I/O. The Raspberry
Pi product line is used for a lot of home server projects, but it limits
you to 10/100 networking and a USB2 port for additional storage. Where
the Banana Pi product line really shines is in the fact that it includes both
a Gigabit network port and SATA port, while still having similar GPIO
expansion options and running around the same price as a Raspberry
Pi.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/raspberry-pi-alternatives" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 22 Jan 2018 13:14:11 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1339613 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
