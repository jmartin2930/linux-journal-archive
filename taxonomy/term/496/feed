<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/amazon">
  <channel>
    <title>Amazon</title>
    <link>https://www.linuxjournal.com/tag/amazon</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Time for Net Giants to Pay Fairly for the Open Source on Which They Depend</title>
  <link>https://www.linuxjournal.com/content/time-net-giants-pay-fairly-open-source-which-they-depend</link>
  <description>  &lt;div data-history-node-id="1340213" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Money-8204584.jpg" width="800" height="590" alt="money" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/glyn-moody" lang="" about="https://www.linuxjournal.com/users/glyn-moody" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Glyn Moody&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Net giants depend on open source: so where's the gratitude?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Licensing lies at the heart of open source.
Arguably, free software began
with &lt;a href="https://www.gnu.org/licenses/old-licenses/gpl-1.0.html"&gt;the
publication of the GNU GPL in 1989&lt;/a&gt;. And since then, open-source projects
are defined as such by virtue of &lt;a href="https://opensource.org/licenses"&gt;the licenses they adopt&lt;/a&gt; and
whether the latter meet the &lt;a href="https://opensource.org/osd"&gt;Open Source
Definition&lt;/a&gt;. The continuing importance of licensing is shown by the
periodic flame wars that erupt in this area. Recently, there have been two
such flarings of strong feelings, both of which raise important issues.
&lt;/p&gt;

&lt;p&gt;
First, we had the incident with &lt;a href="https://lernajs.io"&gt;Lerna&lt;/a&gt;, "a
tool for managing JavaScript projects with multiple packages". It came about
as a result of the way the US Immigration and Customs Enforcement (ICE) has
been &lt;a href="https://twitter.com/ACLU/status/1033084026893070338"&gt;separating
families&lt;/a&gt; and &lt;a href="https://www.bbc.com/news/world-us-canada-44518942"&gt;holding children in
cage-like cells&lt;/a&gt;. The Lerna core team was appalled by this behavior and
wished to do something concrete in response. As a result, it &lt;a href="https://github.com/lerna/lerna/pull/1616"&gt;added an extra clause to the
MIT license&lt;/a&gt;, which forbade a list of companies, including Microsoft,
Palantir, Amazon, Motorola and Dell, from being permitted to use the code:
&lt;/p&gt;

&lt;blockquote&gt;&lt;p&gt;
For the companies that are known supporters of ICE: Lerna will no
longer be licensed as MIT for you. You will receive no licensing rights and
any use of Lerna will be considered theft. You will not be able to pay for a
license, the only way that it is going to change is by you publicly tearing
up your contracts with ICE.&lt;/p&gt;&lt;/blockquote&gt;

&lt;p&gt;
Many sympathized with the feelings about the actions of the ICE and the
intent of the license change. However, many also pointed out that such a
move went against the core principles of both free software and open source.
&lt;a href="https://www.gnu.org/philosophy/free-sw.en.html"&gt;Freedom 0 of the
Free Software Definition&lt;/a&gt; is "The freedom to run the program as you wish,
for any purpose." Similarly, the Open Source Definition requires "No
Discrimination Against Persons or Groups" and "No Discrimination Against
Fields of Endeavor". The situation is clear cut, and it didn't take long for
the Lerna team to realize their error, and &lt;a href="https://github.com/lerna/lerna/pull/1633"&gt;they soon reverted the
change&lt;/a&gt;:

&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/time-net-giants-pay-fairly-open-source-which-they-depend" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 05 Nov 2018 13:00:00 +0000</pubDate>
    <dc:creator>Glyn Moody</dc:creator>
    <guid isPermaLink="false">1340213 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>CloudWatch Is of the Devil, but I Must Use It</title>
  <link>https://www.linuxjournal.com/content/cloudwatch-devil-i-must-use-it</link>
  <description>  &lt;div data-history-node-id="1340200" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Cloud-Computing-Symbol-20809646.jpg" width="667" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/corey-quinn-0" lang="" about="https://www.linuxjournal.com/users/corey-quinn-0" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Corey Quinn&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Let's talk about Amazon CloudWatch.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
For those fortunate enough to not be stuck in the weeds of Amazon Web
Services (AWS), CloudWatch is, and I quote from the official
&lt;a href="https://aws.amazon.com/cloudwatch"&gt;AWS description&lt;/a&gt;, "a monitoring and
management service built for developers, system operators, site reliability
engineers (SRE), and IT managers." This is all well and good, except for the
part where there isn't a single named constituency who enjoys working with
the product. Allow me to dispense some monitoring heresy.
&lt;/p&gt;

&lt;p&gt;
Better, let me describe this in the context of the 14 &lt;a href="https://www.amazon.jobs/principles"&gt;Amazon
Leadership Principles&lt;/a&gt; that reportedly guide every decision Amazon makes.
When you take a hard look at CloudWatch's complete failure across all
14 Leadership Principles, you wonder how this product ever made it out
the door in its current state.
&lt;/p&gt;

&lt;h3&gt;
"Frugality"&lt;/h3&gt;

&lt;p&gt;
I'll start with billing. Normally left for the tail end of articles like
this, the CloudWatch billing paradigm is so terrible, I'm leading with
it instead. You get billed per metric, per month. You get billed per
thousand metrics you request to view via the API. You get billed per
dashboard per month. You get billed per alarm per month. You get charged for
logs based upon data volume ingested, data volume stored and "vended logs"
that get published natively by AWS services on behalf of the customer. And,
you get billed per custom event. All of this can be summed up best as
"nobody on the planet understands how your CloudWatch metrics and logs get
billed", and it leads to scenarios where monitoring vendors can inadvertently
cost you thousands of dollars by polling CloudWatch too frequently. When the
AWS charges are larger than what you're paying your monitoring vendor, it's
not a wonderful feeling.
&lt;/p&gt;

&lt;h3&gt;
"Invent and Simplify"&lt;/h3&gt;

&lt;p&gt;
CloudWatch Logs, CloudWatch Events, Custom Metrics, Vended Logs and Custom
Dashboards all mean different things internally to CloudWatch from what you'd
expect, compared to metrics solutions that actually make some fathomable
level of sense. There are, thus, multiple services that do very different
things, all operating under the "CloudWatch" moniker. For example, it's not
particularly intuitive to most people that scheduling a Lambda function to
invoke once an hour requires a custom CloudWatch Event. It feels overly
complicated, incredibly confusing, and very quickly, you find yourself in a
situation where you're having to build complex relationships to monitor
things that are themselves far simpler.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/cloudwatch-devil-i-must-use-it" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 31 Oct 2018 11:30:00 +0000</pubDate>
    <dc:creator>Corey Quinn</dc:creator>
    <guid isPermaLink="false">1340200 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Shall We Study Amazon's Pricing Together?</title>
  <link>https://www.linuxjournal.com/content/shall-we-study-amazons-pricing-together</link>
  <description>  &lt;div data-history-node-id="1340108" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/pricinggun_0.jpg" width="800" height="400" alt="pricing gun" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Is it possible to figure out how we're being profiled online?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;This past July, I spent a quality week getting rained out in a series of brainstorms by alpha data geeks at the &lt;a href="https://www.strategic-pr.com/bi-analyst-summit/"&gt;Pacific Northwest BI &amp; Analytics Summit&lt;/a&gt; in Rogue River, Oregon. Among the many things I failed to understand fully there was how much, or how well, we could know about how the commercial sites and services of the online world deal with us, based on what they gather about us, on the fly or over time, as we interact with them.&lt;/p&gt;

&lt;p&gt;The short answer was "not much". But none of the experts I talked to said "Don't bother trying." On the contrary, the consensus was that the sums of data gathered by most companies are (in the words of one expert) "spaghetti balls" that are hard, if not possible, to unravel completely. More to my mission in life and work, they said it wouldn't hurt to have humans take some interest in the subject.&lt;/p&gt;

&lt;p&gt;In fact, that was pretty much why I was invited there, as a Special Guest. My topic was "&lt;a href="https://www.strategic-pr.com/doc-searls"&gt;When customers are in full command of what companies do with their data—and data about them&lt;/a&gt;". As it says at that link, "The end of this story...is a new beginning for business, in a world where customers are fully in charge of their lives in the marketplace—both online and off: a world that was implicit in both the peer-to-peer design of the Internet and the nature of public markets in the pre-industrial world."&lt;/p&gt;

&lt;p&gt;Obviously, this hasn't happened yet.&lt;/p&gt;

&lt;p&gt;This became even more obvious during a break when I drove to our AirBnB nearby. By chance, my rental car radio was tuned to a program called &lt;a href="http://blogs.wgbh.org/innovation-hub/2018/7/27/scurvy-surgery-history-randomized-trials/"&gt;From Scurvy to Surgery: The History Of Randomized Trials&lt;/a&gt;. It was an &lt;a href="http://blogs.wgbh.org/innovation-hub/"&gt;Innovation Hub&lt;/a&gt; interview with &lt;a href="https://twitter.com/ALeighMP"&gt;Andrew Leigh&lt;/a&gt;, Ph.D. (&lt;a href="https://en.wikipedia.org/wiki/Andrew_Leigh"&gt;@ALeighMP&lt;/a&gt;), economist and member of the Australian Parliament, discussing his new book, &lt;a href="https://yalebooks.yale.edu/book/9780300236125/randomistas"&gt;&lt;em&gt;Randomistas: How Radical Researchers Are Changing Our World&lt;/em&gt;&lt;/a&gt; (Yale University Press, 2018). At one point, Leigh reported that "One expert says, 'Every pixel on Amazon's home page has had to justify its existence through a randomized trial.'"&lt;/p&gt;

&lt;p&gt;I thought, &lt;em&gt;Wow. How much of my own experience of Amazon has been as a randomized test subject? And can I possibly be in anything even remotely close to full charge of my own life inside Amazon's vast silo?&lt;/em&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/shall-we-study-amazons-pricing-together" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 02 Oct 2018 11:30:00 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1340108 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>What Does "Ethical" AI Mean for Open Source?</title>
  <link>https://www.linuxjournal.com/content/what-does-ethical-ai-mean-open-source</link>
  <description>  &lt;div data-history-node-id="1340006" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12496c_0.png" width="800" height="400" alt="I'm Sorry, Dave. I'm afraid I can't do that." typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/glyn-moody" lang="" about="https://www.linuxjournal.com/users/glyn-moody" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Glyn Moody&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Artificial intelligence is a threat—and an opportunity—for open
source.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
It would be an understatement to say that artificial intelligence (AI) is
much in the news these days. It's widely viewed as likely to usher in the
next big step-change in computing, but a recent interesting
development in the field has particular implications for open source.
It concerns the rise of "ethical" AI.
&lt;/p&gt;

&lt;p&gt;
In October 2016, the White House Office of Science and Technology Policy, the
European Parliament's Committee on Legal Affairs and, in the UK, the House
of Commons' Science and Technology Committee, all released reports on &lt;a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2906249"&gt;how to
prepare for the future of AI&lt;/a&gt;, with ethical issues being an important
component
of those reports. At the beginning of last year, the &lt;a href="https://futureoflife.org/ai-principles/?cn-reloaded=1"&gt;Asilomar AI
Principles&lt;/a&gt; were published, followed by the &lt;a href="https://www.montrealdeclaration-responsibleai.com/the-declaration"&gt;Montreal
Declaration for a Responsible Development of Artificial Intelligence&lt;/a&gt;,
announced in November 2017.
&lt;/p&gt;

&lt;p&gt;
Abstract discussions of what ethical AI might or should mean became very real
in March 2018. It was revealed then that Google had won a share of
the contract for the &lt;a href="https://www.govexec.com/media/gbc/docs/pdfs_edit/establishment_of_the_awcft_project_maven.pdf"&gt;Pentagon's
Project Maven&lt;/a&gt;, which uses artificial intelligence to interpret huge
quantities of video images collected by aerial drones in order &lt;a href="https://gizmodo.com/google-is-helping-the-pentagon-build-ai-for-drones-1823464533"&gt;to
improve the targeting of subsequent drone strikes&lt;/a&gt;. When this became
known, it caused a firestorm at Google. &lt;a href="https://www.nytimes.com/2018/04/04/technology/google-letter-ceo-pentagon-project.html"&gt;Thousands
of people there signed an internal petition&lt;/a&gt; addressed to the company's
CEO, Sundar Pichai, &lt;a href="https://static01.nyt.com/files/2018/technology/googleletter.pdf"&gt;asking
him to cancel the project&lt;/a&gt;. Hundreds of researchers and academics sent an
&lt;a href="https://www.icrac.net/open-letter-in-support-of-google-employees-and-tech-workers"&gt;open
letter supporting them&lt;/a&gt;, and some &lt;a href="https://gizmodo.com/google-employees-resign-in-protest-against-pentagon-con-1825729300"&gt;Google
employees resigned in protest&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/what-does-ethical-ai-mean-open-source" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 20 Aug 2018 11:30:00 +0000</pubDate>
    <dc:creator>Glyn Moody</dc:creator>
    <guid isPermaLink="false">1340006 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Leaving the Land of the Giants</title>
  <link>https://www.linuxjournal.com/content/leaving-land-giants</link>
  <description>  &lt;div data-history-node-id="1084262" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/shutterstock_33265177_0.jpg" width="500" height="340" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;The next revolution will be personal. Just like the last three were.
&lt;/p&gt;
&lt;p&gt;
The cover of the December 1st–7th 2012 issue of &lt;em&gt;The
Economist&lt;/em&gt; shows four giant squid
battling each other (&lt;a href="http://www.economist.com/printedition/2012-12-01"&gt;http://www.economist.com/printedition/2012-12-01&lt;/a&gt;). The headline reads,
"Survival of the biggest: The internet's
warring giants". The squid are Amazon, Apple, Facebook and Google. Inside, the story is
filed under "Briefing: Technology giants at war". The headline below the title graphic
reads, "Another game of thrones"
(&lt;a href="http://www.economist.com/news/21567361-google-apple-facebook-and-amazon-are-each-others-throats-all-sorts-ways-another-game"&gt;http://www.economist.com/news/21567361-google-apple-facebook-and-amazon-are-each-others-throats-all-sorts-ways-another-game&lt;/a&gt;).
The opening slug line reads "Google, Apple, Facebook
and Amazon are at each other's throats in all sorts of ways." (Raising the metaphor
count to three.)
&lt;/p&gt;

&lt;p&gt;
Now here's the question: &lt;em&gt;Is that all that's going on? Is it not possible that, in five,
ten or twenty years we'll realize that the action that mattered in the early
twenty-teens was happening in the rest of the ocean, and not just among the mollusks
with the biggest tentacles?&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
War stories are always interesting, and very easy to tell because the format is
formulaic. Remember Linux vs. Microsoft, personalized as Linus vs.
Bill? Never mind
that Linux as a server OS worked from the start with countless millions (or even
billions) of Windows clients. Or that both Linus and Bill had other fish to fry from
the start. But personalization is cheap and easy, and there was enough antipathy on
both sides to stoke the story-telling fires, so that's what we got. Thus, today we might
regard Linux as a winner and Microsoft as a loser (or at least trending in that
direction). The facts behind (or ignored by) the stories mostly say that both entities
have succeeded or failed largely on their own merits.
&lt;/p&gt;

&lt;p&gt;
Here's a story that illustrates how stories can both lead and mislead.
&lt;/p&gt;

&lt;p&gt;
The time frame was the late 1980s and early 1990s, and the "war" was between CISC (Complex
Instruction Set Computing,
&lt;a href="http://en.wikipedia.org/wiki/Complex_instruction_set_computing"&gt;http://en.wikipedia.org/wiki/Complex_instruction_set_computing&lt;/a&gt;) and RISC (Reduced
Instruction Set Computing,
&lt;a href="http://en.wikipedia.org/wiki/Reduced_instruction_set_computing"&gt;http://en.wikipedia.org/wiki/Reduced_instruction_set_computing&lt;/a&gt;). The popular
CPUs at the time were CISC, and the big two CISC competitors were Intel's x86 and
Motorola's 68000. Intel was winning that one, so Motorola and other chip makers pushed
RISC as the Next Big Thing. Motorola had an early RISC lead with the 88000 (before
later pivoting to the PowerPC).
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/leaving-land-giants" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 05 Feb 2013 18:44:57 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1084262 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
