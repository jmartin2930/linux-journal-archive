<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/media">
  <channel>
    <title>Media</title>
    <link>https://www.linuxjournal.com/tag/media</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>The Digital Unconformity</title>
  <link>https://www.linuxjournal.com/content/digital-unconformity</link>
  <description>  &lt;div data-history-node-id="1340466" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/digital-unconformity1_sm.jpg" width="900" height="546" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Will our digital lives leave a fossil record? Or any record at all?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In the library of Earth's history, there are missing books. All were written
in rock that is now gone. The greatest example of "gone" rock first
was observed by &lt;a href="https://en.wikipedia.org/wiki/John_Wesley_Powell"&gt;John
Wesley Powell&lt;/a&gt; in 1869, on his expedition by boat through the
Grand Canyon. Floating down the Colorado river, he saw the canyon's
mile-thick layers of reddish sedimentary rock resting on a basement of gray
non-sedimentary rock, and he correctly assumed that the upper layers did not
continue from the bottom one. He knew time had passed between the basement
rock and the floors of rock above it, but he didn't know how much. The answer
turned out to be more than a billion years. The walls of the Grand Canyon say
nothing about what happened during that time. Geology calls that nothing an
&lt;em&gt;unconformity&lt;/em&gt;.
&lt;/p&gt;

&lt;p&gt;
In fact, Powell's unconformity prevails worldwide. The name for this worldwide
missing rock is &lt;a href="https://www.google.com/search?q=%22the+great+unconformity%22"&gt;the Great
Unconformity&lt;/a&gt;. Because of that unconformity, geology
knows comparatively little about what happened in the world through stretches
of time ranging regionally up to 1.6 billion years. All of those stretches
end abruptly with the &lt;a href="https://en.wikipedia.org/wiki/Cambrian_explosion"&gt;Cambrian
Explosion&lt;/a&gt;, which began about 541 million years
ago. Many theories attempt to explain what erased all that geological
history, but the prevailing paradigm is perhaps best expressed in
&lt;a href="https://www.pnas.org/content/early/2019/01/07/1804350116"&gt;"Neoproterozoic
glacial origin of the Great Unconformity"&lt;/a&gt;, published on the
last day of 2018 by nine geologists writing for the National Academy of
Sciences.
&lt;/p&gt;

&lt;p&gt;
Put simply, they blame snow. Lots of it—enough to turn the planet into one
giant snowball, already informally called &lt;a href="https://en.wikipedia.org/wiki/Snowball_Earth"&gt;Snowball Earth&lt;/a&gt;. A more accurate
name for this time would be Glacierball Earth, because glaciers, all formed
from snow, apparently covered most or all of Earth's land during the Great
Unconformity—and most or all of the seas as well.
&lt;/p&gt;

&lt;p&gt;
The relevant fact about glaciers is that they don't sit still. They spread
and slide sideways, pressing and pushing immensities of accumulated ice down
on landscapes that they pulverize and scrape against adjacent landscapes,
abrading their way through mountains and across hills and plains like a
trowel spreading wet cement. Thus, it seems glaciers scraped a vastness of
geological history off the Earth's surface and let plate tectonics hide the
rest of the evidence. As a result, the stories of Earth's missing history are
told only by younger rock that remembers only that a layer of moving
ice had erased pretty much everything other than a signature on its work.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/digital-unconformity" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 06 Mar 2019 13:00:00 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1340466 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Neuranet's Flexitive</title>
  <link>https://www.linuxjournal.com/content/neuranets-flexitive</link>
  <description>  &lt;div data-history-node-id="1339531" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12237f6.png" width="800" height="451" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/james-gray" lang="" about="https://www.linuxjournal.com/users/james-gray" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;James Gray&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
The new Interactive Advertising Bureau (IAB) Standard Ad Unit Portfolio's
support for flexible ads is intended to improve the ad experience for users and
boost revenue potential for advertisers. An updated solution from Neuranet, its
&lt;a href="https://flexitive.com"&gt;Flexitive 2.0&lt;/a&gt; responsive design software, is designed to solve today's design
challenges and give media companies and agencies a significant advantage in
adapting to the IAB's new Flex Specifications and Lean Guidelines that were
released in summer 2017. 
&lt;/p&gt;

&lt;p&gt;
Flexitive 2.0 is the next generation of Neuranet's
HTML5 cloud-based platform that supports more advanced responsive design
capabilities for building high-quality, animated HTML5-based designs that adapt to
unlimited sizes across any device, operating system, app or browser. The new
standards were designed to promote user-friendly digital advertising that can
scale across device types easily. 
&lt;/p&gt;

&lt;p&gt;
Flexitive also incorporates the LEAN principles
of lightweight, encrypted, AdChoices-supported and non-invasive advertising.
Neuranet emphasizes other key features of Flexitive 2.0, such as an easy-to-use
drag-and-drop interface that requires no coding knowledge, unlimited sizing of a
single design, two-click creative design variations with instant scaling, export
for use in more than 30 ad servers and more.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/neuranets-flexitive" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 25 Oct 2017 15:24:05 +0000</pubDate>
    <dc:creator>James Gray</dc:creator>
    <guid isPermaLink="false">1339531 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Will Anything Make Linux Obsolete?</title>
  <link>https://www.linuxjournal.com/content/will-anything-make-linux-obsolete</link>
  <description>  &lt;div data-history-node-id="1339379" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12180f1.png" width="375" height="372" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Remember blogging? Hell, remember magazine publishing? Shouldn't be hard.
You're reading some now.
&lt;/p&gt;

&lt;p&gt;
Both are still around, but they're obsolete—at least relatively. Two cases
in point: &lt;a href="http://blogs.harvard.edu/doc"&gt;my blog&lt;/a&gt;
and &lt;em&gt;Linux
Journal&lt;/em&gt;.
&lt;/p&gt;

&lt;p&gt;
Back when blogging was a thing, in the early 2000s, about 20,000
people subscribed to RSS feeds of my original blog (1999–2007, still
mothballed &lt;a href="http://doc.weblogs.com"&gt;here&lt;/a&gt;). At its peak, I posted many times per day and had a strong
sense of connection with my readership.
&lt;/p&gt;

&lt;p&gt;
Same went, by the way, for my postings in &lt;em&gt;Linux
Journal&lt;/em&gt;, on our website
and on one of our own blogs, called IT Garage—lots of readers, lots of
engagement.
&lt;/p&gt;

&lt;p&gt;
Most early bloggers were journalists by profession or avocation—good
writers, basically. Some blogs turned into online pubs. BoingBoing,
TechCrunch and TPM all started as blogs.
&lt;/p&gt;

&lt;p&gt;
But blogging began to wane after Twitter and Facebook showed up in 2006.
After that journalism also waned, as "content generation" became the way to
fill online publications. Participating in "social media" also became a
requisite function for journalists still hoping to stay active online (if
not also employed).
&lt;/p&gt;

&lt;p&gt;
These days, I blog only a few times per month, for readers that range in number
from dozens to hundreds. Usually I duplicate those posts in
&lt;em&gt;Medium&lt;/em&gt;, where
they get about the same numbers. Meanwhile, I have 23.7k followers on
Twitter (as @dsearls). Although that's a goodly number, you could say the same
for the average parking space. (Which, if it could speak, might say "Hey, I
had 25k impressions on passing drivers today and engaged 15.") From
what I can tell from counting clicks of shortlinks I produce with Bit.ly,
most of my tweets are clicked on by a few dozen people, tops. I'd gladly
trade all my followers (and my Klout score of 81) for the actual readers I
had in my old blog. But alas, this is now.
&lt;/p&gt;

&lt;p&gt;
Thanks to loyal subscribers, &lt;em&gt;Linux Journal&lt;/em&gt; is still trucking along, proving it is possible to operate a journal that isn't just another sluice for
"content".
&lt;/p&gt;

&lt;p&gt;
But we have to face the facts here: content production has clearly
obsolesced journalism—just like TV obsolesced radio, cars obsolesced
horses and printing obsolesced scribes. All of the obsolesced things do
persist, but in a diminished state relative to what obsolesced
them.
&lt;/p&gt;

&lt;p&gt;
To understand why and how, it helps to raid the works of Marshall McLuhan,
the media scholar best known for saying "the media is the
message" (or "the
massage"—he said both) and coining the term "global
village" decades
before the internet materialized one.
&lt;/p&gt;

&lt;p&gt;
The analytic system McLuhan and his colleagues created for understanding
how one medium obsolesces another is the &lt;em&gt;tetrad&lt;/em&gt; (Greek
for &lt;em&gt;group of four&lt;/em&gt;).
Every medium, he said, does four things. These are discovered in answers to
four questions:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/will-anything-make-linux-obsolete" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 09 May 2017 11:27:19 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1339379 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Sicker Than Sickbeard?</title>
  <link>https://www.linuxjournal.com/content/sicker-sickbeard</link>
  <description>  &lt;div data-history-node-id="1335729" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11678nzbdronef1.jpg" width="550" height="350" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
When I wrote about Usenet and Sickbeard a while back, I got many e-mails
that I had broken the first rule of Usenet: don't talk about Usenet. I'm a
sucker for freedom though, and I can't help but share when cool programs
are available. This month, I switched from Sickbeard to NZBDrone for
managing my television shows.
&lt;/p&gt;

&lt;p&gt;
NZBDrone is a program designed to manage a local collection of television
shows. It also has the capability of working with Usenet programs to
automate the possibly illegal downloading of episodes, but that's truly
not all it's good for. NZBDrone will take your TV show files and organize
them into folders, download metadata and let you know if you're missing
files. It also will show you when your favorite shows are going to be airing next.
&lt;/p&gt;

&lt;p&gt;
Although it hasn't given me a problem, the fact that NZBDrone runs on Mono
makes me nervous. The installation guide on the &lt;a href="http://www.nzbdrone.com"&gt;http://www.nzbdrone.com&lt;/a&gt; Web site
makes setup simple enough, but there will be a boatload of dependencies
that you might have to install due to its Mono infrastructure.
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11678nzbdronef1.jpg" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
NZBDrone will work with your existing Plex media server, XBMC machines
and SABNzb installs, and it automatically performs most of its features if
you allow it to do so. The interface is beautiful, and even with a
large collection of television shows (I have more than 15TB of TV shows on
my server), it's very responsive. Whether you record your TV episodes,
rip your television season DVDs or find your episodes in other ways,
NZBDrone is a perfect way to manage your collection. It's so intuitive
and user-friendly, that it gets this month's Editors' Choice award!
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/sicker-sickbeard" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 11 Jun 2014 18:30:20 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1335729 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Native(ish) Netflix!</title>
  <link>https://www.linuxjournal.com/content/nativeish-netflix</link>
  <description>  &lt;div data-history-node-id="1084277" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11414netflixf1.png" width="640" height="359" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
The folks over at &lt;a href="http://www.iheartubuntu.com"&gt;http://www.iheartubuntu.com&lt;/a&gt; recently put up a challenge to
the Linux community to get Netflix to work natively under our beloved
OS. Thankfully, Erich Hoover stepped up to the challenge and patched
the Wine Project in a way to allow Firefox/Silverlight to be installed
and actually work with Netflix's DRM'd Silverlight!
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11414netflixf1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Screenshot from 
&lt;/p&gt;
&lt;p&gt;
The process is a little complex, and it involves patching source code before
compiling, but Erich plans to create a PPA with all the compiling already
done. Eventually, he intends to create a standalone Netflix-playing app
that incorporates all the pieces of Wine and Silverlight. Thankfully,
Erich didn't wait until the project was complete before sharing his
success. If you want to play native(ish) Netflix on your Linux desktop
without virtualization, check out his instructions at:
&lt;a href="http://www.iheartubuntu.com/2012/11/netflix-on-ubuntu-is-here.html"&gt;http://www.iheartubuntu.com/2012/11/netflix-on-ubuntu-is-here.html&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/nativeish-netflix" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 06 Feb 2013 19:01:43 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1084277 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Plex Media Server + Roku = Awesome</title>
  <link>https://www.linuxjournal.com/content/plex-media-server-roku-awesome</link>
  <description>  &lt;div data-history-node-id="1045424" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/plex-header.gif" width="200" height="100" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Plex always has been the Mac-friendly offshoot of XBMC. I've never considered using an Apple product for my home media center, so I've never really put much thought into it. Things have changed recently, however, and now the folks behind Plex have given the Linux community an awesome media server.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11279plexf1.jpg" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;Installing the media server is fairly straightforward. Instructions are available at &lt;a href="http://www.plexapp.com"&gt;http://www.plexapp.com&lt;/a&gt;. The server application runs on a headless Linux server and is configured via a Web interface. After you've pointed Plex Media Server at your video collection, the real magic begins.&lt;/p&gt;
&lt;p&gt;Fire up your Roku, any model, and search for the Plex channel in the Roku Channel Store. With some simple configuration, your Roku will be able to browse your entire media collection and stream HD video to your television. The responsiveness is incredible, and the video quality is astounding. I was expecting pixelated video with stuttering playback over wireless, but everything was smooth. Your local media behaves just like Netflix! For more information, check out the Plex Web site: &lt;a href="http://www.plexapp.com"&gt;http://www.plexapp.com&lt;/a&gt;.&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/plex-media-server-roku-awesome" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 13 Aug 2012 17:19:28 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1045424 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Roaming Media</title>
  <link>https://www.linuxjournal.com/content/roaming-media</link>
  <description>  &lt;div data-history-node-id="1035183" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/headphones.jpg" width="200" height="200" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-nugent" lang="" about="https://www.linuxjournal.com/users/michael-nugent" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Nugent&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p class="abstract"&gt;
Portable music doesn't need to be restricted to headphones.
Here's a step-by-step how-to on setting up a music system
that follows you around the house like a puppy.
&lt;/p&gt;
&lt;p&gt;
Like many of you, I store all my music digitally on a central server in my
home. The problem is when I walk from room to room, my music doesn't
come with me. I could carry around an iThingy or put it on my phone, but
I'd rather not have to wear headphones, and no matter how awesome they
are in their class, tiny phone speakers still are tiny phone speakers.
Fortunately, I have a lot of computer hardware lying around from past
upgrades, so it was fairly easy for me to come up with some small, older
systems for each area of my house. Now, instead of listening to music on a
little device, I use a device to tag my location and have the music follow
me wherever I go.
&lt;/p&gt;

&lt;p&gt;
The system is easy to build and uses mostly off-the-shelf open-source
programs. In addition to the player and control system, you need
a way of tagging your location in the house. I use the Bluetooth radio on
my phone, but you also could use RFID tags, Webcams with motion detection
or facial recognition, or pretty much anything else that will let the
system know where you are. For this setup though, I'm assuming
you're using a Bluetooth device.
&lt;/p&gt;

&lt;p&gt;
The central piece to this project is a server-based music player. I am
using the Music Player Dæmon (MPD), a wonderful server-based system
released under the GNU General Public License and available from the
repositories of most Linux distributions. Install the software with your
favorite package management system. In addition to this player, you need to
set up a streaming system. Icecast fulfills this requirement and also
is widely available. Install it as well.
&lt;/p&gt;

&lt;p&gt;
Configuring MPD is fairly straightforward. The default file for your
distribution is probably very similar to the example below, but you may need to change a
few things. The &lt;code&gt;music_directory&lt;/code&gt; entry should point to the
directory that contains the music files and one
&lt;code&gt;bind_to_address&lt;/code&gt;
should contain the non-loopback name or address of the server. If it 
binds only to 127.0.0.1, outside boxes may have trouble connecting to it.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/roaming-media" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 27 Mar 2012 18:36:18 +0000</pubDate>
    <dc:creator>Michael Nugent</dc:creator>
    <guid isPermaLink="false">1035183 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
