<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/thin-client">
  <channel>
    <title>thin client</title>
    <link>https://www.linuxjournal.com/tag/thin-client</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Observation: Cloud computing is nothing new</title>
  <link>https://www.linuxjournal.com/content/observation-cloud-computing-nothing-new</link>
  <description>  &lt;div data-history-node-id="1016110" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/cloud_terminals.png" width="342" height="297" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://en.wikipedia.org/wiki/Cloud_computing"&gt;Cloud computing&lt;/a&gt; is not only the latest buzz term, it might well be the model of computing that powers the 21st century. However, it’s easy to forget that personal computing, in which each user has a standalone system that can operate without a network, is itself a relatively new approach.&lt;/p&gt;
&lt;p&gt;The first practical computers were enormous behemoths composed of clicking relays and vacuum tubes. Much of the early development of these multi-ton monsters had been spurred by the allied &lt;a href="http://en.wikipedia.org/wiki/Ultra"&gt;code-breaking effort&lt;/a&gt; during World War II. For the first thirty years of the history of general purpose computers, computer time was the exclusive privilege of large institutions and governments. &lt;/p&gt;
&lt;p&gt;One of the first breakthroughs in bringing down the cost of computer access was the concept of a time-sharing system. In such a system, multiple operators can access the resources of the computer through the use of remote terminals. Here, in the form of early Teletype terminals, and later, video terminals, we see the emergence of a network topology in which computing horsepower is located in a central computer, away from the user.&lt;/p&gt;
&lt;p&gt;It was the era of the mainframe and the dumb-terminal. Typically, these  dumb terminals would lack storage or computation capability, as they  were simply a display with a keyboard. By the 1970s, an operator (usually wearing flared trousers, if the textbooks I’ve seen are accurate), would sit in front of an amber or green screened terminal, thankful that he no longer needed to wait in line in to hand in a box of carefully arranged punch-cards.&lt;/p&gt;
&lt;p&gt;Fast forward to the late 70s and a new paradigm was beginning to gain favour. If you’ve seen the film &lt;a href="http://www.imdb.com/title/tt0168122/"&gt;The Pirates of Silicon Valley&lt;/a&gt;, a dramatisation of the early years of Apple Computers, you may remember a scene in which the young Steve Wozniak is compelled to show his prototype personal computer to his employer, Hewlett Packard. In the scene that I’m talking about, Steve fears that his bosses will take his idea from him. The exchange goes something like this:&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;HP exec&lt;/strong&gt;&lt;br /&gt;Steve, it is Steve isn’t it?&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Steve Wozniak&lt;/strong&gt;&lt;br /&gt;(nods)&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;HP exec&lt;/strong&gt;&lt;br /&gt;Steve, you say that this... gadget... of yours is for ordinary people. What on earth would ordinary people want with computers?&lt;/p&gt;
&lt;p&gt;(long pause)&lt;/p&gt;
&lt;p&gt;The idea that was being mooted was that of a personal computer, that is, a self-contained computer that only requires an electrical power supply in order to operate. Singular computers that did not need to be connected to a larger computer in order to run went on to become the popular face of computing for the remainder of the 20th century.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/observation-cloud-computing-nothing-new" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 24 Nov 2010 14:12:19 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1016110 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Using Ubuntu as a thin client</title>
  <link>https://www.linuxjournal.com/content/using-ubuntu-thin-client</link>
  <description>  &lt;div data-history-node-id="1011743" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/ulogo.jpeg" width="104" height="120" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/chase-crum" lang="" about="https://www.linuxjournal.com/users/chase-crum" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Chase Crum&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Over the last few years there have been quite a few articles discussing the uses and advancements of the rdesktop application for Linux users. Rdesktop is an application designed to allow Linux users to access the remote desktop feature of Windows based machines, specifically servers. When my company made the decision to implement a terminal server to ease the network effects of a critical application, I began investigating other practical applications for it's use in a mixed Windows/Linux environment. I set out to determine if a Linux based thin client would be practical.  As it turns out, not only was it practical, it was also exceedingly easy.&lt;/p&gt;
&lt;p&gt;	Recently, my department began paying closer attention to our inter-office bandwidth usage. We began to notice that one of our network applications required its end users to conduct a “push” several times a day, taking up most of our available bandwidth. After implementing QOS style bandwidth throttling, we took the additional step of placing this particular application solely on a terminal server. Our users would log in remotely, do their work, and the data transfer took place locally on that server, with no need for a “push” over the network. Encouraged by the results of this test, I began to explore the possibility of doing this with all of the applications commonly found on our end-users' computers.  The result was a completely free (in all senses) thin client.&lt;/p&gt;
&lt;p&gt;	I took a stock computer from the warehouse that was for most purposes, outdated. Since I was planning to install a thin client version of Linux on the machine, it didn't need much more than a keyboard, mouse, monitor and network access.  We rely heavily on CentOS for our server distribution, but I decided to use Ubuntu for my proof of concept thin client. I chose Ubuntu because of its out of box functionality across a number of different components and drivers. I am certain you could use just about any distribution you are comfortable with. Very little is needed to configure a thin client during the default install. The only deviation on this install was that it would login the user automatically without prompting for a password (since nothing would be stored or accessed on this machine, there were few security concerns).&lt;/p&gt;
&lt;p&gt;	Once the machine was installed, a short script was placed in the home directory containing the following:&lt;/p&gt;
&lt;pre&gt;
!/bin/bash
rdesktop 192.168.???.??? -f    #insert terminal address
sudo /sbin/halt
&lt;/pre&gt;&lt;p&gt;
The script needs to be made executable, which can be chmod'd from the command line, or clicked through Gnome. After that's done, let Gnome's startup program manager know where it is and you're almost done (System → preferences → startup applications).  Currently, there is still a bug of sorts in rdesktop that will only connect once before generating errors. To fix this, enter the following at the command line:&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/using-ubuntu-thin-client" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 06 May 2010 16:29:30 +0000</pubDate>
    <dc:creator>Chase Crum</dc:creator>
    <guid isPermaLink="false">1011743 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
