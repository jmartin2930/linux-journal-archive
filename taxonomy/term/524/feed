<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/talking-point">
  <channel>
    <title>talking point</title>
    <link>https://www.linuxjournal.com/tag/talking-point</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Talking Point: Could Linux Abandon Directories In Favour Of Tagging?</title>
  <link>https://www.linuxjournal.com/content/talking-point-could-linux-abandon-directories-favour-tagging</link>
  <description>  &lt;div data-history-node-id="1014969" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/talloncini%28tagging%29.jpg" width="400" height="303" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;For a fairly scruffy looking guy, I have a surprisingly healthy approach to organising my files. However, I'm constantly pushing up against the limitations of a system that is based around directories. I'm convinced that Linux needs to make greater use of tagging, but I'm also beginning to wonder if desktop Linux could abandon the hierarchical directory structure entirely.&lt;/p&gt;
&lt;p&gt;Why is it that web based technology such online bookmarking makes far greater use of tagging than the Linux desktop does? Directories for files are based on the way that humans have always organised items in the real world, using categories and sub categories. Thanks to powerful computers and cheap, plentiful storage, tagging now offers a method of storage that isn't based on placing files in one place or another.&lt;/p&gt;
&lt;p&gt;The word processor file that makes up this article is stored /documents/articles/linux_journal/ but it could be even more efficiently organised if I could easily tag it as “documents”, “articles”, “linux journal” as well as “op ed”, “daft ideas”, “tagging”, “linux” and “web posts”. That way I could find it by browsing through alll of the web posts I've made this year or all of the op-ed peices I've ever written.&lt;/p&gt;
&lt;p&gt;Some organisational situations illustrate the weakness of the hierarchical approach. For example, if I download some independent electronic dance music, where do I place it within a hierarchical system file system? Does it go in /mp3/dance/electronica/independent or /mp3/independent/electronica/dance? Which system works best depends on whether the significant factor is that it is electronica or independently produced. This is where tagging comes into its own as it allows objects to be placed in more than one category at once.&lt;/p&gt;
&lt;p&gt;When dealing with files, there's a distinction to be made between the files that I normally care about and those that I only care about when I'm fiddling around inside Linux's innards. The default setup of most Linux distributions acknowledges this distinction as the files are stored either:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;outside of the /home directory (files that I don't care about most of the time)&lt;/li&gt;
&lt;li&gt;inside the /home directory but hidden (more files that I don't care about most of the time)&lt;/li&gt;
&lt;li&gt;inside the /home directory and visible (these are the files that I care about)&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;It's this last category of files that is ripe for being moved over to a tagged system. Abandoning the directory system outside of the /home folder would mean not only designing a new operating system but also designing a new set of applications.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/talking-point-could-linux-abandon-directories-favour-tagging" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 03 Nov 2010 14:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1014969 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
