<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/xnec2c">
  <channel>
    <title>xnec2c</title>
    <link>https://www.linuxjournal.com/tag/xnec2c</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Antennas in Linux</title>
  <link>https://www.linuxjournal.com/content/antennas-linux</link>
  <description>  &lt;div data-history-node-id="1340469" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12714f3.png" width="829" height="657" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/joey-bernard" lang="" about="https://www.linuxjournal.com/users/joey-bernard" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Joey Bernard&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
For this article, I want to introduce a piece of software I've actually
used recently in my own work. My new day job involves studying the
ionosphere using an instrument called an ionosonde. This device is
basically a giant radio transmitter that bounces radio waves off
the ionosphere to see its structure and composition. Obviously, an
important part of this is knowing the radiation pattern of the various
transmitters and receivers.
&lt;/p&gt;

&lt;p&gt;
Several methods exist for modeling the
electromagnetic fields around conductors, but here I'm covering
one called NEC2 (Numerical Electromagnetics Code). It originally
was developed in FORTRAN at the Lawrence Livermore National Laboratory in the
1970s. Since then, it's been re-implemented several times in various
languages. Specifically, let's look at xnec2c. This package implements
NEC2 in C, and it also provides a GTK front end for interacting with the
core engine.
&lt;/p&gt;

&lt;p&gt;
xnec2c should be available in most Linux distributions.
In Debian-based distributions, you can install it with the
command:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
sudo apt-get install xnec2c
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
Once it's installed, you can start it with &lt;code&gt;xnec2c&lt;/code&gt;. The
default display doesn't show anything until you actually start using it.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/12714f1.png" width="650" height="515" alt="""" class="image-max_650x650" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. Launching xnec2c gives you a pretty boring starting
point.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
xnec2c's history still affects how it behaves to the present day. This
is most clear when you look at the input file's format. The basic
structure is based on the idea of a punch card, where each
"command" to xnec2c is given by a command card—a definite
holdover from its FORTRAN roots. Luckily, the GTK front end to xnec2c
provides a reasonably functional way of building up these input
files.
&lt;/p&gt;

&lt;p&gt;
Several example files should be available with your
installation of xnec2c. In my Ubuntu distribution, they're
located in /usr/share/doc/xnec2c/examples. These input files have a
filename ending of ".nec". Select one as a starting off
point to play with xnec2c, and then go ahead and make the required
alterations necessary for your own project.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/12714f2.png" width="650" height="515" alt="""" class="image-max_650x650" /&gt;&lt;p&gt;
&lt;em&gt;Figure 2. Loading an input file, you begin with a geometric view
of the relevant antenna wires, other conductors and any ground
planes.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
The central window pane provides a geometric view of the actual antenna
structure in three dimensions. You can click and drag the diagram to
rotate the view and see it from all angles. There are two larger buttons
at the top of the window, named Currents and Charges. Selecting
them alternately will show either the distribution of currents or the
distribution of charges caused by the driving current.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/antennas-linux" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 14 Mar 2019 12:00:00 +0000</pubDate>
    <dc:creator>Joey Bernard</dc:creator>
    <guid isPermaLink="false">1340469 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
