<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/dns">
  <channel>
    <title>DNS</title>
    <link>https://www.linuxjournal.com/tag/dns</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Knot DNS: One Tame and Sane Authoritative DNS Server</title>
  <link>https://www.linuxjournal.com/content/knot-dns-one-tame-and-sane-authoritative-dns-server</link>
  <description>  &lt;div data-history-node-id="1340657" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/knot-logo.jpg" width="900" height="450" alt="knot dns logo" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/thomas-golden" lang="" about="https://www.linuxjournal.com/users/thomas-golden" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Thomas Golden&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;How to install and minimally configure Knot
to act as your home lab's local domain master and slave servers.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
If you were a regular viewer of the original &lt;em&gt;Saturday Night Live&lt;/em&gt; era, you
will remember the Festrunks, two lewd but naïve Czech brothers who were
self-described "wild and crazy guys!" For me, Gyorg and Yortuk
(plus having my binomial handed to me by tests designed by a brilliant Czech
professor at the local university's high-school mathematics contests) were
the extent of my knowledge of the Czech Republic.
&lt;/p&gt;

&lt;p&gt;
I recently discovered something else Czech, and it's not wild and crazy
at all, but quite tame and sane, open-source and easy to configure. &lt;a href="https://www.knot-dns.cz"&gt;Knot DNS&lt;/a&gt;
is an authoritative DNS server written in 2011 by
the Czech CZ.NIC organization. They wrote and continue to maintain it to
serve their national top-level domain (TLD) as well as to prevent further
extension of a worldwide BIND9 software monoculture across all TLDs.
Knot provides a separate fast caching server and resolver library alongside
its authoritative server.
&lt;/p&gt;

&lt;p&gt;
Authoritative nameserver and caching/recursive nameserver functions are
separated for good reason. A nameserver's query result cache can be
"poisoned" by queries that forward to malicious external servers, so
if you don't allow the authoritative nameserver to answer queries for
other domains, it cannot be poisoned and its answers for its own domain can
be trusted.
&lt;/p&gt;

&lt;p&gt;
A software monoculture means running identical software like BIND9 everywhere
rather than different software providing identical functionality and
interoperability. This is bad for the same reasons we eventually will lose
our current popular species of banana—being genetically identical, all
bananas everywhere can be wiped out by a single infectious agent. As with
fruit, a bit of genetic diversity in critical infrastructure is a good thing.
&lt;/p&gt;

&lt;p&gt;
In this article, I describe how to install and minimally configure Knot
to act as your home lab's local domain master and slave servers. I will
secure zone transfer using Transaction Signatures (TSIG). Although Knot
supports DNSSEC, I don't discuss it here, because I like you and want you
to finish reading before we both die of old age. I assume you already know
what a DNS zone file is and what it looks like.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/knot-dns-one-tame-and-sane-authoritative-dns-server" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 24 May 2019 12:30:00 +0000</pubDate>
    <dc:creator>Thomas Golden</dc:creator>
    <guid isPermaLink="false">1340657 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>VCs Are Investing Big into a New Cryptocurrency: Introducing Handshake</title>
  <link>https://www.linuxjournal.com/content/vcs-are-investing-big-new-cryptocurrency-introducing-handshake</link>
  <description>  &lt;div data-history-node-id="1340056" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/handshake.png" width="800" height="401" alt="handshake logo" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/petros-koutoupis" lang="" about="https://www.linuxjournal.com/users/petros-koutoupis" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Petros Koutoupis&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
The entire landscape of how we authenticate domain names likely will see a
complete overhaul, all powered by blockchain technologies. Just released,
Handshake brings with it the much needed security and reliability on which
we rely. Backed by venture capitalists and industry-established blockchain
developers, Handshake has raised $10.2 million to replace the current
digital entities maintaining our current internet infrastructure.
&lt;/p&gt;

&lt;p&gt;
The project and protocol has been led by Joseph Poon (creator of
Bitcoin's Lightning Network), Andrew Lee (CEO of Purse), Andrew Lee (founder of
Private Internet Access or PIA) and Christopher Jeffrey (CTO of Purse). The
effort also is backed by 67 individuals with funding coming from A16z,
Founders Fund, Sequoia Capital, Greylock Partners, Polychain Capital and
Draper Associates.
&lt;/p&gt;
&lt;p&gt;
The Handshake project pledges to donate its initial
funding of $10.2 million to FOSS projects, university research
departments and more. The list of recipients includes projects and foundations
such as the Apache Software Foundation, FreeBSD, Reproducible Builds, GNOME,
FSF, SFC, Outreachy, ArchLinux, systemd and many more.
&lt;/p&gt;

&lt;h3&gt;
What Is Handshake?&lt;/h3&gt;

&lt;p&gt;
Handshake aims to be a wholly democratic and decentralized certificate
authority and naming system. Handshake does not replace the Domain
Name System (DNS). It is, however, an alternative to today's certificate
authorities—that is, it uses a decentralized trust anchor to prove domain
ownership. Although the primary goal of the project is to simplify and
secure top-level domain registration while also &lt;em&gt;making the root
zone uncensorable, permissionless and free of gatekeepers&lt;/em&gt;.
&lt;/p&gt;

&lt;p&gt;
A traditional root DNS supports the current infrastructure of the internet
and, therefore, facilitates online access. The root servers hosting the
internet publish root zone file contents, which are responsible for the
internet's DNS functionality. DNS associates information with domain
names and maps them to public-facing IP addresses.
&lt;/p&gt;

&lt;p&gt;
The way Handshake differs from this is that it's all peer to peer. Every
peer is responsible for validating and managing the root zone (via the use
of "light clients"). All existing entries in the root zone file
will form the genesis block of the blockchain supporting it. The same
root zone will be distributed across the nodes forming the chain. The
implementation allows for any participant to help host this distributed
root zone and add to it.
&lt;/p&gt;

&lt;h3&gt;
How Does It Work?&lt;/h3&gt;

&lt;p&gt;
Handshake makes use of a coin system for name registration (that is, the
Handshake coin or HNS). It is the mechanism by which participants are able
to transfer, register and update internet domain names. Currently,
Handshake has opened a faucet to distribute HNS coins to qualified FOSS
contributors. If you are one such contributor and you meet the
project's criteria, you can sign up &lt;a href="https://handshake.org/signup"&gt;here&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/vcs-are-investing-big-new-cryptocurrency-introducing-handshake" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 02 Aug 2018 21:14:15 +0000</pubDate>
    <dc:creator>Petros Koutoupis</dc:creator>
    <guid isPermaLink="false">1340056 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Best of Hack and /</title>
  <link>https://www.linuxjournal.com/content/best-hack-and</link>
  <description>  &lt;div data-history-node-id="1339356" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/head_400x400.jpg" width="400" height="400" alt="Kyle Rankin" title="Kyle Rankin" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/user/800005" lang="" about="https://www.linuxjournal.com/user/800005" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;LJ Staff&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;Secure Server Deployments in Hostile Territory; Preseeding Full Disk Encryption; Own Your Own DNS; Learn How-to Secure Desktops with Qubes; What's New In 3D Printing
&lt;p&gt;
&lt;/p&gt;
&lt;a href="http://www.linuxjournal.com/content/secure-server-deployments-hostile-territory"&gt;Secure Server Deployments in Hostile Territory&lt;/a&gt;
&lt;br /&gt;
Would you change what you said on the phone, if you knew someone malicious was listening? Whether or not you view the NSA as malicious, I imagine that after reading the &lt;a href="http://www.linuxjournal.com/content/nsa-linux-journal-extremist-forum-and-its-readers-get-flagged-extra-surveillance"&gt;NSA coverage on &lt;cite&gt;Linux Journal&lt;/cite&gt;&lt;/a&gt;, some of you found yourselves modifying your behavior. The same thing happened to me when I started deploying servers into a public cloud (EC2 in my case). 
&lt;p&gt;
&lt;/p&gt;
In this article, I discuss some of the techniques I use to secure servers when they are in hostile territory. Although some of these techniques are specific to EC2, most are adaptable to just about any environment.
&lt;ul&gt;&lt;li&gt;&lt;a href="http://www.linuxjournal.com/content/secure-server-deployments-hostile-territory"&gt;Part I&lt;/a&gt;
&lt;/li&gt;&lt;li&gt;&lt;a href="http://www.linuxjournal.com/content/secure-server-deployments-hostile-territory-part-ii"&gt;Part II&lt;/a&gt; 
&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;
&lt;/p&gt;
&lt;a href="https://www.linuxjournal.com/content/preseeding-full-disk-encryption"&gt;Preseeding Full Disk Encryption&lt;/a&gt;
&lt;br /&gt;
Usually I try to write articles that are not aimed at a particular distribution. Although I may give examples assuming a Debian-based distribution, whenever possible, I try to make my instructions applicable to everyone. This is not going to be one of those articles. Here, I document a process I went through recently with Debian preseeding (a method of automating a Debian install, like kickstart on Red Hat-based systems) that I found much more difficult than it needed to be, mostly because documentation was so sparse. In fact, I really found only two solid examples to work from in my research, one of which referred to the other.
&lt;p&gt;
&lt;/p&gt;
&lt;a href="http://www.linuxjournal.com/content/own-your-dns-data"&gt;Own Your Own DNS&lt;/a&gt;
&lt;br /&gt;
I honestly think most people simply are unaware of how much personal data they leak on a daily basis as they use their computers. Even if they have some inkling along those lines, I still imagine many think of the data they leak only in terms of individual facts, such as their name or where they ate lunch. What many people don't realize is how revealing all of those individual, innocent facts are when they are combined, filtered and analyzed.
&lt;p&gt;
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/best-hack-and" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 05 Apr 2017 11:40:59 +0000</pubDate>
    <dc:creator>LJ Staff</dc:creator>
    <guid isPermaLink="false">1339356 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Dynamic DNS—an Object Lesson in Problem Solving</title>
  <link>https://www.linuxjournal.com/content/dynamic-dns%E2%80%94-object-lesson-problem-solving</link>
  <description>  &lt;div data-history-node-id="1085151" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11451f1.png" width="267" height="165" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
The other day in the &lt;em&gt;Linux Journal&lt;/em&gt; IRC room (#linuxjournal on Freenode),
I was whining to the channel about no-ip.com deleting my account
without warning. My home IP address hadn't changed in a couple months,
and because there was no update, it appeared abandoned. The problem was,
although the IP address hadn't changed, I was using the Dynamic DNS domain
name to connect to my house. When the account was deleted, the domain
name wouldn't resolve, and I couldn't connect to my house anymore.
&lt;/p&gt;

&lt;p&gt;
The folks in the IRC channel were very helpful in recommending alternate
Dynamic DNS hosting services, and although there are still a few free
options out there, I was frustrated by relying on someone else to manage
my DNS services. So, like any nerd, I tried to figure out a way to host
a Dynamic DNS service on my own. I thought it would be a simple &lt;code&gt;apt-get
install&lt;/code&gt; on my colocated server, but it turns out there's not a simple
Dynamic DNS server package out there—at least, not one I could find. So,
again like any particularly nerdy nerd, I decided to roll my own. It's
not elegant, it's not pretty, and it's really just a bunch of cheap hacks.
But, it's a great example of solving a problem using existing tools,
so in this article, I explain the process.
&lt;/p&gt;

&lt;p&gt;
First, it's important to point out a few things. The purpose of this
article is not really to explain the best way to make a self-hosted
Dynamic DNS system. In fact, there probably are a dozen better ways to do
the same thing. That's sort of the point. The more familiar you are with
Linux tools, the more resourceful you can be when it comes to problem
solving. I go through the steps I took, and hopefully most readers
can take my method and improve on it several times over. That type of
collaboration is what makes the Open Source community so great! Let's
get started.
&lt;/p&gt;

&lt;h3&gt;
My Particular Toolbox&lt;/h3&gt;

&lt;p&gt;
We all have a slightly different bag of tricks. I'm in the "extremely
lucky" category, thanks to Kyle Rankin tipping me off about the free
Raspberry Pi colocation service. A full-blown Linux box with a static
IP on the Internet is truly the sonic screwdriver when it comes to these
sorts of things, but perhaps someone else's solution won't require a
complete server.
&lt;/p&gt;

&lt;p&gt;
Along with the Raspberry Pi server, I have a Linux server at home, a home
router and a handful of domains I own. I also have accounts on several
Web hosting servers, and accounts with cloud-based storage like Dropbox,
Google Drive and a handful of others.
&lt;/p&gt;

&lt;h3&gt;
First Problem—What's My IP?&lt;/h3&gt;

&lt;p&gt;
The beauty of Dynamic DNS hosting is that regardless of what your home IP
address is, the same domain name will resolve even if it changes. Granted,
my home IP address hadn't changed for months, but I still used the DNS
name to access it. Therefore, when my account at no-ip.com was deleted,
I had no way to tell what my home IP was.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/dynamic-dns%E2%80%94-object-lesson-problem-solving" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 21 May 2013 18:47:39 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1085151 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
