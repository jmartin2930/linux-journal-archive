<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/ai">
  <channel>
    <title>AI</title>
    <link>https://www.linuxjournal.com/tag/ai</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>An AI Wizard of Words</title>
  <link>https://www.linuxjournal.com/content/ai-wizard-words</link>
  <description>  &lt;div data-history-node-id="1340681" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Artificial-Intelligence-1745972.jpg" width="900" height="675" alt="AI" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/marcel-gagn%C3%A9" lang="" about="https://www.linuxjournal.com/users/marcel-gagn%C3%A9" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Marcel Gagné&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;A look at using OpenAI's Generative Pretrained Transformer 2 (GPT-2) to generate text.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
It's probably fair to say that there's more than one person out there who is
worried about some version of artificial intelligence, or AI, possibly in a
robot body of some kind, taking people's jobs. Anything that is repetitive or
easily described is considered fair game for a robot, so driving a car or
working in a factory is fair game.
&lt;/p&gt;

&lt;p&gt;
Until recently, we could tell ourselves that people like yours truly—the
writers and those who create things using some form of creativity—were more
or less immune to the march of the machines. Then came GPT-2, which stands for
Generative Pretrained Transformer 2. I think you'll agree, that isn't the
sexiest name imaginable for a civilization-ending text bot. And since it's
version 2, I imagine that like &lt;em&gt;Star Trek&lt;/em&gt;'s M-5 computer, perhaps GPT-1 wasn't
entirely successful. That would be the original series episode titled, "The
Ultimate Computer", if you want to check it out.
&lt;/p&gt;

&lt;p&gt;
So what does the name "GPT-2" stand for? Well, "generative" means
pretty much what it sounds like. The program generates text based on a
predictive model, much like your phone suggests the next word as you type.
The "pretrained" part is also quite obvious in that the model released by
OpenAI has been built and fine-tuned for a specific purpose. The last word,
"Transformer", refers to the "transformer architecture", which is a neural network
design architecture suited for understanding language. If you want to dig
deeper into that last one, I've included a link from a Google AI blog that
compares it to other machine learning architecture (see Resources).
&lt;/p&gt;

&lt;p&gt;
On February 14, 2019, Valentine's Day, OpenAI released GPT-2 with a
warning:
&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;
Our model, called GPT-2 (a successor to GPT), was trained simply to predict
the next word in 40GB of Internet text. Due to our concerns about malicious
applications of the technology, we are not releasing the trained model. As an
experiment in responsible disclosure, we are instead releasing a much smaller
model for researchers to experiment with, as well as a technical paper.
&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;
I've included a link to the blog in the Resources section at the end of this
article. It's worth
reading partly because it demonstrates a sample of what this software is
capable of using the full model (see Figure 1 for a sample). We already have
a problem with human-generated fake news; imagine a tireless machine capable
of churning out vast quantities of news and posting it all over the internet, and you start to get a feel for the dangers. For that reason, OpenAI released
a much smaller model to demonstrate its capabilities and to engage
researchers and developers.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/ai-wizard-words" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 15 Jul 2019 11:00:00 +0000</pubDate>
    <dc:creator>Marcel Gagné</dc:creator>
    <guid isPermaLink="false">1340681 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>How Can We Bring FOSS to the Virtual World?</title>
  <link>https://www.linuxjournal.com/content/how-can-we-bring-foss-virtual-world</link>
  <description>  &lt;div data-history-node-id="1340251" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/december-fte-smaller.jpeg" width="800" height="533" alt="Liam Broza" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Is there room for FOSS in the AI, VR, AR, MR, ML and XR revolutions—or vice versa?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;Will the free and open-source revolution end when our most personal computing happens inside the walled gardens of proprietary AI VR, AR, MR, ML and XR companies? I ask, because that's the plan.&lt;/p&gt;

&lt;p&gt;I could see that plan when I met the &lt;a href="https://www.magicleap.com/magic-leap-one"&gt;Magic Leap One&lt;/a&gt; at &lt;a href="http://iiworkshop.org"&gt;IIW&lt;/a&gt; in October (only a few days ago as I write this). The ML1 (my abbreviation) gave me an MR (mixed reality) experience when I wore all of this:&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;Lightwear (a headset).&lt;/li&gt;
	&lt;li&gt;Control (a handset).&lt;/li&gt;
	&lt;li&gt;Lightpack (electronics in a smooth disc about the size of a saucer).&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;So far, all Magic Leap offers is a Creator Edition. That was the one I met. Its price is $2,295, revealed only at the end of a registration gauntlet that requires name, email address, birth date and agreement with two click-wrap contracts totaling more than 7,000 words apiece. Here's what the page with the price says you get:&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;Magic Leap One Creator Edition is a lightweight, wearable computer that seamlessly blends the digital and physical worlds, allowing digital content to coexist with real world objects and the people around you. It sees what you see and uses its understanding of surroundings and context to create unbelievably believable experiences.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;Also recommended on the same page are a shoulder strap ($30), a USB (or USB-like) dongle ($60) and a "fit kit" ($40), bringing the full price to $2,425.&lt;/p&gt;

&lt;p&gt;Buying all this is the cost of entry for chefs working in the kitchen, serving apps and experiences to customers paying to play inside Magic Leap's walled garden: a market Magic Leaps hopes will be massive, given an investment sum that now totals close to $2 billion.&lt;/p&gt;

&lt;p&gt;The experience it created for me, thanks to the work of one early developer, was with a school of digital fish swimming virtually in my physical world. Think of a hologram without a screen. I could walk through them, reach out and make them scatter, and otherwise interact with them. It was a nice demo, but far from anything I might crave.&lt;/p&gt;

&lt;p&gt;But I wondered, given Magic Leap's secretive and far-advanced tech, if it could eventually &lt;em&gt;make&lt;/em&gt; me crave things. I ask because &lt;em&gt;immersive&lt;/em&gt; doesn't cover what this tech does. A better adjective might be &lt;em&gt;invasive&lt;/em&gt;.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/how-can-we-bring-foss-virtual-world" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 10 Dec 2018 12:30:00 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1340251 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>What Does "Ethical" AI Mean for Open Source?</title>
  <link>https://www.linuxjournal.com/content/what-does-ethical-ai-mean-open-source</link>
  <description>  &lt;div data-history-node-id="1340006" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12496c_0.png" width="800" height="400" alt="I'm Sorry, Dave. I'm afraid I can't do that." typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/glyn-moody" lang="" about="https://www.linuxjournal.com/users/glyn-moody" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Glyn Moody&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Artificial intelligence is a threat—and an opportunity—for open
source.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
It would be an understatement to say that artificial intelligence (AI) is
much in the news these days. It's widely viewed as likely to usher in the
next big step-change in computing, but a recent interesting
development in the field has particular implications for open source.
It concerns the rise of "ethical" AI.
&lt;/p&gt;

&lt;p&gt;
In October 2016, the White House Office of Science and Technology Policy, the
European Parliament's Committee on Legal Affairs and, in the UK, the House
of Commons' Science and Technology Committee, all released reports on &lt;a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2906249"&gt;how to
prepare for the future of AI&lt;/a&gt;, with ethical issues being an important
component
of those reports. At the beginning of last year, the &lt;a href="https://futureoflife.org/ai-principles/?cn-reloaded=1"&gt;Asilomar AI
Principles&lt;/a&gt; were published, followed by the &lt;a href="https://www.montrealdeclaration-responsibleai.com/the-declaration"&gt;Montreal
Declaration for a Responsible Development of Artificial Intelligence&lt;/a&gt;,
announced in November 2017.
&lt;/p&gt;

&lt;p&gt;
Abstract discussions of what ethical AI might or should mean became very real
in March 2018. It was revealed then that Google had won a share of
the contract for the &lt;a href="https://www.govexec.com/media/gbc/docs/pdfs_edit/establishment_of_the_awcft_project_maven.pdf"&gt;Pentagon's
Project Maven&lt;/a&gt;, which uses artificial intelligence to interpret huge
quantities of video images collected by aerial drones in order &lt;a href="https://gizmodo.com/google-is-helping-the-pentagon-build-ai-for-drones-1823464533"&gt;to
improve the targeting of subsequent drone strikes&lt;/a&gt;. When this became
known, it caused a firestorm at Google. &lt;a href="https://www.nytimes.com/2018/04/04/technology/google-letter-ceo-pentagon-project.html"&gt;Thousands
of people there signed an internal petition&lt;/a&gt; addressed to the company's
CEO, Sundar Pichai, &lt;a href="https://static01.nyt.com/files/2018/technology/googleletter.pdf"&gt;asking
him to cancel the project&lt;/a&gt;. Hundreds of researchers and academics sent an
&lt;a href="https://www.icrac.net/open-letter-in-support-of-google-employees-and-tech-workers"&gt;open
letter supporting them&lt;/a&gt;, and some &lt;a href="https://gizmodo.com/google-employees-resign-in-protest-against-pentagon-con-1825729300"&gt;Google
employees resigned in protest&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/what-does-ethical-ai-mean-open-source" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 20 Aug 2018 11:30:00 +0000</pubDate>
    <dc:creator>Glyn Moody</dc:creator>
    <guid isPermaLink="false">1340006 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Why the Failure to Conquer the Desktop Was Great for GNU/Linux</title>
  <link>https://www.linuxjournal.com/content/why-failure-conquer-desktop-was-great-gnulinux</link>
  <description>  &lt;div data-history-node-id="1339910" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--193625041.jpg" width="800" height="533" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/glyn-moody" lang="" about="https://www.linuxjournal.com/users/glyn-moody" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Glyn Moody&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;AI: open source's next big win.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
Canonical recently launched Ubuntu 18.04 LTS. It's an
important release. In part, that's because Canonical will
support it for five years, making it one of the relatively rare &lt;a href="https://wiki.ubuntu.com/LTS"&gt;LTS&lt;/a&gt; products in Ubuntu's history.
Ubuntu 18.04 also marks a high-profile return to GNOME as the default
desktop, after a few years of controversial experimentation with Unity.
The result is regarded by many as the best desktop Ubuntu so far (that's my
view too, for what it's worth). And yet, the emphasis at launch lay elsewhere. Mark
Shuttleworth, CEO of Canonical and founder of Ubuntu, said:
&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;
Multi-cloud operations are the new normal. Boot-time and
performance-optimised images of Ubuntu 18.04 LTS on every major public
cloud make it the fastest and most efficient OS for cloud computing,
especially for storage and compute-intensive tasks like machine
learning.
&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;
The bulk of &lt;a href="https://insights.ubuntu.com/2018/04/26/ubuntu-18-04-lts-optimised-for-security-multi-cloud-containers-ai"&gt;the
official 18.04 LTS announcement&lt;/a&gt; is about Ubuntu's &lt;a href="https://www.linuxjournal.com/content/everything-you-need-know-about-cloud-and-cloud-computing-part-i"&gt;cloud
computing&lt;/a&gt; features. On the main web site, Ubuntu claims
to be "&lt;a href="https://www.ubuntu.com/cloud"&gt;The standard
OS for cloud computing&lt;/a&gt;", citing (slightly old) research
that shows "70% of public cloud workloads and 54% of OpenStack
clouds" use it. Since Canonical is a privately held company,
it doesn't publish a detailed breakdown of its operations, just &lt;a href="https://beta.companieshouse.gov.uk/company/06870835/filing-history"&gt;a
basic summary&lt;/a&gt;. That means it's hard to tell just how successful
the cloud computing strategy is proving. But, the fact that &lt;a href="https://www.youtube.com/watch?time_continue=235&amp;v=y4lF_fYxvIk"&gt;Shuttleworth
is now openly talking about an IPO&lt;/a&gt;—not something to be undertaken
lightly—suggests that there is enough good news to convince
investors to throw plenty of money at Canonical when the prospectus
spells out how the business is doing.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/why-failure-conquer-desktop-was-great-gnulinux" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 06 Aug 2018 11:30:00 +0000</pubDate>
    <dc:creator>Glyn Moody</dc:creator>
    <guid isPermaLink="false">1339910 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Engineers vs. Re-engineering</title>
  <link>https://www.linuxjournal.com/content/engineers-vs-re-engineering</link>
  <description>  &lt;div data-history-node-id="1340013" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/your-car-is-a-leash.jpg" width="762" height="549" alt="Sign: Your Car is a Leash and You Are an AI's Pet" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;In an age when people are being re-engineered into farm animals for AI
ranchers, it's the job of engineers to save humanity through true personal
agency.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
A few months ago, I was driving through Los Angeles when the &lt;a href="https://www.waze.com"&gt;Waze&lt;/a&gt; app on my
phone told me to take the Stadium Way exit off the 110 freeway. About five
other cars peeled off with me, and we became a caravan, snaking through side
streets and back onto the freeway a few miles later. I knew Waze had to be in
charge of us, since Waze is the navigation app of choice in Los Angeles, and
it was beyond coincidence that all these cars took the same wild maze run
through streets only locals knew well.
&lt;/p&gt;

&lt;p&gt;
What was Waze up to here, besides offering its users (or a subset of them) a
way around a jam? Was it optimizing traffic by taking some cars off the
highway and leaving others on? Running an experiment only some AI understood?
There was no way to tell. I doubt anyone at Waze could say exactly what was
going on either. Algorithms are like that. So are the large and constantly
changing data sets informing algorithms most of us with mobile devices depend
on every day.
&lt;/p&gt;

&lt;p&gt;
In &lt;em&gt;&lt;a href="https://www.amazon.com/Re-Engineering-Humanity-Brett-Frischmann/dp/1107147093/"&gt;Re-engineering Humanity&lt;/a&gt;&lt;/em&gt;, Brett Frischmann and Evan Selinger have dug
deeply into what's going on behind the "cheap bliss" in our fully connected
world.
&lt;/p&gt;

&lt;p&gt;
What they say is that we are all subjects of &lt;em&gt;techno-social
engineering&lt;/em&gt;. In
other words, our algorithmic conveniences are re-making us, much as the
technologies and techniques of agriculture re-makes farm animals. And, as
with farming, there's an extraction business behind a lot of it.
&lt;/p&gt;

&lt;p&gt;
They say "humanity's techno-social dilemma" is that "companies, institutions,
and designers regularly treat us as &lt;em&gt;programmable objects&lt;/em&gt; through personalized
technologies that are attuned to our personal histories, present behavior and
feelings, and predicted futures."
&lt;/p&gt;

&lt;p&gt;
And we are not innocent of complicity in this. "We outsource memory,
decision-making and even our interpersonal relations...we rely on the
techno-social engineers' tools to train ourselves, and in doing so, let
ourselves be trained."
&lt;/p&gt;

&lt;p&gt;
There are obvious benefits to "delegating physical, cognitive, emotional and
ethical labor to a third party", such as Waze, but there are downsides, which
Brett and Evan number: 1) passivity, 2) decreased agency, 3) decreased
responsibility, 4) increased ignorance, 5) detachment and 6) decreased
independence. On the road to these diminished human states, we have
"fetishised computers and idealized computation".
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/engineers-vs-re-engineering" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 02 Aug 2018 12:07:07 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1340013 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
