<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/vms">
  <channel>
    <title>VMs</title>
    <link>https://www.linuxjournal.com/tag/vms</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>VirtualBox 4.1 Introduces Cloning Amongst Other Features</title>
  <link>https://www.linuxjournal.com/content/virtualbox-41-introduces-cloning-amongst-other-features</link>
  <description>  &lt;div data-history-node-id="1022995" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/virtualbox41_1_rescale.png" width="200" height="139" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;We first &lt;a href="../../../../../content/virtualbox-40"&gt;covered&lt;/a&gt; the release of &lt;a href="http://www.virtualbox.org/"&gt;VirtualBox&lt;/a&gt; 4.0 back in January. Amongst other improvements and new features, 4.1 adds VM cloning and a nascent PCI passthrough feature.&lt;/p&gt;
&lt;p&gt;The  new cloning feature allows the user to easily make a copy of an  existing VM including the hard disk image and all associated settings.  It’s going to be particularly handy for people like me who create a lot  of blank VMs with similar specs. This means that I can create a generic  Linux 2.6 supporting VM with 1GM of RAM and a virtual HD of 8GB and then  simply clone it when I need a new one. The speed of the cloning  procedure varies according to the size of the HD file. For example,  cloning a fresh VM with an unused dynamically scaling hard disk image  takes almost no time, whereas cloning my Windows XP VM with a 4GB  partition took a few minutes.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/u1013687/virtualbox41_clone_scaled.png" alt="" width="500" height="316" /&gt;&lt;/p&gt;
&lt;p&gt;Virtual disk management has received a couple of useful enhancements. It is now possible to directly create and work with &lt;a href="http://en.wikipedia.org/wiki/VMDK"&gt;VMDK&lt;/a&gt; (VMware) and &lt;a href="http://en.wikipedia.org/wiki/VHD_%28file_format%29"&gt;VHD&lt;/a&gt;  (Microsoft Virtual PC) formats from the GUI. There is also a GUI  interface for copying hard disk images. As with the cloning features,  the new hard disk management features were already more-or-less possible  from the command line and with a bit of extra trouble, but having them  built into the GUI will be more convenient for those of us who prefer to  work that way.&lt;/p&gt;
&lt;p&gt; PCI Passthrough, a  new Linux only feature looks intriguing, but  according to the docs, it seems to be at early stage with limited  hardware support. The feature allows guest operating systems to directly  access PCI and PCI Express hardware, even in cases in which the  hardware is not supported by the host. This could offer benefits in two  main areas: Firstly, it offers the potential to greatly improve the  performance of hardware such as network adaptors when accessed from  within a VM. Secondly, it would allow Linux to use hardware (albeit via a  VM) that either has no, or only limited, Linux driver support. For the  moment, only specific combinations of motherboard chipset and add on  card are supported. Something tells me that this is a feature worth  watching out for in the future. See the &lt;a href="http://www.virtualbox.org/manual/ch09.html#pcipassthrough"&gt;appropriate section&lt;/a&gt; of the manual for more info.&lt;/p&gt;
&lt;p&gt;Note  that version 4.1 increases the memory limit to 1TB for 64 bit guests.  So, it looks like developers are already thinking about support for  KDE5. A new GUI for setting the CPU cap makes it possible to limit the  amount of processor time that a VM can gobble.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/virtualbox-41-introduces-cloning-amongst-other-features" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 28 Jul 2011 14:52:57 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1022995 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Xen Enters Mainline Kernel</title>
  <link>https://www.linuxjournal.com/content/xen-enters-mainline-kernel</link>
  <description>  &lt;div data-history-node-id="1021710" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/xen_crop.jpg" width="600" height="480" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Future versions of the Linux Kernel (such as 3.0) will include support for the &lt;a href="http://en.wikipedia.org/wiki/Xen"&gt;Xen&lt;/a&gt; hypervisor. This means that Linux distributions will typically offer out of the box support for both hosting Xen and running as a guest operating system under Xen.&lt;/p&gt;
&lt;p&gt;Xen requires operating system support from both the host and the guest. In other words, you need an operating system that has been modified in order to run Xen or to run under Xen. &lt;strong&gt;[Correction: Xen can run unmodified operating systems on a processor that supports &lt;a href="http://en.wikipedia.org/wiki/X86_virtualization#AMD_virtualization_.28AMD-V.29"&gt;x86 virtualisation&lt;/a&gt;, which to be fair, should include most modern desktop processors.] &lt;/strong&gt;In the past, installing Xen (in most distributions) has been a more complicated procedure than for other virtualizers such as VirtualBox. &lt;/p&gt;
&lt;p&gt;As it stands, the Linux Kernel offers support for &lt;a href="http://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine"&gt;KVM&lt;/a&gt;, a virtualization technology that can speed up the &lt;a href="http://en.wikipedia.org/wiki/Qemu"&gt;QEMU&lt;/a&gt; machine emulator. It is the hope of the Xen community that out of the box support for Xen will increase adoption. However, the degree to which built-in kernel support will raise the profile of Xen is debatable. The target of Xen has always been server admins who placed a higher premium on top-flight security and server specific features than they did on ease of use. Anyone who needs the features that distinguish Xen from other solutions would probably not have been dissuaded from using it by the difficult installation. Casual users, who need a simple installation via the package manager on distributions such as Ubuntu, would probably be better served by &lt;a href="http://en.wikipedia.org/wiki/Virtualbox"&gt;VirtualBox&lt;/a&gt;, &lt;a href="http://en.wikipedia.org/wiki/Qemu"&gt;QEMU&lt;/a&gt; or &lt;a href="http://en.wikipedia.org/wiki/Vmware"&gt;VMWare&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Who knows, perhaps someone will cook up a virtualization solution aimed at casual desktop users, but that uses Xen as its underlying technology? There might even be some scope for adding other features, such as application sandboxing, by making use of Xen.&lt;/p&gt;
&lt;p&gt;The &lt;a href="http://blogs.oracle.com/wim/entry/linux_mainline_contains_all_the"&gt;announcement&lt;/a&gt; on the Oracle website.&lt;/p&gt;
&lt;p&gt;&lt;em&gt;The image that I used in this article, a picture of a zen garden, was taken from the Flicker account of &lt;/em&gt;&lt;a href="http://www.flickr.com/photos/filicudi/"&gt;&lt;em&gt;CyboRoZ&lt;/em&gt;&lt;/a&gt;&lt;em&gt;. I was able to use it here because it of the Creative Commons &lt;/em&gt;&lt;a href="http://creativecommons.org/licenses/by-nd/2.0/deed.en_GB"&gt;&lt;em&gt;license&lt;/em&gt;&lt;/a&gt;&lt;em&gt; that he released the work under.&lt;/em&gt;&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/xen-enters-mainline-kernel" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 20 Jun 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1021710 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Consolidate: Put Your Servers into a VirtualBox VM</title>
  <link>https://www.linuxjournal.com/content/consolidate-put-your-servers-virtualbox-vm</link>
  <description>  &lt;div data-history-node-id="1014337" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/virtualbox_ose.png" width="640" height="457" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;Rather than installing a server, such as a web server, directly onto your main computer, why not install it in a VM? This sort of setup has a few advantages of security and convenience. These days, spreading resources out into the cloud is the in-thing, but consolidation is often underexploited. Hosting a server in a virtualizer such as VirtualBox is often a good approach for casual or occasional server needs on a home network.
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/consolidate-put-your-servers-virtualbox-vm" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 30 Sep 2010 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1014337 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
