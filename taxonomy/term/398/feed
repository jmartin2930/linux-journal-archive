<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/qvfb">
  <channel>
    <title>qvfb</title>
    <link>https://www.linuxjournal.com/tag/qvfb</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>The Qt Virtual Framebuffer</title>
  <link>https://www.linuxjournal.com/content/qt-virtual-framebuffer</link>
  <description>  &lt;div data-history-node-id="1011766" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/qvfb-4-137x300.png" width="137" height="300" alt="QVFb in action" title="The Qt Virtual Framebuffer running wiggly" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/johan-thelin" lang="" about="https://www.linuxjournal.com/users/johan-thelin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Johan Thelin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Qt comes with a handy tool that lives a rather anonymous life in the tools/qvfb directory. This tool does wonders when it comes to demoing, training users and creating documentation.&lt;/p&gt;
&lt;p&gt;The QVFb, or &lt;a rel="nofollow"&gt;Qt Virtual Framebuffer&lt;/a&gt;, is a tool that makes it possible to run Qt applications on a virtual system with a limited screen resolution, limited bit-depth, and with limited input abilities. All of this on your desktop (or laptop) PC, saving you the hazzles of cross-platform debugging, cross-compiling, transfering files to the target, etc.&lt;/p&gt;
&lt;p&gt;The trick with the QVFb is not really how to use it, but rather, how to build it. It is interesting enough that I've actually given a number of speeches on it. The problem is not that it is difficult to build, rather, that you have to build Qt two times to get a working environment.&lt;/p&gt;
&lt;p&gt;When building Qt, the result of the configure script is a binary: qmake. This binary processes *.pro files and builds target specific Makefiles. The qmake tool carries your configuration, so if you are targetting two systems with your Qt project, you will have to run "make distclean" followed by "qmake-&lt;i&gt;target&lt;/i&gt;" to aim your project at a specific target (where "qmake-&lt;i&gt;target&lt;/i&gt;" can be "qmake-x11", "qmake-osx", "qmake-arm-fb", etc).&lt;/p&gt;
&lt;p&gt;With this knowledge, and some careful thought when configuring Qt (do not forget to use the &lt;tt&gt;-prefix&lt;/tt&gt; option to avoid having three Qt installs in one directory), building QVFb is quite easy.&lt;/p&gt;
&lt;p&gt;I usually build three instances of Qt when targeting an embedded system - all with roughly the same configuration. This is to make sure that what I test on my PC also works on my target. The Qt builds are:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;Qt for host/X11 - used to build the Qt tools for the host machine.&lt;/li&gt;
&lt;li&gt;Qt for target/fb - used to build the actual target files.&lt;/li&gt;
&lt;li&gt;Qt for host/qvfb - used to build binaries for QVFb.&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Qt for host/X11 is simple, just build Qt as you usually do. However, when you have built it, cd into tools/qvfb and do "make &amp;&amp; make install" for QVFb as well.&lt;/p&gt;
&lt;p&gt;Qt for target/fb can be a completely different story. It's &lt;i&gt;usually&lt;/i&gt; dead simple to build Qt for your target. However, as we all know, the devil lives in the details. This build can be tuned in so many ways that this is one of the things I do for a living.&lt;/p&gt;
&lt;p&gt;Qt for host/qvfb is basically Qt for target/fb, but for the host machine. I also make sure to include graphics and input drivers for QVFb. This means adding &lt;tt&gt;-qt-gfx-qvfb&lt;/tt&gt;, &lt;tt&gt;-qt-kbd-qvfb&lt;/tt&gt; and &lt;tt&gt;-qt-mouse-qvfb&lt;/tt&gt; to the configure command line.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/qt-virtual-framebuffer" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 12 May 2010 20:18:43 +0000</pubDate>
    <dc:creator>Johan Thelin</dc:creator>
    <guid isPermaLink="false">1011766 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
