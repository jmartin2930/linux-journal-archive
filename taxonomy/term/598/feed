<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/openbox">
  <channel>
    <title>OpenBox</title>
    <link>https://www.linuxjournal.com/tag/openbox</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>CrunchBang 10 “Statler” refresh R20111125</title>
  <link>https://www.linuxjournal.com/content/crunchbang-10-%E2%80%9Cstatler%E2%80%9D-refresh-r20111125</link>
  <description>  &lt;div data-history-node-id="1027235" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/CrunchBangNov2011_resize.png" width="200" height="150" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;Although officially a version 10 refresh and still under the “Statler” moniker, the latest Crunchbang release constitutes some notable changes.&lt;/p&gt;
&lt;p&gt;I first covered &lt;a href="http://crunchbanglinux.org/"&gt;CrunchBang&lt;/a&gt; back &lt;a href="http://www.linuxjournal.com/content/spotlight-linux-crunchbang"&gt;in March&lt;/a&gt;. In brief, it's a lightweight desktop OS that uses Debian Stable 6.0 as its base. The biggest change in the latest refresh is that the developer has jettisoned the Xfce version in order to become a pure Openbox distro. I'm a fan of Xfce, but I welcome the decision of developer Philip Newborough aka corenominal. The truth is that there are other Xfce based distros to choose from such as my personal current favorite, Xubuntu. One of the biggest challenges for a smaller Linux distro is to carve a useful niche for itself, and if this helps him to hone the Openbox experience, all the better.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/CrunchBangNov2011_640.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;As before, when you first run a new installation of CrunchBang you are prompted to answer a series of questions about additional components such as the development tools and LibreOffice. By default, as ever, the theme is a dark one, but you can easily change this if it's not to your taste. I'm not going get into a drawn-out argument in the comments (&lt;a href="http://www.linuxjournal.com/content/spotlight-linux-crunchbang#comments"&gt;see last time&lt;/a&gt;), but the window decorations have been changed, and I for one prefer the new design. Overall, the user interface feels fast and minimalist without feeling compromised. However, it isn't a clone of, say, Gnome, and I would advise anyone to have a play around with the Live CD before committing to an installation.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/crunchbangNov2011_trans_crop.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;It's simple to enable compositing courtesy of either Xcompmgr or Cairo Composite Manager for fancy screen effects.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;Other changes include:&lt;/p&gt;
&lt;p&gt;• Firefox rather than Chrome&lt;br /&gt;• LibreOffice rather than OpenOffice.org&lt;br /&gt;• &lt;a href="http://slim.berlios.de/"&gt;SLiM&lt;/a&gt; rather than GDM as the login manager.&lt;br /&gt;• Programmer friendly &lt;a href="http://www.geany.org/"&gt;Geany&lt;/a&gt; rather than Gedit&lt;br /&gt;• &lt;a href="http://www.uvena.de/gigolo/index.html"&gt;Gigolo&lt;/a&gt; and &lt;a href="http://thunar.xfce.org/"&gt;Thunar&lt;/a&gt; are set up and ready to go and this means that network resources are can be easily browsed from the GUI.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/crunchbang-10-%E2%80%9Cstatler%E2%80%9D-refresh-r20111125" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 30 Nov 2011 16:22:56 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1027235 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Spotlight On Linux: CrunchBang</title>
  <link>https://www.linuxjournal.com/content/spotlight-linux-crunchbang</link>
  <description>  &lt;div data-history-node-id="1017776" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/crunchbang_desktop.png" width="640" height="480" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://crunchbanglinux.org/"&gt;CrunchBang&lt;/a&gt; is a lightweight Linux distribution based on Debian. It comes in &lt;a href="http://openbox.org/"&gt;OpenBox&lt;/a&gt; and XFCE editions, and a very dark visual theme. It's the OpenBox version that I took a look at.&lt;/p&gt;
&lt;p&gt;Being based on Debian is a point is its favor as it means that standard trouble shooting and standard packages work on the system. The documentation on the website assures that CrunchBang is, essentially, a standard Debian installation with a few additional custom packages.&lt;/p&gt;
&lt;p&gt;Installation takes a familiar path. It's a usable system when booted from the CD image, and hard disk installation is invoked by running a program from the desktop.&lt;/p&gt;
&lt;p&gt;So far, so much the same. So, in what ways does CrunchBang differ from other distros?&lt;/p&gt;
&lt;p&gt;When CrunchBang is booted for the first time, the user is presented with an interactive post installation script comprising of 15 questions. These prompts allow the user to further refine and specialize the installation. They allow for the optional installation of various applications such as Open Office, suites of tools such as build tools and conveniences such as popular media codecs and the Sun Java runtime. One option is to install a specialized kernel which is apparently optimized for desktop use. If selected rather than skipped, each installation option takes between a few seconds and several minutes to complete.&lt;/p&gt;
&lt;p&gt;The desktop itself has a dark theme consisting of black, white and gray but this can, of course, be changed. On the backdrop there is a system information panel showing free memory and CPU usage amongst other data. Below this area is a reminder of keyboard shortcuts. For example, Windows key + W launches the web browser.&lt;/p&gt;
&lt;p&gt;I'd not used OpenBox before, and I can confirm that is has a minimalist appearance along and feels responsive and fast. As standard, the window controls use a series of Braille-like dotted icons. These look cool, but it's impossible to guess the function of each at first glance. The task switcher sits on the bottom of the screen and features a standard status area with a clock. The task switcher is icon-based and also includes a virtual desktop switcher in quite a nice arrangement, although it doesn't automatically stay on top windows, which will perturb some. The application launch menu and system menu is the sort that is invoked by right clicking on on either the backdrop or the task switcher.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/u1013687/crunchbang_dots.png" width="444" height="149" alt="" /&gt;&lt;em&gt;Any guesses as to what these dots mean?&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/u1013687/crunchbang_taskbar_crop.png" width="418" height="104" alt="" /&gt;&lt;em&gt;The taskbar is also a pager. A nice arrangement.&lt;/em&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/spotlight-linux-crunchbang" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 16 Mar 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1017776 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
