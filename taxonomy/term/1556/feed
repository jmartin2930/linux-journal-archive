<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/c">
  <channel>
    <title>C</title>
    <link>https://www.linuxjournal.com/tag/c</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Considering Fresh C Extensions</title>
  <link>https://www.linuxjournal.com/content/considering-fresh-c-extensions</link>
  <description>  &lt;div data-history-node-id="1340448" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Laptop-Programming-Code-On-Sc-280677649.jpg" width="737" height="600" alt="programming " typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/zack-brown" lang="" about="https://www.linuxjournal.com/users/zack-brown" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Zack Brown&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
&lt;strong&gt;Matthew Wilcox&lt;/strong&gt; recently realized there might be a value in
depending on &lt;strong&gt;C&lt;/strong&gt;
extensions provided by the &lt;strong&gt;Plan 9&lt;/strong&gt; variant of the C programming language.
All it would require is using the &lt;code&gt;-fplan9-extensions&lt;/code&gt; command-line
argument when compiling the kernel. As Matthew pointed out, Plan 9
extensions have been supported in &lt;strong&gt;GCC&lt;/strong&gt; as of version 4.6, which is the
minimum version supported by the kernel. So theoretically, there would be no
conflict.
&lt;/p&gt;

&lt;p&gt;
&lt;strong&gt;Nick Desaulniers&lt;/strong&gt; felt that any addition of &lt;code&gt;-f&lt;/code&gt; compiler flags to any
project always would need careful consideration. Depending on what the
extensions are needed for, they could be either helpful or downright
dangerous.
&lt;/p&gt;

&lt;p&gt;
In the current case, Matthew wanted to use the Plan 9 extensions to shave
precious bytes off of a cyclic memory allocation that needed to store a
reference to the "next" value. Using the extensions, Matthew said, he could
embed the "next" value without breaking various existing function calls.
&lt;/p&gt;

&lt;p&gt;
Nick also suggested making any such extension dependencies optional, so
that other compilers would continue to be able to compile the kernel.
&lt;/p&gt;

&lt;p&gt;
It looked as though there would be some back and forth on the right way to
proceed, but &lt;strong&gt;Linus Torvalds&lt;/strong&gt; immediately jumped in to veto the entire
concept, saying:
&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;
Please don't.
&lt;/p&gt;

&lt;p&gt;
The subset of the plan9 extensions that are called just "ms" extensions is
fine. That's a reasonable thing, and is a very natural expansion of the
unnamed structures we already have—namely being able to pre-declare that
unnamed structure/union.
&lt;/p&gt;

&lt;p&gt;
But the full plan9 extensions are nasty, and makes it much too easy to
write "convenient" code that is really hard to read as an outsider because
of how the types are silently converted.
&lt;/p&gt;

&lt;p&gt;
And I think what you want is explicitly that silent conversion.
&lt;/p&gt;

&lt;p&gt;
So no. Don't do it. Use a macro or an inline function that makes the
conversion explicit so that it's shown when grepping.
&lt;/p&gt;

&lt;p&gt;
The "one extra argument" is not a strong argument for something that simply
isn't that common. The upsides of a nonstandard feature like that needs to
be pretty compelling.
&lt;/p&gt;

&lt;p&gt;
We've used various gcc extensions since day #1 ("inline" being perhaps the
biggest one that took _forever_ to become standard C), but these things
need to have very strong arguments.
&lt;/p&gt;

&lt;p&gt;
"One extra argument" that could be hidden by a macro or a helper inline is
simply not a strong argument.
&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;
Nick was sympathetic to this point, and said:
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/considering-fresh-c-extensions" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 12 Mar 2019 13:03:28 +0000</pubDate>
    <dc:creator>Zack Brown</dc:creator>
    <guid isPermaLink="false">1340448 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Simulate Typing with This C Program</title>
  <link>https://www.linuxjournal.com/content/simulate-typing-c-program</link>
  <description>  &lt;div data-history-node-id="1340222" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Education-concept-Head-With-F-61652528_2.jpg" width="800" height="600" alt="Tech Tips" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/jim-hall" lang="" about="https://www.linuxjournal.com/users/jim-hall" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Jim Hall&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
I recently created a video demonstration of how to do some work
at the command line, but as I tried to record my video, I kept running
into problems. I'm just not the kind of person who can type commands
at a keyboard and talk about it at the same time. I quickly realized
I needed a way to simulate typing, so I could create a
"canned" demonstration that I could narrate in my video.&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
After doing some searching, I couldn't find a command on my distribution that
would simulate typing. I wasn't surprised; that's not a common thing
people need to do. So instead, I rolled my own program to do it.
&lt;/p&gt;

&lt;p&gt;
Writing a program to simulate typing isn't as difficult as it first
might seem. I needed my program to act like the &lt;code&gt;echo&lt;/code&gt; command, where it displayed
output given as command-line parameters. I added command-line options so
I could set a delay between the program "typing" each letter, with an
additional delay for spaces and newlines. The program basically did this
the following
for each character in a given string:
&lt;/p&gt;

&lt;ol&gt;&lt;li&gt;
Insert a delay.&lt;/li&gt;

&lt;li&gt;
Print the character.&lt;/li&gt;

&lt;li&gt;
Flush the output buffer so it shows up on screen.&lt;/li&gt;
&lt;/ol&gt;&lt;p&gt;
First, I needed a way to simulate a delay in typing, such as someone
typing slowly, or pausing before typing the next word or pressing
Enter. The C function to create a delay is &lt;code&gt;usleep(useconds_t
usec)&lt;/code&gt;. You use
&lt;code&gt;usleep()&lt;/code&gt; with the number of microseconds you want your program to
pause. So if you want to wait one second, you would use
&lt;code&gt;usleep(1000000)&lt;/code&gt;.
&lt;/p&gt;

&lt;p&gt;
Working in microseconds means too many zeroes for me to type, so I wrote a
simple wrapper called &lt;code&gt;msleep(int millisec)&lt;/code&gt; that does the same thing
in milliseconds:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
int
msleep (int millisec)
{
  useconds_t usec;
  int ret;


  /* wrapper to usleep() but values in milliseconds instead */


  usec = (useconds_t) millisec *1000;
  ret = usleep (usec);
  return (ret);
}
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
Next, I needed to push characters to the screen after each
delay. Normally, you can use &lt;code&gt;putchar(int char)&lt;/code&gt; to send a single character
to standard output (such as the screen), but you won't actually see the
output until you send a newline. To get around this, you need to flush the
output buffer manually. The C function &lt;code&gt;fflush(FILE *stream)&lt;/code&gt; will flush an
output stream for you. If you put a &lt;code&gt;delay()&lt;/code&gt; before each
&lt;code&gt;fflush()&lt;/code&gt;, it will
appear that someone is pausing slightly between typing each character.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/simulate-typing-c-program" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 24 Oct 2018 12:00:00 +0000</pubDate>
    <dc:creator>Jim Hall</dc:creator>
    <guid isPermaLink="false">1340222 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
