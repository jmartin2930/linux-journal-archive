<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/user-interfaces">
  <channel>
    <title>user interfaces</title>
    <link>https://www.linuxjournal.com/tag/user-interfaces</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Ubuntu Tweak 0.6 Enters beta</title>
  <link>https://www.linuxjournal.com/content/ubuntu-tweak-06-enters-beta</link>
  <description>  &lt;div data-history-node-id="1025243" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/UbuntuTweak_settings_crop2.png" width="284" height="209" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;There are a wealth of changes which can be made to an Ubuntu system that sit in a category that is half way between the obscure and the genuinely useful. &lt;a href="http://ubuntu-tweak.com/"&gt;Ubuntu Tweak&lt;/a&gt; is utility that exposes these options with a handy GUI.&lt;/p&gt;
&lt;p&gt;At time of writing the Ubuntu Tweak 0.6 beta can be installed by using the PPA on the website (see this page).&lt;/p&gt;
&lt;p&gt;Ubuntu Tweak may be runnable on other Ubuntu powered desktops or even other distros, but it’s clear that it is aimed squarely at users of stock Ubuntu who like to fiddle. There look to be about 100 options which are divided into three main categories which are then divided into further subcategories:&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Startup&lt;/strong&gt; (Login Settings, Session Control)&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Desktop&lt;/strong&gt; (Compiz Settings, Desktop Icon Settings, GNOME Settings, Window Manager Settings)&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;System&lt;/strong&gt; (Nautilus Settings, Power Manager Settings, Security Related, Workarounds)&lt;/p&gt;
&lt;p&gt;Version 0.6 has switched to an icon pallet interface rather than the list based interface of previous versions. The options themselves could all be classed as useful but not essential. For those of you who haven’t used Ubuntu Tweak before, I’ll pick out some features to give you a flavor of what’s available.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/UbuntuTweak_settings_crop.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;It’s possible to toggle the startup sound, alter the background image of the GDM login screen, remove the restart icon from the Gnome menu and suppress the restart confirmation dialog. The window manager tweaks move into KDE territory as you can tweak things like the actions of different mouse buttons on window title bars and other areas of the user interface. Under the System section, there are options to control whether the system should become locked when power management blanks the screen and you can force Nautilus to always use the location entry bar rather than the path bar.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/UbuntuTweak_settings_janitor_600.png" alt="" /&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Some some of the janitor tools. It's amazing how often purging a cache can resolve a problem. As ever with Ubuntu Tweak, one begins to wonder if these features should be made standard in Ubuntu.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;There are many other options, and despite the level of detail offered, the actual navigation between them is perfectly simple thanks to the arrangement of sections and subsections.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/ubuntu-tweak-06-enters-beta" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 29 Sep 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1025243 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>GIMP 2.7.3 adds Single Window Mode</title>
  <link>https://www.linuxjournal.com/content/gimp-273-adds-single-window-mode</link>
  <description>  &lt;div data-history-node-id="1024090" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/thegimp273_ljr_600b.jpg" width="550" height="413" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://www.gimp.org/"&gt;GIMP&lt;/a&gt; 2.7.3 has added one of the most requested features in the program’s history: a single window mode. Version 2.7 is part of the development branch, so unfortunately, the feature won't hit most distro repositories for a while.&lt;/p&gt;
&lt;p&gt;As I said, because 2.7 is a development branch, most distributions won’t add it to their repositories, and this means that you’ll have to build it yourself. It’s a shame, in a way, because the new window mode might be viewed as a ease of use feature that less advanced users would appreciate. According to &lt;a href="http://tasktaste.com/projects/Enselic/gimp-2-8"&gt;this&lt;/a&gt; page, it looks like version 2.8 should arrive in about November.&lt;/p&gt;
&lt;p&gt;If you want to have a sneak peek at the new development features, you’ll probably have to compile from scratch. As it’s a development version, it depends on some up to date libraries which you may have to add by hand. Fortunately, if you’re an Ubuntu user, you can use &lt;a href="http://www.gimpusers.com/tutorials/compiling-gimp-for-ubuntu"&gt;this&lt;/a&gt; excellent guide to find all the bits that you’ll need.&lt;/p&gt;
&lt;p&gt;The one window mode is activated by selecting the option in the Windows menu. So, people who prefer the old way of working still can. It handles multiple open documents with an iconified tab bar that runs along the top of the screen.&lt;/p&gt;
&lt;p&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/thegimp273_ljr_600b.jpg" alt="" /&gt;&lt;br /&gt;&lt;em&gt;The single window interface. The two questions are: What will long term users make of it? Will new users find it more appealing?&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;Personally, I’m sure that this is how I’ll be using GIMP from now on. I’ve used the program a lot of over the years, despite an interface that I always considered to be rather awkward. As for gauging how a new user would take to the program, it’s difficult to be sure. For a start, it should look more inviting than the old interface and reminiscent of other programs with a similar purpose.&lt;/p&gt;
&lt;p&gt;Let me know if you spot any promising looking custom repos, PPAs or packages for 2.7 series, and I add them to the post.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;[User bartman2589 has spotted a PPA that he has successfully installed. See comments.]&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;The gimpusers.com &lt;a href="http://www.gimpusers.com/tutorials/compiling-gimp-for-ubuntu"&gt;guide&lt;/a&gt; to compiling the development branch on Ubuntu. It’s not that difficult and the guide should work as an aid for people working with other distibutions.&lt;/p&gt;
&lt;p&gt;A &lt;a href="http://www.gimp.org/release-notes/gimp-2.7.html"&gt;summary&lt;/a&gt; of all of the changes to 2.7 series so far.&lt;/p&gt;
&lt;p&gt;The &lt;a href="http://www.gimp.org/downloads/"&gt;download&lt;/a&gt; page with direct links to the 2.7.3 tarball.&lt;/p&gt;
&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/gimp-273-adds-single-window-mode" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 30 Aug 2011 13:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1024090 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
