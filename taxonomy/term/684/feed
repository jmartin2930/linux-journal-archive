<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/passwordless">
  <channel>
    <title>passwordless</title>
    <link>https://www.linuxjournal.com/tag/passwordless</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Safer Access without Passwords</title>
  <link>https://www.linuxjournal.com/content/safer-access-without-passwords</link>
  <description>  &lt;div data-history-node-id="1022174" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/command-line_1.png" width="150" height="150" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/johan-thelin" lang="" about="https://www.linuxjournal.com/users/johan-thelin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Johan Thelin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;How do you make sure that your passwords are safe? You can make them longer, complicate them by adding odd characters, making sure to use different passwords for each user account that you have. Or, you can simply skip them all together.&lt;/p&gt;
&lt;p&gt;The secure shell, ssh, is a key tool in any Linux user's toolbox. As soon as you have more than one machine to interact with, ssh is the obvious choice.&lt;/p&gt;
&lt;p&gt;When logging into a remote machine through ssh, you are usually prompted with the remote user's password. An alternative to this is to use an asymmetric key pair.&lt;/p&gt;
&lt;p&gt;An asymmetric key pair consists of a private and public key. These are generated from an algorithm - either RSA or DSA. RSA has been around for a long time and is widely supported, even by old legacy implementations. DSA is safer, but requires v2 of the ssh protocol. This is not much of an issue in an open source world - keeping the ssh daemon implementation up to date is not a problem, but rather a requirement. Thus, DSA is the recommended choice, unless you have any specific reason to pick RSA.&lt;/p&gt;
&lt;p&gt;The generated keys are larger than a common user password. RSA keys are at least 768 bits, default 2048 bits. DSA keys are 1024, as the standard specifies this.&lt;/p&gt;
&lt;p&gt;To generate a DSA key, use the following command:&lt;/p&gt;
&lt;pre&gt;$ ssh-keygen -t dsa&lt;/pre&gt;&lt;p&gt;This generates the files ~/.ssh/id_dsa and ~/.ssh/id_dsa.pub. You can specify a passphrase in the key generation process. This means that they key only can be used in combination with a passphrase, adding to the security.&lt;/p&gt;
&lt;p&gt;The generated file ending with pub is the public half of the pair. This can be shared with remote hosts that you want to ssh to. The other file, id_dsa, is the private half of the pair. This must be protected just as you password. I.e. do not mail this, do not store it on untrusted machines, etc.&lt;/p&gt;
&lt;p&gt;Having a 1024 bits long key can be thought of as having a 128 characters long password. This means that the key pair method is safer than most passwords that you can remember. Keys are also completely random, so they cannot be cracked using dictionary attacks. This means that you can increase the safety of your remote host by disabling logins using passwords, thus forcing all users to use key pairs.&lt;/p&gt;
&lt;p&gt;Having generated your key pair, all that is left is copying the public half of the key to the remote machine. You do that using the ssh-copy-id command.&lt;/p&gt;
&lt;pre&gt;$ ssh-copy-id -i ~/.ssh/id_dsa.pub user@remote&lt;/pre&gt;&lt;p&gt;This adds your key to the remote machine's list of authorized keys. Just to be on the safe side, it is also good to ensure that the ~/.ssh and ~/.ssh/authorized_keys aren't writable by any other user than you. You might have to fix this using chmod.&lt;/p&gt;
&lt;p&gt;Having added the key to the remote machine, you should now be able to ssh to it without using a password.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/safer-access-without-passwords" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 06 Jul 2011 13:00:00 +0000</pubDate>
    <dc:creator>Johan Thelin</dc:creator>
    <guid isPermaLink="false">1022174 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
