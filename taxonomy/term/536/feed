<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/arch-linux">
  <channel>
    <title>Arch Linux</title>
    <link>https://www.linuxjournal.com/tag/arch-linux</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>The Arch Way</title>
  <link>https://www.linuxjournal.com/content/arch-way</link>
  <description>  &lt;div data-history-node-id="1017346" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/archlinux-logo-dark-90dpi.png" width="600" height="199" alt="Arch Linux" title="Arch Linux" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kevin-bush" lang="" about="https://www.linuxjournal.com/users/kevin-bush" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kevin Bush&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;You love tinkering with your computer. You've tried Ubuntu and Fedora, and they're good, but you feel something is not quite right. Maybe you don't like all those daemons loading on boot, or maybe you want to build your Linux desktop stack just how you want it? Perhaps you're completely new to Linux and want to learn exactly what makes a Linux workstation tick? It's time for you to try Arch Linux. Arch Linux is often called the binary Gentoo—an appropriate description. Arch gives you a full but simple command-line base to build on, but unlike Gentoo, Arch uses i686 or x86_64 optimized binary packages instead of source code.&lt;/p&gt;
&lt;p&gt;This will not be a step-by-step guide on getting an Arch Linux desktop up and running. You can find that on Arch's fabulous wiki. Instead I'll share with you what separates Arch from other Linux distributions and what makes Arch one of the best distributions for a personal workstation.&lt;/p&gt;
&lt;h3&gt;Simplicity&lt;/h3&gt;
&lt;p&gt;Arch development is guided by simplicity. The Arch wiki states, "Simplicity is absolutely the principal objective behind Arch development."  Most system configuration is done via text files, which may seem complex at first, but is much easier in practice than searching visually through a menu tree. For example, daemons, kernel modules, and networking are all configured in a single text file, rc.conf. GNU text parsing tools included in every version of Linux make finding and editing configuration files a breeze. It's much faster to search for and edit a text string in Vim than to click through endless GUI tabs looking for a radio button.&lt;/p&gt;
&lt;h3&gt;Rolling Release&lt;/h3&gt;
&lt;p&gt;Arch uses a rolling release model. This means that the repositories are updated frequently with the latest stable packages from upstream developers. The advantage here is that you don't need to reinstall or do a complex upgrade procedure every six months. Do a pacman -Syu (more on the gloriousness that is Pacman below) every few days to keep your entire system updated. This is not to say that an update will never cause problems, but if you encounter problems, in all likelihood there will be a fix posted on the forums or the Arch front page.&lt;/p&gt;
&lt;h3&gt;The Wickedly Good Arch Wiki&lt;/h3&gt;
&lt;p&gt;Speaking of documentation, the Arch Wiki is the most comprehensive repository of Linux information out there. Back when I used Ubuntu, I often found the answer to a tough configuration issue on The Arch Wiki. Yes, the Arch Wiki is tailored to Arch, but it can often help you solve problems with other distributions as well.&lt;/p&gt;
&lt;h3&gt;BSD Style init&lt;/h3&gt;
&lt;p&gt;Say bye-bye to complicated System V runlevels and their associated symlinks. Most distributions use a System V style init, which has a separate directory for each of its eight runlevels. Arch uses a BSD-style init system, which accomplishes much the same with a simple set of scripts. Need to add CUPS to start at boot? Simply add CUPS to the daemons list in /etc/rc.conf. Done.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/arch-way" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 13 Jan 2011 14:00:00 +0000</pubDate>
    <dc:creator>Kevin Bush</dc:creator>
    <guid isPermaLink="false">1017346 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Spotlight on Linux:  Arch Linux 2010.05</title>
  <link>https://www.linuxjournal.com/content/spotlight-linux-arch-linux-201005</link>
  <description>  &lt;div data-history-node-id="1015304" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/arch_07102010_busy_thumb.png" width="320" height="256" alt="Arch Linux" title="Arch Linux" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/susan-linton" lang="" about="https://www.linuxjournal.com/users/susan-linton" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Susan Linton&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;One of the top 10 most popular distributions on Distrowatch.com's page hit ranking is &lt;a href="http://www.archlinux.org/"&gt;Arch Linux&lt;/a&gt;.   It attracts a lot of users because of its ability to give the user a  feeling of ownership without an excessive amount of time and effort.  It  began life in 2002 and has been increasing in popularity since.   Besides the great operating system, the project offers a moderate sized  community, a friendly and active &lt;a href="https://bbs.archlinux.org/"&gt;user forum&lt;/a&gt;, and lots of easy-to-follow documentation.&lt;/p&gt; &lt;p&gt;Arch is developed as a rolling release system, which means it's  updated regularly through the package manager rather than being  reinstalled every six months like some other distros.  However,  developers do release a new core and net image every six to nine months for  new users or those wishing a clean start.  It's a live CD, but don't  expect a fancy desktop.  It boots to a commandline interface to allow  users a system from which to work.  But have no fear, the install  procedure isn't really very different from Slackware's.  It is a wizard  that will walk the user through most of the procedure.  However,  prospective  users might want to visit the &lt;a href="http://wiki.archlinux.org/index.php/Official_Arch_Linux_Install_Guide"&gt;Arch Wiki&lt;/a&gt; and make a few notes before starting.&lt;/p&gt;
&lt;div class="right"&gt;
 
&lt;a href="http://pricechirp.com/items/33027"&gt;PriceChirp.com Widget&lt;/a&gt;
&lt;/div&gt;
&lt;p&gt;The primary advantage of Arch Linux is that one can build their  system to their own taste and needs.  You can make it as light or  full-featured as you want.  You can even build packages from the source  if you so desire.  Another advantage is high performance.  One  disadvantage is that you will need to be familiar with your hardware, and you will  have to set up a few configuration files by hand with a text editor.   Again, refer to the &lt;a href="http://wiki.archlinux.org/index.php/Official_Arch_Linux_Install_Guide"&gt;Official Arch Linux Install Guide&lt;/a&gt; or the unofficial &lt;a href="http://wiki.archlinux.org/index.php/Beginners%27_Guide"&gt;Beginners' Guide&lt;/a&gt; for full details.  Documentation is included on the install CD as well at &lt;code&gt;/usr/share/aif/docs/official_installation_guide_en.&lt;/code&gt;&lt;/p&gt; &lt;p&gt;It's your choice, but any of the known desktops are available for installation as is most any piece of &lt;a href="http://www.archlinux.org/packages/"&gt;software&lt;/a&gt;  you might need.  After the install, Pacman can install additional  software and keep your Arch system updated.  It is similar to APT at the  commandline and can install, uninstall, update, search repositories,  and query installation data.&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/spotlight-linux-arch-linux-201005" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 28 Oct 2010 13:00:00 +0000</pubDate>
    <dc:creator>Susan Linton</dc:creator>
    <guid isPermaLink="false">1015304 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
