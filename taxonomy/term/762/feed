<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/thin-clients">
  <channel>
    <title>thin clients</title>
    <link>https://www.linuxjournal.com/tag/thin-clients</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>The Linux powered LAN Gaming House</title>
  <link>https://www.linuxjournal.com/content/linux-powered-lan-gaming-house</link>
  <description>  &lt;div data-history-node-id="1028544" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/LAN_party_clients_people_600.jpg" width="600" height="450" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;LAN parties offer the enjoyment of head to head gaming in a real-life social environment. In general, they are experiencing decline thanks to the convenience of Internet gaming, but Kenton Varda is a man who takes his LAN gaming very seriously. His LAN gaming house is a &lt;a href="http://kentonsprojects.blogspot.com/2011/12/lan-party-optimized-house.html"&gt;fascinating project&lt;/a&gt;, and best of all, Linux plays a part in making it all work.&lt;/p&gt;
&lt;p&gt;Varda has done his own write ups (&lt;a href="http://kentonsprojects.blogspot.com/2011/12/lan-party-optimized-house.html"&gt;short&lt;/a&gt;, &lt;a href="http://kentonsprojects.blogspot.com/2011/12/lan-party-house-technical-design-and.html"&gt;long&lt;/a&gt;), so I'm only going to give an overview here. The setup is a large house with 12 gaming stations and a single server computer.&lt;/p&gt;
&lt;p&gt;The client computers themselves are rack mounted in a server room, and they are linked to the gaming stations on the floor above via extension cables (HDMI for video and audio and USB for mouse and keyboard). Each client computer, built into a 3U rack mount case, is a well specced gaming rig in its own right, sporting an Intel Core i5 processor, 4GB of RAM and an Nvidia GeForce 560 along with a 60GB SSD drive.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/LAN_party_clients.JPG" /&gt;&lt;/p&gt;
&lt;p&gt;Originally, the client computers ran Ubuntu Linux rather than Windows and the games executed under &lt;a href="http://en.wikipedia.org/wiki/Wine_%28software%29"&gt;WINE&lt;/a&gt;, but Varda had to abandon this scheme. As he explains on his site:&lt;br /&gt;&lt;em&gt;&lt;br /&gt;&lt;/em&gt;&lt;em&gt;"Amazingly, a majority of games worked fine, although many had minor bugs (e.g. flickering mouse cursor, minor rendering artifacts, etc.). Some games, however, did not work, or had bad bugs that made them annoying to play.&lt;/em&gt;"&lt;/p&gt;
&lt;p&gt;Subsequently, the gaming computers have been moved onto a more conventional gaming choice, Windows 7. It's a shame that WINE couldn't be made to work, but I can sympathize as it's rare to find modern games that work perfectly and at full native speed. Another problem with WINE is that it tends to suffer from regressions, which is hardly surprising when considering the difficulty of constantly improving the emulation of the Windows API. Varda points out that he preferred working with Linux clients as they were easier to modify and came with less licensing baggage.&lt;/p&gt;
&lt;p&gt;Linux still runs the server and all of the tools used are open source software. The hardware here is a &lt;a href="http://ark.intel.com/products/52271"&gt;Intel Xeon E3-1230&lt;/a&gt; with 4GB of RAM. The storage hanging off this machine is a bit more complex than the clients. In addition to the 60GB SSD, it also has 2x1TB drives and a 240GB SDD.&lt;/p&gt;
&lt;p&gt;&lt;img alt="" src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1013687/LAN_party_clients_people_600.jpg" /&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/linux-powered-lan-gaming-house" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 08 Feb 2012 18:12:05 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1028544 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
