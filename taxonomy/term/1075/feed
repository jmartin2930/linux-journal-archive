<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/odroid">
  <channel>
    <title>ODROID</title>
    <link>https://www.linuxjournal.com/tag/odroid</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Papa's Got a Brand New NAS: the Software</title>
  <link>https://www.linuxjournal.com/content/papas-got-brand-new-nas-software</link>
  <description>  &lt;div data-history-node-id="1340119" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--177981730.jpg" width="800" height="533" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;Who needs a custom NAS OS or a web-based GUI when command-line
NAS software is so easy to configure?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In a recent letter to the editor, I was contacted by a reader who
enjoyed my &lt;a href="https://www.linuxjournal.com/content/papas-got-brand-new-nas"&gt;"Papa's
Got a Brand New NAS"&lt;/a&gt; article, but wished I had
spent more time describing the software I used. When I
wrote the article, I decided not to dive into the software too much,
because it all was pretty standard for serving files under Linux.
But on second thought, if you want to re-create what I made, I
imagine it would be nice to know the software side as well, so this article
describes the software I use in my home NAS.
&lt;/p&gt;

&lt;h3&gt;
The OS&lt;/h3&gt;

&lt;p&gt;
My NAS uses the &lt;a href="https://www.hardkernel.com/main/products/prdt_info.php"&gt;ODROID-XU4&lt;/a&gt; as the main computing platform, and so
far, I've found its octo-core ARM CPU and the rest of its resources
to be adequate for a home NAS. When I first set it up, I visited the
&lt;a href="https://wiki.odroid.com/odroid-xu4/odroid-xu4"&gt;official wiki
page&lt;/a&gt; for the computer, which provides a number of OS
images, including Ubuntu and Android images that you can copy onto a
microSD card. Those images are geared more toward desktop use,
however, and I wanted a minimal server image. After some searching,
I found a &lt;a href="https://forum.odroid.com/viewtopic.php?f=96&amp;t=17542"&gt;minimal image for what was the current Debian stable
release at the time (Jessie)&lt;/a&gt;.
&lt;/p&gt;


&lt;p&gt;
Although this minimal image worked okay for me, I don't necessarily
recommend just going with whatever OS some volunteer on a forum
creates. Since I first set up the computer, the Armbian project has
been released, and it supports a number of standardized OS images for quite
a few ARM platforms including the ODROID-XU4. So if you
want to follow in my footsteps, you may want to start with the &lt;a href="https://www.armbian.com/odroid-xu4"&gt;minimal Armbian
Debian image&lt;/a&gt;.
&lt;/p&gt;

&lt;p&gt;
If you've ever used a Raspberry Pi before, the process of setting
up an alternative ARM board shouldn't be too different. Use another
computer to write an OS image to a microSD card, boot the ARM board,
and at boot, the image will expand to fill the existing filesystem.
Then reboot and connect to the network, so you can log in with the default
credentials your particular image sets up. Like with Raspbian builds,
the first step you should perform with Armbian or any other OS image
is to change the default password to something else. Even better,
you should consider setting up proper user accounts instead of
relying on the default.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/papas-got-brand-new-nas-software" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 29 Oct 2018 12:00:00 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1340119 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Two Portable DIY Retro Gaming Consoles</title>
  <link>https://www.linuxjournal.com/content/two-portable-diy-retro-gaming-consoles</link>
  <description>  &lt;div data-history-node-id="1340063" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Retro-Games-Vector-Logo-Retro-244605349.jpg" width="600" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
A look at Adafruit's PiGRRL Zero vs. Hardkernel's ODROID-GO.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
If
you enjoy retro gaming, there are so many options, it can
be tough to know what to get. The choices range from officially sanctioned
systems from Nintendo all the way to homemade RetroPie projects like I've
covered in &lt;em&gt;Linux Journal&lt;/em&gt; in the past. Of course, those systems are designed
to be permanently attached to a TV.
&lt;/p&gt;

&lt;p&gt;
But, what if you want to play retro games
on the road? Although it's true that you could just connect a gamepad to a
laptop and use an emulator, there's something to be said for a console
that fits in your pocket like the original Nintendo Game Boy. In this
article, I describe two different portable DIY retro
gaming projects I've built and compare and contrast their features.
&lt;/p&gt;


&lt;h3&gt;
Adafruit PiGRRL Zero&lt;/h3&gt;

&lt;p&gt;
The RetroPie project spawned an incredible number of DIY retro consoles
due to how easy and cheap the project made it to build a console out of the widely
available and popular Raspberry Pi. Although most of the projects were aimed
at home consoles, Adafruit took things a step further and created the
PiGRRL project series that combines Raspberry Pis with LCD screens,
buttons, batteries and other electronics into a portable RetroPie system
that has a similar form factor to the original Game Boy. You buy the kit,
print the case and buttons yourself with a 3D printer, and after some
soldering, you have a portable console.
&lt;/p&gt;

&lt;p&gt;
The original &lt;a href="https://learn.adafruit.com/pigrrl-raspberry-pi-gameboy"&gt;PiGRRL&lt;/a&gt; was based off the Raspberry Pi and was similar
in size and shape to the original Game Boy. In the original kit, you
also took apart an SNES gamepad, cut the electronics and used it for
gamepad electronics. Although you got the benefit of a real SNES gamepad's
button feedback, due to that Game Boy form factor, there were no L and
R shoulder buttons, and only A and B buttons on the front, so it was aimed
at NES and Game Boy games.
&lt;/p&gt;

&lt;p&gt;
The &lt;a href="https://learn.adafruit.com/pigrrl-2"&gt;PiGRRL 2&lt;/a&gt; took the original PiGRRL and offered a number of
upgrades. First, it was based on the faster Raspberry Pi 2, which could
emulate newer systems like the SNES. It also incorporated its own custom
gamepad electronics, so you could get A, B, X and Y buttons in the front,
plus L and R buttons in the back, while still maintaining the similar
Game Boy form factor.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_1300x1300/public/u%5Buid%5D/12516f1.jpg" width="1300" height="866" alt="""" class="image-max_1300x1300" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. PiGRRL 2&lt;/em&gt;
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/two-portable-diy-retro-gaming-consoles" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 03 Sep 2018 12:33:21 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1340063 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Papa's Got a Brand New NAS</title>
  <link>https://www.linuxjournal.com/content/papas-got-brand-new-nas</link>
  <description>  &lt;div data-history-node-id="1339260" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12067f1.jpg" width="450" height="600" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/kyle-rankin" lang="" about="https://www.linuxjournal.com/users/kyle-rankin" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Kyle Rankin&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
It used to be that the true sign you were dealing with a Linux geek
was the pile of computers lying around that person's house. How else could
you experiment with networked servers without a mass of computers
and networking equipment? If you work as a sysadmin for a large
company, sometimes one of the job perks is that you get first dibs on
decommissioned equipment. Through the years, I was able to amass quite a home
network by combining some things I bought myself with some equipment that
was too old for production. A major point of pride in my own home network
was the 24U server cabinet in the garage. It had a gigabit top-of-rack
managed switch, a 2U UPS at the bottom, and in the middle was a 1U HP
DL-series server with a 1U eSATA disk array attached to it. Above that
was a slide-out LCD and keyboard in case I ever needed to work on the
server directly.
&lt;/p&gt;

&lt;p&gt;
The 1U server acted as my primary server for just about everything. It
was the gateway router, local mail relay and secondary MX for my personal
domains, DNS server, DHCP server, and with the eSATA array, it became
our home NAS (Network Attached Storage) array that we used for general file
storage and backups. Everything generally worked well, and if you ignored
the power bill and the space it took up, it was quite the impressive
setup in its day.
&lt;/p&gt;

&lt;p&gt;
The key phrase here is "in its day", because these days, a combination
of virtualization and cloud computing means you are more likely to
see a Linux geek with a laptop than a pile of servers.
As computers have become faster, smaller and cheaper, my 1U server was
starting to show its age. Given that all of my eggs were in this basket,
I started wondering about what I'd do if one of the expensive components
on the server failed. Although the server had been stable up to this point,
I realized it wouldn't last forever, and if it did break, I could
buy modern hardware for the cost of replacing, for instance, one of
its fancy serial-attached SCSI drives. I ended up researching a lot of
different options, and in this article, I describe how I ended
up replacing that 24U cabinet and all the hardware in it with something
that's smaller than a shoe box, much lower-powered and relatively cheap.
&lt;/p&gt;

&lt;h3&gt;
It's an ARM's World&lt;/h3&gt;

&lt;p&gt;
At the beginning of my search, I started down a more traditional route
with a cheap 1U server and a modern motherboard, but I quickly started
narrowing down the motherboards to small, lower-power solutions given
this machine was going to run all day. As I started considering some
of the micro ATX solutions out there, it got me thinking: could I use a
Raspberry Pi? After all, the latest iteration of the Raspberry Pi has
a reasonably fast processor, a decent amount of RAM, and it's cheap,
so even if one by itself wasn't enough to manage all my services, two
or three might do the trick and not only be cheaper than a standard
motherboard but lower power as well.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/papas-got-brand-new-nas" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Thu, 12 Jan 2017 13:21:40 +0000</pubDate>
    <dc:creator>Kyle Rankin</dc:creator>
    <guid isPermaLink="false">1339260 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
