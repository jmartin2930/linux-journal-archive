<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/compiling">
  <channel>
    <title>compiling</title>
    <link>https://www.linuxjournal.com/tag/compiling</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Using Checkinstall To Build Packages From Source</title>
  <link>https://www.linuxjournal.com/content/using-checkinstall-build-packages-source</link>
  <description>  &lt;div data-history-node-id="1013021" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/checkinstall2_crop.png" width="402" height="232" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/michael-reed" lang="" about="https://www.linuxjournal.com/users/michael-reed" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Michael Reed&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;a href="http://www.asic-linux.com.mx/~izto/checkinstall/"&gt;Checkinstall&lt;/a&gt; is a utility that builds a .deb, .rpm or Slackware package from a third party source code tarball. This allows you to introduce such third party software using the standard package management features of your distribution. In contrast, the conventional instructions for installing such software packages puts your package manager out of sync with the actual collection of software installed on your Linux box.&lt;/p&gt;
&lt;p&gt;This short tutorial presumes that you're using a Debian-derived distribution such as Ubuntu, although it should work with most distributions. It also presumes that you have some prior experience of building a package from the source code.&lt;/p&gt;
&lt;p&gt;For most users, the preferred method of adding software to a Linux system is by using a package manager. Package management is very reliable these days, and apart from anything else, it offers an advantage that Linux systems enjoy. However, what do you do when a package that you need isn't in the repository of your chosen distribution, or it is in there but it's an old version? In such cases, there is often nothing else for it but to build from source.&lt;/p&gt;
&lt;p&gt;Building from source is a reasonably simple process, but it brings with it a few problems. For a start, you're circumventing the package manager, and this puts its internal database out of sync with the software installed on your computer. You can even end up with two versions of an application installed simultaneously which can cause all sorts of problems.&lt;/p&gt;
&lt;p&gt;Thankfully, there is a tool called Checkinstall that is designed to sort out this mess. Checkinstall takes a compiled source code tarball and turns it into a Debian, Slackware or RPM package that you can install “officially” via the package management tools. Furthermore, you can distribute the finished package so that other people can install it without having to build from the source code. Best of all, it's very easy to use if you already know how to build packages from the source. Checkinstall is not included by default in some distributions, so you might have to search for it and install it via the package manager.&lt;/p&gt;
&lt;p&gt;The normal process for building a package from source is begun by, having first downloaded and unpacked the source code from the maintainer's website, navigating to the the source code directory and typing:&lt;/p&gt;
&lt;pre&gt;./configure.&lt;/pre&gt;&lt;p&gt;
Once the configuration process has completed, you then type:&lt;/p&gt;
&lt;pre&gt;make&lt;/pre&gt;&lt;p&gt;
which builds the source code, followed by:&lt;/p&gt;
&lt;pre&gt;sudo make install&lt;/pre&gt;&lt;p&gt;
which installs the package. However, the official instructions typically encourage you to circumvent the advantages of a package manager, and this is the part that causes the problem . Instead of invoking “sudo make install” type:&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/using-checkinstall-build-packages-source" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 02 Aug 2010 11:00:00 +0000</pubDate>
    <dc:creator>Michael Reed</dc:creator>
    <guid isPermaLink="false">1013021 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
