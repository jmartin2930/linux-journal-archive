<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/gps">
  <channel>
    <title>GPS</title>
    <link>https://www.linuxjournal.com/tag/gps</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Support for a GNSS and GPS Subsystem</title>
  <link>https://www.linuxjournal.com/content/support-gnss-and-gps-subsystem</link>
  <description>  &lt;div data-history-node-id="1340075" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--207055501.jpg" width="800" height="554" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/zack-brown" lang="" about="https://www.linuxjournal.com/users/zack-brown" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Zack Brown&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
Recently, there was a disagreement over whether a subsystem really addressed
its core purpose or not. That's an unusual debate to have. Generally
developers know if they're writing support for one feature or
another.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In this particular case, &lt;strong&gt;Johan Hovold&lt;/strong&gt; posted patches to add
a &lt;strong&gt;GNSS&lt;/strong&gt; subsystem
(Global Navigation Satellite System), used by &lt;strong&gt;GPS&lt;/strong&gt; devices. His idea was that
commercial GPS devices might use any input/output ports and
protocols—serial, USB and whatnot—forcing user code to perform difficult probes in
order to determine which hardware it was dealing with. Johan's code would
unify the user interface under a /dev/gnss0 file that would hide the various
hardware differences.
&lt;/p&gt;

&lt;p&gt;
But, &lt;strong&gt;Pavel Machek&lt;/strong&gt; didn't like this at all. He said that there wasn't any
actual GNSS-specific code in Johan's GNSS subsystem. There were a number of
GPS devices that wouldn't work with Johan's code. And, Pavel felt that at best
Johan's patch was a general power management system for serial devices. He
felt it should not use names (like "GNSS") that then would be unavailable for
a "real" GNSS subsystem that might be written in the future.
&lt;/p&gt;

&lt;p&gt;
However, in kernel development, "good enough" tends to trump "good but not
implemented". Johan acknowledged that his code didn't support all GPS
devices, but he said that many were proprietary devices using proprietary
interfaces, and those companies could submit their own patches. Also, Johan
had included two GPS drivers in his patch, indicating that even though his
subsystem might not contain GNSS-specific code, it was still useful for its
intended purpose—regularizing the GPS device interface.
&lt;/p&gt;

&lt;p&gt;
The debate went back and forth for a while. Pavel seemed to have the ultimate
truth on his side—that Johan's code was at best misnamed, and at worst,
incomplete and badly structured. Although Johan had real-world usefulness on his
side, where something like his patch had been requested by other developers
for a long time and solved actual problems confronted by people today.
&lt;/p&gt;

&lt;p&gt;
Finally &lt;strong&gt;Greg Kroah-Hartman&lt;/strong&gt; put a stop to all debate—at least for the
moment—by simply accepting the patch and feeding it up to &lt;strong&gt;Linus
Torvalds&lt;/strong&gt;
for inclusion in the main kernel source tree. He essentially said that there
was no competing patch being offered by anyone, so Johan's patch would do
until anything better came along.
&lt;/p&gt;

&lt;p&gt;
Pavel didn't want to give up so quickly, and he tried at least to negotiate a
name change away from "GNSS", so that a "real" GNSS subsystem might still
come along without a conflict. But with his new-found official support, Johan
said, "This is the real gnss subsystem. Get over it."
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/support-gnss-and-gps-subsystem" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 26 Sep 2018 12:00:00 +0000</pubDate>
    <dc:creator>Zack Brown</dc:creator>
    <guid isPermaLink="false">1340075 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Android Candy: Waze Redux</title>
  <link>https://www.linuxjournal.com/content/android-candy-waze-redux</link>
  <description>  &lt;div data-history-node-id="1339214" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12037androidf1.png" width="283" height="235" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
Back in 2014, I highlighted Waze, which is a turn-by-turn GPS navigation
program created by a startup in Israel. That company was bought by Google,
but it still remains independent, at least for now. (It does share some data
behind the scenes, but it functions differently when it comes to routing.)
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1000009/12037androidf1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
(Image from &lt;a href="https://www.waze.com"&gt;https://www.waze.com&lt;/a&gt;)
&lt;/p&gt;


&lt;p&gt;
Although I had very bad luck with Waze early on, I recently used it on a
cross-country trip, and it was amazing. I still have unpleasant memories
of Waze trying to force me to turn off an overpass and having it rout me
to an off-ramp only to get back on the on-ramp immediately. I'm happy to
say, however, those issues seem to be resolved. In fact, it was a very
pleasant experience!
&lt;/p&gt;

&lt;p&gt;
Not only was the navigation reliable (and murder-free), but it has a
unique way of saving time by taking less-traveled routes. Last year when
I was driving through Atlanta, Georgia, I got stuck in traffic for hours
using my Garmin GPS. This year, Waze took me into corn fields in order
to avoid traffic jams in Nashville. I'll admit, I was a bit worried when
GPS advised me to turn on a poorly maintained country road, but in the
end, it saved me hours of monotonous city traffic.
&lt;/p&gt;

&lt;p&gt;
The TL;DR truth is, Waze has gotten to the point where it's now my
favorite GPS app. Plus, if your passenger is bored, it's fun to report
speed traps and hazards on the road. All that input makes for better
driving, which makes family vacations far more enjoyable! Check it out
in the Google Play store today. Waze is still free, and you'd be silly
not to give it a test drive.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/android-candy-waze-redux" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 15 Nov 2016 13:26:53 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1339214 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
