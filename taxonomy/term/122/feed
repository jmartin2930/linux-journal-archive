<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/chromium">
  <channel>
    <title>Chromium</title>
    <link>https://www.linuxjournal.com/tag/chromium</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Switching to Chrom(ium)</title>
  <link>https://www.linuxjournal.com/content/switching-chromium</link>
  <description>  &lt;div data-history-node-id="1084285" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/Chrome_Logo.png" width="200" height="200" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/reuven-lerner" lang="" about="https://www.linuxjournal.com/users/reuven-lerner" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Reuven Lerner&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/small-200px-left-align-wrap/u1002061/Chrome_Logo_0.png" alt="" title="" class="imagecache-small-200px-left-align-wrap" /&gt;&lt;p&gt;
For someone who works with, writes about and teaches cutting-edge
technologies, I tend to be a bit of a laggard when adopting new ones.
I upgrade my laptop and servers very conservatively. I got my first
smartphone just earlier this year. I still use the Apache HTTP
server, even though I know that nginx is a bit faster. And until
recently, Mozilla's Firefox was my default browser.
&lt;/p&gt;

&lt;p&gt;
Firefox is a remarkable piece of software, and it has been a massive
success by any measure. It was around before and during Netscape's
IPO, which marked the start of the IPO-crazy dot-com era. I then
watched as it declined as a company, turning its flagship product
(Firefox) into an open-source project before disappearing.
&lt;/p&gt;

&lt;p&gt;
I used Firefox from its first pre-release versions and have been a
loyal user ever since. This was not only because Firefox generally
adhered to and promoted standards, but also because of the wide
variety of plugins and extensions available for it. As a Web
developer, I found that a combination of plugins—from Firebug to
the aptly named Web developer to Tamper Data—gave me enormous
power and flexibility when developing, debugging and working on Web
applications.
&lt;/p&gt;

&lt;p&gt;
During the past year, I've discovered that a very large number of
non-techies have switched browsers. But, they haven't been switching
to Firefox. Rather, they've been switching to Chrome, a relatively
new browser whose development is sponsored by Google. I've certainly
used Chrome through the years, and I've generally been impressed by its
abilities. But for a long time, some combination of nostalgia and
comfort with Firefox's tools kept me from switching.
&lt;/p&gt;

&lt;p&gt;
Well, no more. As of recently, Google Chrome has become my browser of
choice. In this article, I describe a bit about Chrome and why
I've switched, both for personal use and browsing, and in my Web
development work. In a future article, I'll explain how to write
extensions for Chrome. One of the nice things about Chrome is that
writing extensions is extremely easy and exclusively uses Web
technologies (for example HTML, CSS and JavaScript).
&lt;/p&gt;

&lt;p&gt;
I should make it clear before I continue that Chrome is not an
open-source product. It is free-as-in-beer, but it isn't released under
an open-source license. That said, there are several reasons why
open-source advocates should take a look at Chrome. First, it
is rapidly growing in popularity, with many developers and users alike
adopting it. Just as my clients expect that I'll test Web
applications against IE, they now expect that I'll test applications
against Chrome. If you aren't including Chrome in your testing, 
you might be missing some issues in your site's design or
functionality.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/switching-chromium" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Fri, 08 Feb 2013 15:00:00 +0000</pubDate>
    <dc:creator>Reuven Lerner</dc:creator>
    <guid isPermaLink="false">1084285 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
