<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/cryptocurrency">
  <channel>
    <title>Cryptocurrency</title>
    <link>https://www.linuxjournal.com/tag/cryptocurrency</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>Blockchain, Part II: Configuring a Blockchain Network and Leveraging the Technology</title>
  <link>https://www.linuxjournal.com/content/blockchain-part-ii-configuring-blockchain-network-and-leveraging-technology</link>
  <description>  &lt;div data-history-node-id="1339695" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock-Blockchain-technology-futurist-225602305.jpg" width="500" height="281" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/petros-koutoupis" lang="" about="https://www.linuxjournal.com/users/petros-koutoupis" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Petros Koutoupis&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
How to set up a private ethereum blockchain using open-source tools and a
look at some markets and industries where blockchain technologies can add value.&lt;/em&gt;&lt;/p&gt;


&lt;p&gt;
In Part I, I spent quite a bit of time exploring cryptocurrency
and the mechanism that makes it possible: the blockchain. I covered details
on how the blockchain works and why it is so secure and
powerful. In this second part, I describe how to set up and configure your very own
private ethereum blockchain using open-source tools. I also look
at where this technology can bring some value or help redefine how people
transact across a more open web.
&lt;/p&gt;

&lt;h3&gt;
Setting Up Your Very Own Private Blockchain Network&lt;/h3&gt;

&lt;p&gt;
In this section, I explore the mechanics of an ethereum-based
blockchain network—specifically, how to create a private ethereum
blockchain, a private network to host and share this blockchain,
an account, and then how to do some interesting things with the
blockchain.
&lt;/p&gt;

&lt;p&gt;
What is ethereum, again? Ethereum is an open-source and public blockchain
platform featuring smart contract (that is, scripting) functionality. It
is similar to bitcoin but differs in that it extends beyond monetary
transactions.
&lt;/p&gt;

&lt;p&gt;
Smart contracts are written in programming languages, such as Solidity
(similar to C and JavaScript), Serpent (similar to Python), LLL (a
Lisp-like language) and Mutan (Go-based). Smart contracts are compiled
into EVM (see below) bytecode and deployed across the ethereum blockchain
for execution. Smart contracts help in the exchange of money, property,
shares or anything of value, and it does so in a transparent and conflict-free
way avoiding the traditional middleman.
&lt;/p&gt;

&lt;p&gt;
If you recall from Part I, a typical layout for any
blockchain is one where all nodes are connected to every other node,
creating a mesh. In the world of ethereum, these nodes are referred
to as Ethereum Virtual Machines (EVMs), and each EVM will host a copy
of the entire blockchain. Each EVM also will compete to mine the next
block or validate a transaction. Once the new block is appended to the
blockchain, the updates are propagated to the entire network, so that
each node is synchronized.
&lt;/p&gt;

&lt;p&gt;
In order to become an EVM node on an ethereum network, you'll need to
download and install the proper software. To accomplish this, you'll
be using Geth (Go Ethereum). Geth is the official Go implementation
of the ethereum protocol. It is one of three such implementations;
the other two are written in C++ and Python. These open-source software
packages are licensed under the GNU Lesser General Public License (LGPL)
version 3. The standalone Geth client packages for all
supported operating systems and architectures, including Linux, are available
&lt;a href="https://geth.ethereum.org/downloads"&gt;here&lt;/a&gt;. The source code for
the package is hosted on &lt;a href="https://github.com/ethereum/go-ethereum"&gt;GitHub&lt;/a&gt;.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/blockchain-part-ii-configuring-blockchain-network-and-leveraging-technology" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 24 Apr 2018 16:30:00 +0000</pubDate>
    <dc:creator>Petros Koutoupis</dc:creator>
    <guid isPermaLink="false">1339695 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Blockchain, Part I: Introduction and Cryptocurrency</title>
  <link>https://www.linuxjournal.com/content/blockchain-part-i-introduction-and-cryptocurrency</link>
  <description>  &lt;div data-history-node-id="1339694" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bigstock--208224247.jpg" width="600" height="600" alt="""" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/petros-koutoupis" lang="" about="https://www.linuxjournal.com/users/petros-koutoupis" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Petros Koutoupis&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
It seems nearly impossible these days to open a news
feed discussing anything technology- or finance-related and not
see a headline or two covering bitcoin and its underlying framework,
blockchain. But why? What makes both bitcoin and blockchain so
exciting? What do they provide? Why is everyone talking about this? And,
what does the future hold?&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
In this two-part series, I introduce this
now-trending technology, describe how it works and provide instructions
for deploying your very own private blockchain network.&lt;/p&gt;

&lt;h3&gt;
Bitcoin and Cryptocurrency&lt;/h3&gt;

&lt;p&gt;
The concept of cryptocurrency isn't anything new, although with the
prevalence of
the headlines alluded to above, one might think otherwise. Invented
and released in 2009 by an unknown party under the name Satoshi
Nakamoto, bitcoin is one such kind of cryptocurrency in that it provides
a decentralized method for engaging in digital transactions. It is
also a global technology, which is a fancy way of saying that it's
a worldwide payment system. With the technology being decentralized,
not one single entity is considered to have ownership or the
ability to impose regulations on the technology.
&lt;/p&gt;

&lt;p&gt;
But, what does that truly mean? Transactions are secure. This makes them more
difficult to track and, therefore, difficult to tax.
This is because these transactions are strictly peer-to-peer,
without an intermediary in between. Sounds too good to be true,
right? Well, it &lt;em&gt;is&lt;/em&gt; that good.
&lt;/p&gt;

&lt;p&gt;
Although transactions are limited to the two parties involved, they do,
however, need to be validated across a network of independently functioning
nodes, called a blockchain. Using cryptography and a distributed public
ledger, transactions are verified.
&lt;/p&gt;

&lt;p&gt;
Now, aside from making secure and more-difficult-to-trace transactions,
what is the real appeal to these cryptocurrency platforms? In the case of
bitcoin, a "bitcoin" is generated as a reward through the process of
"mining". And if you fast-forward to the present, bitcoin has earned
monetary value in that it can be used to purchase both goods and services,
worldwide. Remember, this is a digital currency, which means no physical
"coins" exist. You must keep and maintain your own cryptocurrency
wallet and spend the money accrued with retailers and service providers
that accept bitcoin (or any other type of cryptocurrency) as a method
of payment.
&lt;/p&gt;

&lt;p&gt;
All hype aside, predicting the price of cryptocurrency is a fool's
errand, and there's not a single variable driving its worth. One thing
to note, however, is that cryptocurrency is not in any way a monetary
investment in a real currency. Instead, buying into cryptocurrency is
an investment into a possible future where it can be exchanged for goods
and services—and that future may be arriving sooner than expected.
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/blockchain-part-i-introduction-and-cryptocurrency" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 09 Apr 2018 15:45:00 +0000</pubDate>
    <dc:creator>Petros Koutoupis</dc:creator>
    <guid isPermaLink="false">1339694 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>It’s Here. The March 2018 Issue of Linux Journal Is Available for Download Now.</title>
  <link>https://www.linuxjournal.com/content/its-here-march-2018-issue-linux-journal-available-download-now</link>
  <description>  &lt;div data-history-node-id="1339791" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/LJ-March2018-Cover.png" width="600" height="600" alt="Cover" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/carlie-fairchild" lang="" about="https://www.linuxjournal.com/users/carlie-fairchild" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Carlie Fairchild&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;Boasting as many pages as most technical books, this month’s issue of &lt;cite&gt;Linux Journal&lt;/cite&gt; comes in at a hefty 181—that’s 23 articles exploring topics near and dear to everyone from home automation hobbyists to Free Software advocates to hard-core hackers to high-level systems architects.
&lt;p&gt;
&lt;/p&gt;
&lt;img src="https://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/small-200px-left-align-wrap/u800391/march_2018_blockchain_0.jpg" alt="" title="" class="imagecache-small-200px-left-align-wrap" /&gt;
Besides making the magazine bigger overall with more articles in each issue on a wider range of topics, we’ve also added a new feature that explores a given topic in-depth: the Deep Dive—think of it like an ebook inside each magazine. This month contributing editor Petros Koutoupis dives deep in to blockchain. He explores what makes Bitcoin and blockchain so exciting, what they provide, and what the future of blockchain holds. From there, he describes how to set up a private Etherium blockchain using open-source tools and looks at some markets and industries where blockchain technologies can add value.
&lt;p&gt;
&lt;/p&gt;
Subscribers, you can &lt;a href="https://secure2.linuxjournal.com/pdf/dljdownload.php"&gt;download your March issue&lt;/a&gt; now.
&lt;p&gt;
&lt;/p&gt;
Not a subscriber? It’s not too late. &lt;a href="http://www.linuxjournal.com/subscribe"&gt;Subscribe today&lt;/a&gt; and receive instant access to this and all back issues since 2010. Alternatively, you can buy the single issue &lt;a href="https://linuxjournalstore.com/collections/back-issues-of-linux-journal/products/march-2018-issue-of-linux-journal"&gt;here&lt;/a&gt;.&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/its-here-march-2018-issue-linux-journal-available-download-now" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 13 Mar 2018 15:08:24 +0000</pubDate>
    <dc:creator>Carlie Fairchild</dc:creator>
    <guid isPermaLink="false">1339791 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Cryptocurrency and the IRS</title>
  <link>https://www.linuxjournal.com/content/cryptocurrency-and-irs</link>
  <description>  &lt;div data-history-node-id="1339685" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/bitcoin_fig1.png" width="800" height="474" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;&lt;em&gt;
One for you, one for me, and 0.15366BTC for Uncle Sam.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;
When people ask me about bitcoin, it's usually because someone told
them about my days as an early miner. I had thousands of bitcoin, and
I sold them for around a dollar each. At the time, it was awesome, but
looking back—well you can do the math. I've been mining and trading
with cryptocurrency ever since it was invented, but it's only over the
past few years that I've been concerned about taxes.
&lt;/p&gt;

&lt;p&gt;
In the beginning, no one knew how to handle the tax implications of
bitcoin. In fact, that was one of the favorite aspects of the idea for
most folks. It wasn't "money", so it couldn't be taxed. We could start
an entire societal revolution without government oversight! Those times
have changed, and now the government (at least here in the US) very much
does expect to get taxes on cryptocurrency gains. And you know what? It's
very, very complicated, and few tax professionals know how to handle it.
&lt;/p&gt;

&lt;h3&gt;What Is Taxable?&lt;/h3&gt;

&lt;p&gt;
Cryptocurrencies (bitcoin, litecoin, ethereum and any of the 10,000 other
altcoins) are taxed based on the "gains" you make with them. (Often in this
article I mention bitcoin specifically, but the rules are the same for
all cryptocurrency.) Gains are considered income, and income is taxed. What
sorts of things are considered gains? Tons. Here are a few examples:
&lt;/p&gt;

&lt;ul&gt;&lt;li&gt;&lt;p&gt;
Mining.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;
Selling bitcoin for cash.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;
Trading one crypto coin for another on an exchange.
&lt;/p&gt;&lt;/li&gt;

&lt;li&gt;&lt;p&gt;
Buying something directly with bitcoin.
&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;
The frustrating part about taxes and cryptocurrency is that every
transaction must be calculated. See, with cash transactions, a dollar is
always worth a dollar (according to the government, let's not get into
a discussion about fiat currency). But with cryptocurrency, at any given
moment, the coin is worth a certain amount of dollars. Since we're taxed
on dollars, that variance must be tracked so we are sure to report how
much "money" we had to spend.
&lt;/p&gt;

&lt;p&gt;
It gets even more complicated, because we're taxed on the same bitcoin
over and over. It's not "double dipping", because the taxes are only
on the gains and losses that occurred between transactions. It's not
unfair, but it's insanely complex. Let's look at the life of a bitcoin
from the moment it's mined. For simplicity's sake, let's say it took
exactly one day to mine one bitcoin:
&lt;/p&gt;

&lt;p&gt;
1) After 24 hours of mining, I receive 1BTC. The market price for bitcoin
that day was $1,000 per BTC. It took me $100 worth of electricity that
day to mine (yes, I need to track the electrical usage if I want to
deduct it as a loss).
&lt;/p&gt;

&lt;p&gt;
&lt;em&gt;Taxable income for day 1: $900.&lt;/em&gt;&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/cryptocurrency-and-irs" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 28 Feb 2018 14:28:25 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1339685 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>The Actually Distributed Web</title>
  <link>https://www.linuxjournal.com/content/actually-distributed-web</link>
  <description>  &lt;div data-history-node-id="1339457" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/12215f1.png" width="800" height="423" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/doc-searls" lang="" about="https://www.linuxjournal.com/users/doc-searls" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Doc Searls&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
I thought my mind was through getting blown until I heard in mid-June 2017 that &lt;a href="https://brave.com"&gt;Brave&lt;/a&gt;
raised $35 million in &lt;a href="https://techcrunch.com/2017/06/01/brave-ico-35-million-30-seconds-brendan-eich"&gt;less
than 30 seconds&lt;/a&gt;,
though an ICO (&lt;a href="http://www.investopedia.com/terms/i/initial-coin-offering-ico.asp"&gt;Initial Coin
Offering&lt;/a&gt;).
I did know ICOs were hot stuff. I also knew Brave's ICO was about to
happen, because &lt;a href="https://en.wikipedia.org/wiki/Brendan_Eich"&gt;Brendan Eich&lt;/a&gt;,
the company CEO, said so over breakfast two days
earlier. So my seat belt was fastened, but the acceleration of the ICO still left
my mental ass on the pavement two counties back.
&lt;/p&gt;


&lt;p&gt;
Since then, I've hyper-focused on &lt;a href="https://en.wikipedia.org/wiki/Cryptocurrency"&gt;cryptocurrencies&lt;/a&gt;,
&lt;a href="http://tokenfactory.io/smart-beta"&gt;tokens&lt;/a&gt;,
&lt;a href="https://en.wikipedia.org/wiki/Distributed_ledger"&gt;distributed ledgers&lt;/a&gt;,
ICOs and the
rest of it for two reasons. One is that there is a craze going on. See
Figure 1.
&lt;/p&gt;
&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1000009/12215f1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Figure 1. Crypto Currency Market Capitalizations (from &lt;a href="http://coinmarketcap.com/charts"&gt;http://coinmarketcap.com/charts&lt;/a&gt;)
&lt;/p&gt;

&lt;p&gt;
The other is that the investment here includes a measure of faith that we can once
again imagine full agency for individuals as distributed peers on the internet,
and that many positive personal, social, economic, political and other
transformations will arise from that agency.
&lt;/p&gt;

&lt;p&gt;
Phil Windley, who now chairs the &lt;a href="https://sovrin.org"&gt;Sovrin Foundation&lt;/a&gt;,
told me yesterday that this is
the third tech revolution of his lifetime. "The first was the PC, and the second
was the Internet. This is the third", he said. I'm inclined to agree, simply
because so many of us are seeing a wide open future where before there was just a
wall of silos. I lamented that wall here in &lt;em&gt;Linux Journal&lt;/em&gt;, way &lt;a href="http://www.linuxjournal.com/content/way-ranch"&gt;back in September
2011&lt;/a&gt;:
&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;
As entities on the Web, we have devolved. Client-server has become calf-cow. The
client—that's you—is the calf, and the Web site is the cow. What you get
from the cow is milk and cookies. The milk is what you go to the site for. The
cookies are what the site gives to you, mostly for its own business purposes,
chief among which is tracking you like an animal. There are perhaps a billion or
more server-cows now, each with its own "brand" (as marketers and cattle owners
like to say).
&lt;/p&gt;

&lt;p&gt;
This is not what the Net's founders had in mind. Nor was it what Tim Berners-Lee
meant for his World Wide Web of hypertext documents to become. But it's what we've
got, and it's getting worse.
&lt;/p&gt;&lt;/blockquote&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/actually-distributed-web" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Tue, 08 Aug 2017 12:13:23 +0000</pubDate>
    <dc:creator>Doc Searls</dc:creator>
    <guid isPermaLink="false">1339457 at https://www.linuxjournal.com</guid>
    </item>
<item>
  <title>Here, Have Some Money...</title>
  <link>https://www.linuxjournal.com/content/here-have-some-money</link>
  <description>  &lt;div data-history-node-id="1338676" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/11834compf1.png" width="640" height="285" alt="" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/shawn-powers" lang="" about="https://www.linuxjournal.com/users/shawn-powers" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Shawn Powers&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
I love Bitcoin. It's not a secret; I've written about Bitcoin mining and
cryptocurrency in the past. I'm the first to admit, however, that we're at
the very beginning of the cryptocurrency age. Although it's becoming easier
and easier 
to use Bitcoin (see &lt;a href="http://www.coinbase.com"&gt;http://www.coinbase.com&lt;/a&gt;, for example), the limited use cases
combined with the wild volatility of price make Bitcoin the wild wild west of
the financial world.
&lt;/p&gt;

&lt;p&gt;
There are a few awesome ideas, however, that are brilliant in their
simplicity. Certainly things like the Humble Bundle folks integrating
Bitcoin purchasing and Overstock.com allowing Bitcoin purchases are great
first steps. Those are really just re-inventing the wheel, however, because we
already can buy things using existing forms of currency.
&lt;/p&gt;

&lt;p&gt;
Enter ChangeTip.
&lt;/p&gt;

&lt;img src="http://www.linuxjournal.com/files/linuxjournal.com/ufiles/imagecache/large-550px-centered/u1002061/11834compf1.png" alt="" title="" class="imagecache-large-550px-centered" /&gt;&lt;p&gt;
Sending someone a tip or donation on the Internet generally has been done
with something like PayPal. That's all well and good, but it's fairly
cumbersome. The folks at &lt;a href="http://www.changetip.com"&gt;changetip.com&lt;/a&gt; have made sending money over the
Internet extremely simple, and fun!
&lt;/p&gt;

&lt;p&gt;
With its integration into Twitter, Facebook, GitHub, Google+, YouTube,
Reddit and more, ChangeTip makes sending money as simple as sending a Tweet.
Thanks to the world of OAUTH, you don't even need to create an account on
ChangeTip.com to claim or send funds. If you send money to people who
don't have accounts, they simply sign in to ChangeTip via the
social-media account from which you sent it to them, and the money will be
there waiting for
them. Oh, and the money? It's Bitcoin!
&lt;/p&gt;

&lt;p&gt;
With its seamless integration to Coinbase, ChangeTip makes actual financial
transactions secure, simple, and did I mention simple? Check it out today
at &lt;a href="http://changetip.com"&gt;http://changetip.com&lt;/a&gt;, or visit my personal page at
&lt;a href="http://shawnp0wers.tip.me"&gt;shawnp0wers.tip.me&lt;/a&gt;. And, if you want incentive to try
it out, I
originally planned to include a bunch of "one-time links" in this article
that could be claimed for $1 each. It turns out that the one-time links expire
after a week. So although it might have been a great April Fool's joke, I
really want to give everyone a chance to claim some tips, so keep reading!
&lt;/p&gt;

&lt;p&gt;
On April 1st, 2015, watch my personal Twitter account (@shawnp0wers), and
I'll tweet out some ChangeTip URLs worth actual money. Be the first to
click the link, and you will be the proud owner of $1 from yours truly.
I'll try to spread out the tweets throughout the day, so don't worry if you're
reading this after work. It probably won't be too late!
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/here-have-some-money" hreflang="und"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Mon, 06 Apr 2015 21:29:29 +0000</pubDate>
    <dc:creator>Shawn Powers</dc:creator>
    <guid isPermaLink="false">1338676 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
