<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" version="2.0" xml:base="https://www.linuxjournal.com/tag/cadnano">
  <channel>
    <title>cadnano</title>
    <link>https://www.linuxjournal.com/tag/cadnano</link>
    <description/>
    <language>en</language>
    
    <item>
  <title>DNA Geometry with cadnano</title>
  <link>https://www.linuxjournal.com/content/dna-geometry-cadnano</link>
  <description>  &lt;div data-history-node-id="1340744" class="layout layout--onecol"&gt;
    &lt;div class="layout__region layout__region--content"&gt;
      
            &lt;div class="field field--name-field-node-image field--type-image field--label-hidden field--item"&gt;  &lt;img src="https://www.linuxjournal.com/sites/default/files/nodeimage/story/cadnano3.png" width="1496" height="998" alt="cadnano" typeof="foaf:Image" class="img-responsive" /&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-author field--type-ds field--label-hidden field--item"&gt;by &lt;a title="View user profile." href="https://www.linuxjournal.com/users/joey-bernard" lang="" about="https://www.linuxjournal.com/users/joey-bernard" typeof="schema:Person" property="schema:name" datatype="" xml:lang=""&gt;Joey Bernard&lt;/a&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"&gt;&lt;p&gt;
This article introduces a tool you can use to work on three-dimensional DNA origami. The package is called &lt;a href="https://cadnano.org"&gt;cadnano&lt;/a&gt;, and it's currently
being developed at the Wyss Institute. With this package, you'll be
able to construct and manipulate the three-dimensional representations
of DNA structures, as well as generate publication-quality graphics of
your work.
&lt;/p&gt;

&lt;p&gt;
Because this software is research-based, you won't likely find
it in the package repository for your favourite distribution, in which
case
you'll need to install it from the GitHub
repository.
&lt;/p&gt;

&lt;p&gt;
Since cadnano is a Python
program, written to use the Qt framework, you'll need to install
some packages first. For example, in Debian-based distributions, you'll
want to run the following commands:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
sudo apt-get install python3 python3-pip
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
I found that installation was a bit tricky, so I created a virtual Python
environment to manage module installations.
&lt;/p&gt;

&lt;p&gt;
Once you're in your activated
virtualenv, install the required Python modules with the
command:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
pip3 install pythreejs termcolor pytz pandas pyqt5 sip
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
After those dependencies are installed, grab the source code with
the command:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
git clone https://github.com/cadnano/cadnano2.5.git
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
This will grab the Qt5 version. The Qt4 version is in the repository
&lt;a href="https://github.com/cadnano/cadnano2.git"&gt;https://github.com/cadnano/cadnano2.git&lt;/a&gt;.
&lt;/p&gt;

&lt;p&gt;
Changing directory into the
source directory, you can build and install cadnano with:

&lt;/p&gt;&lt;pre&gt;
&lt;code&gt;
python setup.py install
&lt;/code&gt;
&lt;/pre&gt;


&lt;p&gt;
Now your cadnano should be available within the virtualenv.
&lt;/p&gt;

&lt;p&gt;
You can start cadnano simply by executing the &lt;code&gt;cadnano&lt;/code&gt;
command from
a terminal window. You'll see an essentially blank
workspace, made up of several empty view panes and an empty inspector
pane on the far right-hand side.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/cadnano1.png" width="650" height="445" alt="""" class="image-max_650x650" /&gt;&lt;p&gt;
&lt;em&gt;Figure 1. When you first start cadnano, you get a completely
blank work space.&lt;/em&gt;
&lt;/p&gt;

&lt;p&gt;
In order to walk through a few of
the functions available in cadnano, let's create
a six-strand nanotube. The first step is to create a background that
you can use to build upon. At the top of the main window, you'll find
three buttons in the toolbar that will let you create a "Freeform",
"Honeycomb" or "Square" framework. For this example,
click the honeycomb button.
&lt;/p&gt;

&lt;img src="https://www.linuxjournal.com/sites/default/files/styles/max_650x650/public/u%5Buid%5D/cadnano2.png" width="650" height="434" alt="""" class="image-max_650x650" /&gt;&lt;p&gt;
&lt;em&gt;Figure 2. Start your construction with one of the
available geometric frameworks.&lt;/em&gt;
&lt;/p&gt;&lt;/div&gt;
      
            &lt;div class="field field--name-node-link field--type-ds field--label-hidden field--item"&gt;  &lt;a href="https://www.linuxjournal.com/content/dna-geometry-cadnano" hreflang="en"&gt;Go to Full Article&lt;/a&gt;
&lt;/div&gt;
      
    &lt;/div&gt;
  &lt;/div&gt;

</description>
  <pubDate>Wed, 07 Aug 2019 19:30:00 +0000</pubDate>
    <dc:creator>Joey Bernard</dc:creator>
    <guid isPermaLink="false">1340744 at https://www.linuxjournal.com</guid>
    </item>

  </channel>
</rss>
