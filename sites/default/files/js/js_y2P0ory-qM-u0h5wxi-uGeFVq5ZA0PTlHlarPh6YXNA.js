/* eslint max-len: 0 */
/* eslint camelcase: 0 */
/* eslint no-unused-vars: 0 */
/* eslint func-names: 0 */
/* eslint space-before-function-paren: 0 */
(function iife($, Drupal, drupalSettings) {
  Drupal.behaviors.loadDisqusListener = {
    /**
     * Our attach function simply attaches an event listener to load the Disqus
     * embed once the user interacts. In order to retain user privacy there is
     * no automatic loading of comments.
     */
    attach: function loadDisqusListener(context) {
      $('#comments-link', context).on('click', function ldl(e) {
        e.preventDefault();

        // Load commments from Disqus.
        loadDisqus(e);

        // Remove comment link.
        $('#comments-link').remove();
      });
    },
  };

  /**
   * This function loads the Disqus embed. Use drupalSettings to adjust the
   * configuration used in this block:
   *
   * @see linuxjournal_page_attachments_alter()
   */
  function loadDisqus() {
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    var disqus_config = function() {
      this.page.url = drupalSettings.linuxjournal.disqus.fullUrl;
      this.page.identifier = drupalSettings.linuxjournal.disqus.prettyUrl;
    };
    (function() { // DON'T EDIT BELOW THIS LINE
      var d = document;
      var s = d.createElement('script');
      s.src = drupalSettings.linuxjournal.disqus.embedUrl;
      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
    }());
  }
}(jQuery, Drupal, drupalSettings));
;
/**
 * This library is hardcoded to one video hosting service: YouTube.
 *
 * The Drupal module and frontend code could be adapted to support multiple
 * video hosting services, but right now it just supports YouTube.
 */
(function iife($, Drupal) {
  Drupal.behaviors.prepVideoEmbeds = {
    attach: function attach(context) {
      // Array of selectors where embeddable videos will be found.
      var embeddableVideos = [
        '.node--view-mode-full .video-embed--unprocessed',
        '.view-display-id-latest_video .views-row:first-child .video-embed--unprocessed',
      ].join(',');

      // Prep eligible videos.
      $(embeddableVideos, context)
        .once('prepVideoEmbeds')
        .each(function prepVideoEmbeds() {
          var $thisVideo = $(this);

          $thisVideo.on('click', loadAndPlayVideo);
          $thisVideo.removeClass('video-embed--unprocessed');
          $thisVideo.removeAttr('href');
        });

      // Setup video overlays if they exist.
      $('.video__overlay-close', context)
        .once('prepVideoOverlays')
        .on('click', closeOverlays);
    },
  };

  /**
   * Inline and autoplay the video from YouTube.
   */
  function loadAndPlayVideo(e) {
    var container = e.target.parentNode;
    var embed = 'https://www.youtube-nocookie.com/embed/' + this.dataset.videoSlug + '?autoplay=1&rel=0';
    var iframe = document.createElement('iframe');

    // Prevent no-js behavior of link.
    e.preventDefault();

    // Prepare video embed
    iframe.setAttribute('src', embed);
    iframe.setAttribute('class', 'video-embed__iframe');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('allowfullscreen', '1');

    // Insert embed into container
    container.innerHTML = '';
    container.appendChild(iframe);

    // Close overlay if it's open.
    closeOverlays(e);
  }

  /**
   * Close video overlay. Yeah this is heavy-handed but there's never more than
   * one overlay so it gets the job done.
   */
  function closeOverlays(e) {
    e.preventDefault();
    $('.video__overlay').addClass('is-closed');
  }
}(jQuery, Drupal));
;
