/* eslint max-len: 0 */
/* eslint camelcase: 0 */
/* eslint no-unused-vars: 0 */
/* eslint func-names: 0 */
/* eslint space-before-function-paren: 0 */
(function iife($, Drupal, drupalSettings) {
  Drupal.behaviors.loadDisqusListener = {
    /**
     * Our attach function simply attaches an event listener to load the Disqus
     * embed once the user interacts. In order to retain user privacy there is
     * no automatic loading of comments.
     */
    attach: function loadDisqusListener(context) {
      $('#comments-link', context).on('click', function ldl(e) {
        e.preventDefault();

        // Load commments from Disqus.
        loadDisqus(e);

        // Remove comment link.
        $('#comments-link').remove();
      });
    },
  };

  /**
   * This function loads the Disqus embed. Use drupalSettings to adjust the
   * configuration used in this block:
   *
   * @see linuxjournal_page_attachments_alter()
   */
  function loadDisqus() {
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    var disqus_config = function() {
      this.page.url = drupalSettings.linuxjournal.disqus.fullUrl;
      this.page.identifier = drupalSettings.linuxjournal.disqus.prettyUrl;
    };
    (function() { // DON'T EDIT BELOW THIS LINE
      var d = document;
      var s = d.createElement('script');
      s.src = drupalSettings.linuxjournal.disqus.embedUrl;
      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
    }());
  }
}(jQuery, Drupal, drupalSettings));
;
